#!/bin/bash

# Define the path to the YAML file
yaml_file="./src/environments/version.yaml"

# Get the current date in the format "YYYY.MM.DD"
current_date=$(date +%Y.%m.%d)

# Check if the file exists
if [ -e "$yaml_file" ]; then
    # Extract the current version from the YAML file
    current_version=$(awk '/version/ {print $2}' "$yaml_file")

    if [ -n "$current_version" ]; then
        # Extract the date portion of the current version
        version_date="${current_version:0:10}"

        # Check if the current date is the same as the version date
        if [ "$current_date" = "$version_date" ]; then
            # Split the current version to extract the last two digits
            last_two_digits="${current_version:(-2)}"

            # Increment the last two digits
            new_last_two_digits=$((10#$last_two_digits + 1))  # Ensure base 10 for correct arithmetic
            # Add leading zeros if necessary
            new_last_two_digits=$(printf "%02d" "$new_last_two_digits")

            # Construct the new version with updated last two digits
            new_version="${current_date}.${new_last_two_digits}"
        else
            # It's a new day, so start the version at "01"
            new_version="${current_date}.01"
        fi
    else
        # 'version' property not found in the file, so start the version at "01"
        new_version="${current_date}.01"
    fi

    # Update the YAML file with the new version
    sed -i -e "s/version: .*/version: $new_version/" "$yaml_file"

    echo "Updated version to $new_version"
else
    # File doesn't exist, so create it with version "YYYY.MM.DD.01" and current date
    new_version="${current_date}.01"
    echo -e "version: $new_version" > "$yaml_file"
    echo "Created $yaml_file with version $new_version"
fi
