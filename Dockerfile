FROM public.ecr.aws/nginx/nginx-unprivileged:1.26.1-alpine

# Remove any unwanted packages (if they are even present) and force an upgrade of all package dependencies
USER root
RUN apk del curl fontconfig nginx-module-image-filter geoip nginx-module-geoip scanelf libc-utils nginx-module-xslt \
    && apk add libcap-setcap \
    && apk update \
    && apk upgrade \
    && apk cache clean

# Allow NGinx to bind on port < 1024
RUN setcap cap_net_bind_service=+ep /usr/sbin/nginx

# Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

USER nginx

# Define configuration and routes for nginx
COPY .nginx/nginx.conf /etc/nginx/nginx.conf

# Add package dependencies (used by security scanners?)
COPY package.json yarn.lock ./

# Copy build artifacts into released image
COPY dist /usr/share/nginx/user-mgmt

# Expose the legacy and more secure port
EXPOSE 80
EXPOSE 8080

ENTRYPOINT ["nginx", "-g", "daemon off;"]

