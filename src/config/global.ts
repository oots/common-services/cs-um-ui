import { GlobalConfig } from '@eui/core';

export const INACTIVITY_TIMEOUT = 0;

export const GLOBAL: GlobalConfig = {
  appTitle: 'CSDR-app',
  i18n: {
    i18nService: {
      defaultLanguage: 'en',
      languages: [
        'bg',
        'cs',
        'da',
        'de',
        'et',
        'el',
        'en',
        'es',
        'fr',
        'ga',
        'hr',
        'it',
        'lv',
        'hu',
        'mt',
        'nl',
        'pl',
        'pt',
        'ro',
        'sk',
        'sl',
        'fi',
        'sv'
      ]
    },
    i18nLoader: {
      i18nFolders: ['i18n-eui', 'i18n']
    }
  },
  user: {
    defaultUserPreferences: {
      dashboard: {},
      lang: 'en'
    }
  }
};
