import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppStarterService } from './app-starter.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CommonDataApi } from './services/main/common-data/common-data.api';
import { CommonService } from './services/main/common.service';
import { CoreModule } from './core/core.module';
import { MainComponent } from './components/main/main.component';
import { MenuService } from './services/main/menu.service';
import { NgModule } from '@angular/core';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './components/main/dashboard/dashboard.component';
import { TopUserBarComponent } from './components/main/common/top-user-bar/top-user-bar.component';
import { TopMenuBarComponent } from './components/main/common/top-menu-bar/top-menu-bar.component';
import { ListRowActionComponent } from './components/main/common/list-row-action/list.row.action.component';
import { UserComponent } from './components/main/user/user.component';
import { UserListComponent } from './components/main/user/user-list/user-list.component';
import { BreadcrumbComponent } from 'src/app/components/main/common/breadcrumb/breadcrumb.component';
import { BreadcrumbService } from 'src/app/services/main/breadcrumb.service';
import { FlowComponent } from 'src/app/components/flow/flow.component';
import { DataTableComponent } from 'src/app/components/main/common/data-table/data-table.component';
import { TableFilterComponent } from 'src/app/components/main/common/table-filter/table-filter.component';
import { TextColumnComponent } from 'src/app/components/main/common/filter/text-column/text-column.component';
import { StatusComponent } from 'src/app/components/main/common/status/status.component';
import { UserFlowComponent } from './components/main/user/user-flow/user-flow.component';
import { UserEditComponent } from './components/main/user/user-edit/user-edit.component';
import { FlowTemplateComponent } from 'src/app/components/flow/flow-template/flow-template.component';
import { FormErrMsgComponent } from 'src/app/components/main/common/form-err-msg/form-err-msg.component';
import { AlertComponent } from 'src/app/components/main/common/alert/alert.component';
import { EDeliveryComponent } from './components/main/e-delivery/e-delivery.component';
import { EDeliveryListComponent } from './components/main/e-delivery/e-delivery-list/e-delivery-list.component';
import { EDeliveryEditComponent } from './components/main/e-delivery/e-delivery-edit/e-delivery-edit.component';
import { EDeliveryFlowComponent } from './components/main/e-delivery/e-delivery-flow/e-delivery-flow.component';
import { LoginComponent } from 'src/app/components/login/login.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { RouterModule } from '@angular/router';
import { ToogleRowComponent } from './components/main/common/data-table/toogle-row/toogle-row.component';
import { EventsHistoryComponent } from './components/main/events-history/events-history.component';
import { EventsHistoryListComponent } from './components/main/events-history/events-history-list/events-history-list.component';
import { FlagPipe } from './pipes/flag.pipe';
import { ConfirmationDialogComponent } from './components/main/common/confirmation-dialog/confirmation-dialog.component';
import { UserEditConfirmationComponent } from './components/main/user/user-edit/user-edit-confirmation/user-edit-confirmation.component';
import { SummaryPageComponent } from './components/main/common/summary-page/summary-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NotfoundComponent,
    DashboardComponent,
    TopUserBarComponent,
    TopMenuBarComponent,
    ListRowActionComponent,
    UserComponent,
    UserListComponent,
    EventsHistoryListComponent,
    BreadcrumbComponent,
    FlowComponent,
    DataTableComponent,
    TableFilterComponent,
    ToogleRowComponent,
    TextColumnComponent,
    StatusComponent,
    UserFlowComponent,
    UserEditComponent,
    FlowTemplateComponent,
    FormErrMsgComponent,
    AlertComponent,
    EDeliveryComponent,
    EDeliveryListComponent,
    EDeliveryEditComponent,
    EDeliveryFlowComponent,
    LoginComponent,
    HomeComponent,
    EventsHistoryComponent,
    ConfirmationDialogComponent,
    UserEditConfirmationComponent,
    SummaryPageComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    FlagPipe
  ],
  providers: [AppStarterService, CommonService, CommonDataApi, MenuService, BreadcrumbService],
  bootstrap: [AppComponent]
})
export class AppModule {}
