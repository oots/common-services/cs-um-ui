import { Component } from '@angular/core';
import { FlowTemplateComponent } from '../components/flow/flow-template/flow-template.component';
import { UserEditComponent } from '../components/main/user/user-edit/user-edit.component';

@Component({
  selector: 'app-user-edit',
  template: '',
  providers: [{ provide: UserEditComponent, useValue: StubUserEditComponent }]
})
export class StubUserEditComponent {}

@Component({
  selector: 'app-flow-template',
  template: '',
  providers: [{ provide: FlowTemplateComponent, useValue: StubFlowTemplateComponent }]
})
export class StubFlowTemplateComponent {}
