import { UiObjectModel } from '../ui-object.model';
import {
  clearAndDisableOtherFieldsIfEmptyDec,
  clearOtherFieldIfSmaller,
  disablePastDate,
  email,
  required,
  trim,
  username
} from '../validation-decorators';
import { RoleModel } from './role.model';

export class UserModel extends UiObjectModel {
  @required()
  @trim()
  @email()
  email?: string = null;
  @required()
  @trim()
  @username()
  userName?: string = null;
  userRoles?: any = null;
  adminRoles?: any = null;
  @required()
  country?: string = null;
  status?: number = 1;
  statusHelper?: boolean = true;
  @required()
  roles?: RoleModel[];
  @required()
  @disablePastDate('startDate', 'startDate')
  @clearOtherFieldIfSmaller('endDate')
  startDate?: Date = null;
  @required()
  @disablePastDate('endDate', 'statusHelper')
  endDate?: Date = null;
  @required()
  @clearAndDisableOtherFieldsIfEmptyDec(['adminRoles', 'userRoles', 'roles'])
  countryHelper?: any = null;

  constructor(data?) {
    super(data);
    this.roles = [];
    if (data) {
      this.email = data.email;
      this.country = data.country;
      this.userName = data.userName;
      this.status = data.status;
      this.statusHelper = data.status ? true : false;
      this.startDate = data.startDate;
      this.endDate = data.endDate;
      if (data.roles) {
        data.roles.forEach((i) => this.roles.push(new RoleModel(i)));
      }
    }
  }
}
