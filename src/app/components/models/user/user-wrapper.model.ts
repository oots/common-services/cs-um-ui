import { validateToServer } from '../validation-decorators';
import { UserModel } from './user.model';

export class UserWrapperModel {
  @validateToServer('user/validation')
  user?: UserModel = new UserModel();

  constructor(data?) {
    if (data) {
      this.user = new UserModel(data);
    }
  }
}
