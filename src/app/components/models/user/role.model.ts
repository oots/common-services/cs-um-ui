import { UiObjectModel } from '../ui-object.model';
import { required, trim } from '../validation-decorators';

export class RoleModel extends UiObjectModel {
  @required()
  @trim()
  roleName?: string = null;
  maxAllowedDuration?: null;

  constructor(data?) {
    super(data);
    if (data) {
      this.roleName = data.roleName;
      this.maxAllowedDuration = data.maxAllowedDuration;
    }
  }
}
