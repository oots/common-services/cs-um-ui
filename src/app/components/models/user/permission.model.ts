import { UiObjectModel } from '../ui-object.model';
import { required, trim } from '../validation-decorators';

export class PermissionModel extends UiObjectModel {
  @required()
  @trim()
  permissionName?: string = null;
  @required()
  @trim()
  permissionDescription?: string = null;
  status?: number = 1;

  constructor(data?) {
    super(data);
    if (data) {
      this.permissionName = data.permissionName;
      this.permissionDescription = data.permissionDescription;
      this.status = data.status;
    }
  }
}
