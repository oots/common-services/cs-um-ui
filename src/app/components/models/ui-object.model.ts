export class UiObjectModel {
  id?: string = null;

  constructor(data?) {
    if (data) {
      this.id = data.id;
    }
  }
}
