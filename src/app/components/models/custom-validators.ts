import { FormArray, FormControl, FormGroup } from '@angular/forms';

export function requiredIfOtherFieldIsFilled(otherFieldAttr) {
  return (group: FormGroup, parentGroup): { [key: string]: any } =>
    (
      control: FormControl
    ): {
      [key: string]: any;
    } => {
      let otherField;
      if (group && group.controls) {
        otherField = parentGroup.controls[otherFieldAttr] || group.controls[otherFieldAttr];
      }
      if (!otherField) return null;

      if (
        otherField.value != null &&
        otherField.value != '' &&
        otherField.value.length != 0 &&
        (control.value == null || control.value == '')
      ) {
        return {
          required: true
        };
      }
    };
}

export function requiredIfPropertyOfOtherFieldIsFilled(otherFieldAttr, property) {
  return (group: FormGroup): { [key: string]: any } =>
    (
      control: FormControl
    ): {
      [key: string]: any;
    } => {
      let otherField;
      if (group && group.controls) {
        otherField = group.controls[otherFieldAttr];
      }
      if (!otherField) return null;

      if (control.value !== '' && control.value !== null) return null;

      //if the attribute is an array of objects
      if (
        otherField.value instanceof Array &&
        otherField.value.length > 0 &&
        otherField.value.filter((n) => n[property] && n[property] !== '' && n[property] !== null).length > 0
      ) {
        return {
          required: true
        };
      }
      return null;
    };
}

export function invalidIfPropertyOfOtherFieldIsNotFilled(otherFieldAttr, property) {
  return (group: FormGroup): { [key: string]: any } =>
    (
      control: FormControl
    ): {
      [key: string]: any;
    } => {
      let otherField;
      if (group && group.controls) {
        otherField = group.controls[otherFieldAttr];
      }
      if (!otherField) return null;

      if (control.value === '' || control.value === null) return null;

      //if the attribute is an array of objects
      if (
        otherField.value instanceof Array &&
        otherField.value.length > 0 &&
        otherField.value.filter((n) => !n[property] || n[property] === '' || n[property] === null).length > 0
      ) {
        return {
          invalid: true
        };
      }
      return null;
    };
}

export function invalidIfOtherFieldIsNotFilled(otherFieldAttr) {
  return (group: FormGroup): { [key: string]: any } =>
    (): {
      [key: string]: any;
    } => {
      let otherField;
      if (group && group.controls) {
        otherField = group.controls[otherFieldAttr];
      }
      if (!otherField) return null;

      //if the attribute is an array of objects
      if (otherField.value instanceof Array && otherField.value.length == 0) {
        return {
          invalid: true
        };
      }
      return null;
    };
}

export function clearAndDisableOtherFieldIfEmpty(otherFieldsAttr) {
  return (group: FormGroup): { [key: string]: any } =>
    (
      control: FormControl
    ): {
      [key: string]: any;
    } => {
      if (control.value === '' || control.value === null || control.value === undefined) {
        otherFieldsAttr.forEach((attr) => {
          if (group.controls[attr]) {
            if (group.controls[attr] instanceof FormArray) {
              group.controls[attr]['controls'] = [];
            } else {
              group.controls[attr].setValue(null);
            }
            group.controls[attr].disable();
          }
        });
      } else {
        otherFieldsAttr.forEach((attr) => {
          if (group.controls[attr]) {
            group.controls[attr].enable();
          }
        });
      }
      return null;
    };
}

export function disablePastDateVal(dateControl, fieldToDisable) {
  return (group: FormGroup): { [key: string]: any } =>
    (): {
      [key: string]: any;
    } => {
      if (group.controls[dateControl] && group.controls[fieldToDisable] && group.controls[dateControl].value) {
        const d1 = new Date(group.controls[dateControl].value).setHours(0, 0, 0, 0);
        const d2 = new Date().setHours(0, 0, 0, 0);
        if (d1 < d2) {
          group.controls[fieldToDisable].disable();
        } else if (group.controls[fieldToDisable].disabled) {
          group.controls[fieldToDisable].enable();
        }
      }
      return null;
    };
}

export function clearOtherFieldIfSmallerVal(otherField) {
  return (group: FormGroup): { [key: string]: any } =>
    (
      control: FormControl
    ): {
      [key: string]: any;
    } => {
      if (group.controls[otherField]) {
        const endDate = new Date(group.controls[otherField].value).setHours(0, 0, 0, 0);
        const startDate = new Date(control.value).setHours(0, 0, 0, 0);
        if (endDate < startDate) {
          group.controls[otherField].setValue(null);
        }
      }
      return null;
    };
}

export function trimValidator() {
  return (): { [key: string]: any } =>
    (
      control: FormControl
    ): {
      [key: string]: any;
    } => {
      const value = control.value;
      if (!value || value === '') return null;
      if (value.startsWith(' ') || value.endsWith(' ')) {
        control.setValue(control.value?.trim(), { emitEvent: false, onlySelf: true });
      }
      return null;
    };
}

export function removeValidatorForDependedVal(attributes) {
  return (group: FormGroup): { [key: string]: any } =>
    (): { [key: string]: any } => {
      attributes.forEach((attr) => {
        if (group && group.controls) {
          const field = group.controls[attr];
          if (field && field.status == 'INVALID' && !field.errors['required']) {
            field.clearValidators();
            field.updateValueAndValidity();
          }
        }
      });
      return null;
    };
}

export function isUsername() {
  return (): { [key: string]: any } =>
    (
      control: FormControl
    ): {
      [key: string]: any;
    } => {
      let response = null;
      if (control.value) {
        const hasMatch = control.value.trim().match(/^[A-Za-z0-9-_]{7,30}$/);
        if (hasMatch === null) {
          response = {
            notUserName: true
          };
        }
      }
      return response;
    };
}