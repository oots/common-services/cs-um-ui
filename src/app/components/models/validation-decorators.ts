import { Validators } from '@angular/forms';
import {
  clearAndDisableOtherFieldIfEmpty,
  clearOtherFieldIfSmallerVal,
  disablePastDateVal,
  invalidIfOtherFieldIsNotFilled,
  invalidIfPropertyOfOtherFieldIsNotFilled,
  isUsername,
  removeValidatorForDependedVal,
  requiredIfOtherFieldIsFilled,
  requiredIfPropertyOfOtherFieldIsFilled,
  trimValidator
} from './custom-validators';
import { ServerSideValidator } from './server-side-validator';

export function required(): any {
  return createValidator(customRequired);
}
export function username(): any {
  return createValidator(isUsername());
}
export function email(): any {
  return  createValidator(nativeEmail);
}

export function removeValidatorForDepended(...attributes): any {
  return createValidator(removeValidatorForDependedVal(attributes));
}

export function trim() {
  return createValidator(trimValidator());
}

export function validateToServer(validationOptions?): any {
  return createValidator(customServerValidation(validationOptions));
}

export function requiredIfOtherFieldIsFilledDec(otherFieldAttr): any {
  return createValidator(requiredIfOtherFieldIsFilled(otherFieldAttr));
}

export function requiredIfPropertyOfOtherFieldIsFilledDec(otherFieldAttr, property): any {
  return createValidator(requiredIfPropertyOfOtherFieldIsFilled(otherFieldAttr, property));
}

export function invalidIfPropertyOfOtherFieldIsNotFilledDec(otherFieldAttr, property): any {
  return createValidator(invalidIfPropertyOfOtherFieldIsNotFilled(otherFieldAttr, property));
}
export function invalidIfOtherFieldIsNotFilledDec(otherFieldAttr): any {
  return createValidator(invalidIfOtherFieldIsNotFilled(otherFieldAttr));
}

export function clearAndDisableOtherFieldsIfEmptyDec(otherFieldsAttr): any {
  return createValidator(clearAndDisableOtherFieldIfEmpty(otherFieldsAttr));
}

export function disablePastDate(dateControl, fieldToDisable): any {
  return createValidator(disablePastDateVal(dateControl, fieldToDisable));
}

export function clearOtherFieldIfSmaller(otherField): any {
  return createValidator(clearOtherFieldIfSmallerVal(otherField));
}

export function customRequired() {
  return Validators.required;
}

export function customServerValidation(type) {
  return (form?, parentForm?, service?) => {
    return ServerSideValidator.createValidator(type, service);
  };
}

export function createValidator(functionToCall: any) {
  return function (object: any, propertyName: string) {
    if (!object.validators) {
      object.validators = {};
    }
    if (!Array.isArray(object.validators[propertyName])) {
      object.validators[propertyName] = [];
    }
    object.validators[propertyName].push(functionToCall);
  };
}

export function nativeEmail() {
  return Validators.email;
}