import { AsyncValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HTTPStatusEnum } from '../../core/models/app.enums';
import { ValidationErrorModel } from '../../core/models/app/validation.model';
import { ValidationApi } from '../../services/main/validation/validation.api';

export class ServerSideValidator {
  static createValidator(type: string, validateApi: ValidationApi): AsyncValidatorFn {
    return (group: FormGroup): Observable<ValidationErrors> => {
      return validateApi.validate(type, group.getRawValue()).pipe(
        map((res: any) => {
          let errors;
          if (res.status === HTTPStatusEnum.OK_200) {
            errors = [];
          } else if (res.status === HTTPStatusEnum.BAD_REQUEST_400) {
            const errorList: ValidationErrorModel[] = [];
            res?.error?.errors.forEach((e) =>
              errorList.push({
                error: e?.reason,
                attribute: e?.attribute
              })
            );
            errors = [...errorList];
          }
          errors.forEach((e) => {
            let g = group;
            e.attribute = e.attribute.split('/')[0];
            e.attribute.split('.').forEach((i) => {
              if (g) {
                // @ts-expect-error This line is expected to cause a TypeScript error
                g = g.controls[i];
              }
            });
            if (g) {
              const error = {};
              error[e.error] = true;
              g.setErrors(error);
            }
          });

          return null;
        }),
        catchError(() => of())
      );
    };
  }
}
