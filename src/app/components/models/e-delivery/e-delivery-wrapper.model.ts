import { validateToServer } from '../validation-decorators';
import { EDeliveryModel } from './e-delivery.model';

export class EDeliveryWrapperModel {
  @validateToServer('edelivery/validation')
  eDelivery?: EDeliveryModel = new EDeliveryModel();

  constructor(data?) {
    if (data) {
      this.eDelivery = new EDeliveryModel(data);
    }
  }
}
