import { UiObjectModel } from '../ui-object.model';
import { removeValidatorForDepended, required, trim } from '../validation-decorators';

export class EDeliveryModel extends UiObjectModel {
  @required()
  @trim()
  name?: string = null;
  @required()
  @trim()
  @removeValidatorForDepended('schemeId')
  identifier?: string = null;
  @required()
  @removeValidatorForDepended('identifier')
  schemeId?: string = null;
  @required()
  country?: string = null;
  owner?: string = null;
  statusHelper?: boolean = true;
  status?: number = 1;

  constructor(data?) {
    super(data);
    if (data) {
      this.name = data.name;
      this.identifier = data.identifier;
      this.schemeId = data.schemeId;
      this.country = data.country;
      this.owner = data.owner;
      this.status = data.status;
      this.statusHelper = !!data.status;
    }
  }
}
