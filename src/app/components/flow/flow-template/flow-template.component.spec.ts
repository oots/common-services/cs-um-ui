import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowTemplateComponent } from './flow-template.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('FlowTemplateComponent', () => {
  let component: FlowTemplateComponent;
  let fixture: ComponentFixture<FlowTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FlowTemplateComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(FlowTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
