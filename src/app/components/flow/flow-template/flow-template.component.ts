import { Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { EuiPopoverComponent } from '@eui/components/eui-popover';

@Component({
  selector: 'app-flow-template',
  templateUrl: './flow-template.component.html',
  styleUrls: ['./flow-template.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlowTemplateComponent extends BaseComponent {
  @Input() data: any = {
    steps: [],
    buttons: [],
    rButtons: [],
    isNavigationAllowed: false,
    activeStep: -1
  };

  @Input()
  form;

  addPressed = false;

  @Output() public stepChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() public wizardActionCalled: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('titlePopover') titlePopover: EuiPopoverComponent;

  onSelectStep(event) {
    this.stepChanged.emit(event.index - 1);
  }

  protected trackByFn(index) {
    return index;
  }

  onWizardAction(event) {
    this.wizardActionCalled.emit(event);
  }

  openTitlePopover(e: any) {
    this.titlePopover.openPopover(e.target);
  }
}
