import { Component, OnDestroy } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { filter } from 'rxjs/operators';
import { UserPermissionEnum } from 'src/app/core/models/app.enums';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flow',
  templateUrl: './flow.component.html',
  styleUrls: ['./flow.component.scss']
})
export class FlowComponent extends BaseComponent implements OnDestroy {
  constructor(
    private commonDataService: CommonDataService,
    private userService: UserService,
    private router: Router
  ) {
    super();
  }

  ngOnDestroy() {
    this.commonDataService.setConfirmationAction(null);
    super.ngOnDestroy();
  }

  initComponent() {
    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !!user?.value && !user.loading)).subscribe((res) => {
        const userPermissions = this.commonDataService.getUserPermissions(res.value, [UserPermissionEnum.USER_APP_VIEW]);
        if (!userPermissions[UserPermissionEnum.USER_APP_VIEW]) {
          this.router.navigate(['/no-permission']);
        } else {
          this.commonDataService.getCountriesList();
          this.commonDataService.getRolesList();
          this.commonDataService.getAssignableRolesList();
          this.commonDataService.getSchemesList();
        }
      })
    );
  }
}
