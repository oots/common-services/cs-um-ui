import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EDeliveryComponent } from './e-delivery.component';
import { StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { CommonDataService } from '../../../services/main/common-data/common-data.service';
import { ValidationApi } from '../../../services/main/validation/validation.api';
import { UserService } from '../../../services/user/user.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { EDeliveryService } from '../../../services/e-delivery/e-delivery.service';
import { BreadcrumbService } from '../../../services/main/breadcrumb.service';

describe('EDeliveryComponent', () => {
  let component: EDeliveryComponent;
  let fixture: ComponentFixture<EDeliveryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EDeliveryComponent],
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      providers: [
        { provide: CommonDataService },
        { provide: BreadcrumbService },
        { provide: EDeliveryService },
        { provide: ValidationApi },
        { provide: UserService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(EDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
