import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EDeliveryEditComponent } from './e-delivery-edit.component';
import { StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';
import { ValidationApi } from '../../../../services/main/validation/validation.api';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { EDeliveryService } from '../../../../services/e-delivery/e-delivery.service';

describe('EDeliveryEditComponent', () => {
  let component: EDeliveryEditComponent;
  let fixture: ComponentFixture<EDeliveryEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EDeliveryEditComponent],
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      providers: [{ provide: CommonDataService }, { provide: ValidationApi }, { provide: EDeliveryService }],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(EDeliveryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
