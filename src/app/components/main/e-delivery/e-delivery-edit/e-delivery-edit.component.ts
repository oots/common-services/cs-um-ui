import { Router } from '@angular/router';
import { Component, EventEmitter, Input, OnDestroy, Output, ViewEncapsulation } from '@angular/core';
import { UserPermissionEnum } from 'src/app/core/models/app.enums';
import { BaseComponent } from 'src/app/components/base/base.component';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { UserProfileModel } from 'src/app/core/models/response/user.profile';
import { EDeliveryService } from 'src/app/services/e-delivery/e-delivery.service';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, first } from 'rxjs/operators';

@Component({
  selector: 'app-e-delivery-edit',
  templateUrl: './e-delivery-edit.component.html',
  styleUrls: ['./e-delivery-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EDeliveryEditComponent extends BaseComponent implements OnDestroy {
  @Input() set user(value: UserProfileModel) {
    if (!value) {
      return;
    }
    this.userProfile = value;
    this.userPermissions = this.commonDataService.getUserPermissions(this.userProfile, [
      UserPermissionEnum.CRUD_EDELIVERY,
      UserPermissionEnum.FE_EDIT_MS_COUNTRY
    ]);
  }

  @Input() set form(value) {
    this.eDeliveryForm = value;
    this.userPermissions = this.commonDataService.getUserPermissions(
      this.userProfile,
      [UserPermissionEnum.CRUD_EDELIVERY, UserPermissionEnum.FE_EDIT_MS_COUNTRY],
      this.eDeliveryForm.get('country').value
    );
  }

  @Output() public saveEDelivery: EventEmitter<any> = new EventEmitter<any>();

  canLeavePage = false;
  eDeliveryForm = null;
  userProfile: UserProfileModel;
  countries: any[] = [];
  roles: any[] = [];
  userPermissions;
  idSchemes = [];
  selectedSchema: any;
  resubmissionSub: Subscription = new Subscription();
  constructor(
    private router: Router,
    private eDeliveryService: EDeliveryService,
    private commonDataService: CommonDataService
  ) {
    super();
  }
  ngOnDestroy(): void {
    this.eDeliveryService.resetConfirmationAction();
    super.ngOnDestroy();
  }

  initComponent() {
    this.addSubscription(
      this.commonDataService.countries$.subscribe((res) => {
        if (res?.value) {
          this.countries = res.value.filter((country) => country.code !== 'EC');
          if (this.userProfile.country && this.userProfile.country !== 'EC') {
            this.countries = this.countries.filter((country) => country.code === this.userProfile.country);
          }
        }
      })
    );
    this.addSubscription(
      this.commonDataService.roles$.subscribe((res) => {
        if (res?.value) {
          this.roles = res.value;
        }
      })
    );
    this.addSubscription(
      this.commonDataService.schemes$.subscribe((res) => {
        if (res?.value) {
          this.idSchemes = res.value.map((item) => ({ ...item, text: item.url }));
          this.selectedSchema = this.idSchemes.find((item) => item.id === this.eDeliveryForm.controls.schemeId.value);
        }
      })
    );
  }
  saveEDeliveryData() {
    this.eDeliveryForm.controls.owner.setValue(this.eDeliveryForm.get('country').value);
    this.eDeliveryForm.markAllAsTouched();
    this.continue();
  }

  continue() {
    this.resubmissionSub.unsubscribe();
    if (this.eDeliveryForm.pending) {
      this.resubmissionSub = this.eDeliveryForm.statusChanges
        .pipe(
          distinctUntilChanged(),
          first(() => !this.eDeliveryForm.pending)
        )
        .subscribe(() => {
          if (this.eDeliveryForm.valid) {
            this.saveEDelivery.emit(this.eDeliveryForm.value);
          }
        });
      this.addSubscription(this.resubmissionSub);
    } else {
      if (this.eDeliveryForm.valid) {
        this.saveEDelivery.emit(this.eDeliveryForm.value);
      }
    }
  }

  slideToggleChange(event) {
    this.eDeliveryForm.get('status').setValue(event ? 1 : 0);
  }

  canDeactivate() {
    if (this.canLeavePage || !this.eDeliveryForm.controls.name.touched) {
      return true;
    } else if (!this.canLeavePage) {
      this.canLeavePage = true;
      return false;
    }
  }
}
