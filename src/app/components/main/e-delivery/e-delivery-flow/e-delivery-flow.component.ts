import { ActionConfirmationEnum, AppFlowEnum, ListActionsEnum, UserPermissionEnum } from 'src/app/core/models/app.enums';
import { NavigationStart, Router } from '@angular/router';
import { Component, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { EuiDialogComponent } from '@eui/components/eui-dialog';
import { FlowModel } from 'src/app/core/models/app/flow.model';
import { ListActionButtonModel } from 'src/app/core/models/response/list.action.button.model';
import { UserProfileModel } from 'src/app/core/models/response/user.profile';
import { UserService } from 'src/app/services/user/user.service';
import { filter } from 'rxjs/operators';
import { EDeliveryService } from 'src/app/services/e-delivery/e-delivery.service';
import { EDeliveryEditComponent } from 'src/app/components/main/e-delivery/e-delivery-edit/e-delivery-edit.component';
import { FormGroup } from '@angular/forms';
import { EDeliveryWrapperModel } from '../../../models/e-delivery/e-delivery-wrapper.model';

@Component({
  selector: 'app-e-delivery-flow',
  templateUrl: './e-delivery-flow.component.html',
  styleUrls: ['./e-delivery-flow.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EDeliveryFlowComponent extends BaseComponent implements OnDestroy {
  navigateUrl: string;
  flowData: FlowModel;
  wizardData: any = {
    lButtons: [],
    buttons: [],
    rButtons: []
  };
  isLeaving = false;
  goToConfirmationPage = false;
  errorKeys: any = {};
  displayComponent: string;
  listActionsEnum = ListActionsEnum;
  actionType: string;
  dialogName: string;
  dialogButtons: ListActionButtonModel[] = [];
  dialogLeftButtons: ListActionButtonModel[] = [];
  userProfile: UserProfileModel;
  userPermissions: any = {};
  form: FormGroup;

  @ViewChild('flowDialog') flowDialog: EuiDialogComponent;
  @ViewChild('editEDeliveryComponent') editEDeliveryComponent: EDeliveryEditComponent;

  constructor(
    private router: Router,
    private userService: UserService,
    private eDeliveryService: EDeliveryService,
    private commonDataService: CommonDataService
  ) {
    super();
  }

  ngOnDestroy() {
    this.isLeaving = true;
    if (!this.goToConfirmationPage) {
      this.commonDataService.resetFlow();
    }
    super.ngOnDestroy();
  }

  initComponent() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.navigateUrl = event.url;
      }
    });

    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !!user?.value && !user.loading)).subscribe((res) => {
        this.userProfile = res.value;
        this.userPermissions = this.commonDataService.getUserPermissions(this.userProfile, [
          UserPermissionEnum.CRUD_EDELIVERY,
          UserPermissionEnum.FE_EDIT_MS_COUNTRY
        ]);
      })
    );

    this.addSubscription(
      this.commonDataService.flow$.subscribe((flow) => {
        if (this.isLeaving || !flow) {
          return;
        }
        if (!flow.value) {
          this.commonDataService.initializeFlow(AppFlowEnum.CREATE_E_DELIVERY);
        } else {
          this.initFlowStep1(flow.value);
        }
      })
    );

    this.addSubscription(
      this.eDeliveryService.actionConfirmation$.subscribe((confirmation) => {
        const result = this.commonDataService.handleActionConfirmation(confirmation, [ActionConfirmationEnum.E_DELIVERY_SAVE]);
        if (result.action && !result.errorKey) {
          this.router.navigate(['main/e-delivery']);
        }
        this.eDeliveryService.resetConfirmationAction();
      })
    );
  }

  initFlowStep1(flow) {
    this.form = this.commonDataService.createFormFromJson(new EDeliveryWrapperModel(flow.data?.eDelivery));
    const activeStep = this.flowData?.activeStep || 0;
    const editFlow = !!flow.data?.user?.id;
    if (activeStep !== flow.activeStep) {
      window.scrollTo(0, 0);
    }
    this.flowData = {
      ...flow,
      editFlow
    };
    if (editFlow === true) {
      this.userPermissions = this.commonDataService.getUserPermissions(
        this.userProfile,
        [UserPermissionEnum.CRUD_EDELIVERY, UserPermissionEnum.FE_EDIT_MS_COUNTRY],
        flow.data.eDelivery.owner
      );
    }
    this.handleStepChanged(this.flowData.activeStep);
  }

  handleStepChanged(event) {
    let wizardButtons = {
      lButtons: [],
      rButtons: [],
      buttons: []
    };

    if (event === 0) {
      wizardButtons = {
        ...wizardButtons,
        lButtons: [
          {
            id: ListActionsEnum.FLOW2_DISCARD,
            label: 'app.list.actions.exitProcess',
            leftIcon: 'eui-close',
            class: 'eui-button--basic eui-button--primary'
          }
        ],
        rButtons: [
          {
            id: ListActionsEnum.FLOW2_ADD,
            label: this.flowData.data?.eDelivery?.id ? 'app.list.actions.update' : 'app.list.actions.add',
            class: 'eui-button--primary'
          }
        ]
      };
    }

    this.wizardData = {
      ...wizardButtons,
      steps: this.wizardData.steps || this.flowData.steps,
      activeStep: this.flowData.activeStep
    };
  }

  setWizardError(key = undefined) {
    this.wizardData = {
      ...this.wizardData,
      errMessageKey: key
    };
  }

  handleWizardActions(event) {
    this.errorKeys = {};
    this.setWizardError();
    switch (event) {
      case ListActionsEnum.FLOW2_DISCARD:
        this.handleDiscardChanges();
        break;
      case ListActionsEnum.FLOW2_ADD:
        this.editEDeliveryComponent.saveEDeliveryData();
        break;
    }
  }

  handleDialogActions(event) {
    if (event === ListActionsEnum.FLOW2_CANCEL_EXIT_PROCESS) {
      this.closeFlowDialog();
    } else if (event === ListActionsEnum.FLOW2_CONFIRM_EXIT_PROCESS) {
      this.closeFlowDialog();
      this.handleDiscardChanges('exit');
    }
  }

  openFlowDialog(actionType) {
    this.actionType = actionType;
    this.displayComponent = null;
    this.dialogButtons = [];
    this.dialogLeftButtons = [];
    if (this.actionType === 'exitProcess') {
      this.dialogName = 'app.flow.2.popup.exitProcessTitle';
      this.displayComponent = 'confirmMessage';
      this.dialogButtons = [
        {
          id: ListActionsEnum.FLOW2_CANCEL_EXIT_PROCESS,
          label: 'app.flow.2.buttons.cancelExitProcess',
          class: 'eui-button eui-u-mr-l'
        },
        {
          id: ListActionsEnum.FLOW2_CONFIRM_EXIT_PROCESS,
          label: 'app.flow.2.buttons.confirmExitProcess',
          class: 'eui-button--primary'
        }
      ];
    }
    this.flowDialog.openDialog();
  }

  closeFlowDialog() {
    this.flowDialog.closeDialog();
    this.dialogName = '';
  }

  handleDiscardChanges(checkStep = 'confirm') {
    if (checkStep === 'exit') {
      this.router.navigate(['/main/e-delivery']);
    } else {
      this.openFlowDialog('exitProcess');
    }
  }

  handleSaveEDelivery(event) {
    this.setWizardError();
    this.eDeliveryService.saveEDelivery(event);
  }
}
