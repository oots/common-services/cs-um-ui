import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EDeliveryFlowComponent } from './e-delivery-flow.component';
import { StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';
import { ValidationApi } from '../../../../services/main/validation/validation.api';
import { UserService } from '../../../../services/user/user.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { EDeliveryService } from '../../../../services/e-delivery/e-delivery.service';

describe('EDeliveryFlowComponent', () => {
  let component: EDeliveryFlowComponent;
  let fixture: ComponentFixture<EDeliveryFlowComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EDeliveryFlowComponent],
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      providers: [{ provide: CommonDataService }, { provide: EDeliveryService }, { provide: ValidationApi }, { provide: UserService }],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(EDeliveryFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
