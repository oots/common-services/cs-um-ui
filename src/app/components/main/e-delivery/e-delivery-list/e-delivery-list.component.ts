import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { ListBaseComponent } from 'src/app/components/base/list.base.component';
import {
  TableColumnFilter,
  TableDataOptionsModel,
  TableOptionsModel,
  TableSettingsModel
} from 'src/app/core/models/app/table.options.model';
import { TableOptionsEnum, UserPermissionEnum } from 'src/app/core/models/app.enums';
import { filter, map } from 'rxjs/operators';
import { TableColumnFilterModel } from 'src/app/core/models/app/table.column.model';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { EDeliveryService } from 'src/app/services/e-delivery/e-delivery.service';

@Component({
  selector: 'app-e-delivery-list',
  templateUrl: '../../common/data-table/data-table-list.component.html',
  styleUrls: ['./e-delivery-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EDeliveryListComponent extends ListBaseComponent {
  @Input()
  public set tableDataOptions(value: TableDataOptionsModel) {
    this.defaultTableOptions = {
      ...this.defaultTableOptions,
      option: value
    };
    if (!this.saveFilter && value) {
      this.filterValues = {};
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  @Input()
  public set pageSize(value: number) {
    this.defaultTableOptions = {
      ...this.defaultTableOptions,
      pageSize: value
    };
  }

  @Input() set tableSettings(value: TableSettingsModel) {
    this.settings = {
      ...value,
      noRecordsText: value.noRecordsText || this.settings.noRecordsText,
      entityName: value.entityName || this.settings.entityName,
      hasActions: this.userPermissions[UserPermissionEnum.CRUD_EDELIVERY]
    };
  }

  @Input() checkedRowId;
  @Input() saveFilter = null;
  @Input() userPermissions = {};
  @Input() userCountry;
  @Input() listFilter: TableColumnFilter[];
  @Input() sortBy: { fieldName: string; order: string };

  @Output() public rowAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() public rowRadioSelect: EventEmitter<any> = new EventEmitter<any>();

  tableOptions$ = this.eDeliveryService.tableOptions$.pipe(
    map((res) => {
      this._tableOptions = res?.value
        ? {
            ...this.defaultTableOptions,
            ...res.value,
            option: this.defaultTableOptions.option
          }
        : this.defaultTableOptions;
      return this._tableOptions;
    })
  );

  data$ = this.eDeliveryService.list$.pipe(
    map((res) => {
      return {
        listInfo: res?.value?.list
          ? {
              ...res.value,
              list: res.value.list.map((row) => ({
                ...row,
                countryCode: row.country,
                checkedRowId: this.checkedRowId,
                listActionBtns: this.eDeliveryService.getActionsForRow(this.userPermissions, this.userCountry),
                canChangeStatus:
                  !this.userCountry || this.userCountry === 'EC'
                    ? this.userPermissions[UserPermissionEnum.CRUD_EDELIVERY]
                    : this.userPermissions[UserPermissionEnum.CRUD_EDELIVERY] && row.country === this.userCountry
              }))
            }
          : this.defaultTableInfo,
        count: res?.value?.count
      };
    })
  );

  columns: TableColumnFilterModel[];
  filterColumns: TableColumnFilterModel[];
  filterValues: any = {};
  defaultTableInfo = { list: [], count: 0 };
  defaultTableOptions: TableOptionsModel = {
    pageNumber: TableOptionsEnum.PAGE_NUMBER,
    pageSize: TableOptionsEnum.PAGE_SIZE_25,
    filter: [],
    sort: [{ fieldName: 'name', order: 'asc' }]
  };
  settings: TableSettingsModel = {
    entityName: 'eDelivery',
    noRecordsText: 'eDeliveryList',
    hasActions: this.userPermissions[UserPermissionEnum.CRUD_EDELIVERY]
  };

  //used to persist the latest table options value
  _tableOptions: TableOptionsModel;

  constructor(
    private commonDataService: CommonDataService,
    private eDeliveryService: EDeliveryService
  ) {
    super();
  }

  initComponent() {
    const col = this.eDeliveryService.getListColumns(this.userCountry);
    this.columns = col.columns;
    this.filterColumns = col.filterByColumns;
    this.settings = { ...this.settings, hasActions: this.userCountry === 'EC' };
    if (this.saveFilter) {
      this.addSubscription(
        this.commonDataService.filter$.pipe(filter((filterVal) => !filterVal.loading)).subscribe((filterValue) => {
          if (filterValue.value?.type === this.saveFilter) {
            this.filterValues = filterValue.value.filterValue;
            this.setTableOptions({
              ...this._tableOptions,
              pageNumber: TableOptionsEnum.PAGE_NUMBER,
              pageSize: this._tableOptions?.pageSize || this.defaultTableOptions.pageSize,
              filter:
                this.userCountry && this.userCountry !== 'EC'
                  ? [{ fieldName: 'country', fieldValues: [this.userCountry] }, ...filterValue.value.apiParam]
                  : filterValue.value.apiParam,
              sort: filterValue.value.sort ? filterValue.value.sort : []
            });
            this.sortBy = filterValue.value.sort ? filterValue.value.sort[0] : {};
          } else {
            this.setNewCurrentFilter();
          }
        })
      );
    } else {
      this.filterValues = {};
      if (this.sortBy) {
        this.defaultTableOptions.sort = [this.sortBy];
      }
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  private setNewCurrentFilter() {
    this.commonDataService.saveCurrentFilter({
      type: this.saveFilter,
      filterValue: {},
      apiParam: [],
      sort: this.defaultTableOptions.sort
    });
  }

  handleRowAction(event) {
    this.rowAction.emit(event);
  }

  handleFilterChanged(tableOptions: TableOptionsModel, event: any) {
    if (this.saveFilter) {
      if (event.resetAllFilters === false) {
        this.commonDataService.saveCurrentFilter({
          type: this.saveFilter,
          filterValue: { ...event.filterSelection },
          apiParam: event.filter,
          sort: this.sortBy ? [this.sortBy] : this.defaultTableOptions.sort[0]
        });
      } else {
        this.filterValues = { ...event.filterSelection };
      }
    } else {
      this.filterValues = { ...event.filterSelection };
      this.setTableOptions({ ...tableOptions, filter: event.filter, pageNumber: 1 });
    }
  }

  setTableOptions(tableOptions) {
    const newFilter = tableOptions.filter ? [...tableOptions.filter] : [];
    if (this.listFilter) {
      this.listFilter.forEach((item) => {
        const idx = newFilter.findIndex((field) => field.fieldName === item.fieldName);

        if (idx === -1) {
          newFilter.push({ fieldName: item.fieldName, fieldValues: [...item.fieldValues] });
        }
      });
    }

    const updatedTableOptions = { ...tableOptions, filter: newFilter };

    this.eDeliveryService.setTableOption(updatedTableOptions);
  }

  refreshTable() {
    if (this.saveFilter) {
      this.commonDataService.saveCurrentFilter({
        type: this.saveFilter,
        filterValue: {},
        apiParam: []
      });
    } else {
      this.filterValues = {};
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  handleRadioBtnSelect(event) {
    this.rowRadioSelect.emit(event);
  }
}
