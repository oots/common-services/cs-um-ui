import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { Router } from '@angular/router';
import {
  AppFlowEnum,
  BreadcrumbLabelsEnum,
  EDeliveryStatusEnum,
  ListActionsEnum,
  ListFilterEnum,
  UserPermissionEnum
} from 'src/app/core/models/app.enums';
import { BaseComponent } from 'src/app/components/base/base.component';
import { filter } from 'rxjs/operators';
import { BreadcrumbModel } from 'src/app/core/models/app/breadcrumb.model';
import { BreadcrumbService } from 'src/app/services/main/breadcrumb.service';
import { TableDataOptionsModel, TableSettingsModel } from 'src/app/core/models/app/table.options.model';
import { EDeliveryService } from 'src/app/services/e-delivery/e-delivery.service';
import { EuiDialogComponent } from '@eui/components/eui-dialog';

@Component({
  selector: 'app-e-delivery',
  templateUrl: './e-delivery.component.html',
  styleUrls: ['./e-delivery.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EDeliveryComponent extends BaseComponent {
  userCountry;
  userPermissions: any = {};
  breadcrumbItems: BreadcrumbModel[] = [];
  displayTopBtns: any[] = [];
  listFilterEnum = ListFilterEnum;
  tableDataOptions: TableDataOptionsModel;
  tableSettings: TableSettingsModel = { hasFilter: true, hasActions: true, tableClass: 'light', hasRowSelection: true };
  selectedEDelivery;
  dialogButtons = [
    {
      id: ListActionsEnum.DELETE_E_DELIVERY_DISCARD_CHANGES,
      label: 'app.list.actions.cancel',
      class: 'eui-button--outline eui-button--primary eui-u-mr-l'
    },
    {
      id: ListActionsEnum.DELETE_E_DELIVERY,
      label: 'app.list.actions.deleteNode',
      class: 'eui-button--primary'
    }
  ];

  @ViewChild('eDeliveryDialog') eDeliveryDialog: EuiDialogComponent;

  constructor(
    private commonDataService: CommonDataService,
    private userService: UserService,
    private eDeliveryService: EDeliveryService,
    private breadcrumbService: BreadcrumbService,
    private router: Router
  ) {
    super();
  }

  initComponent() {
    this.breadcrumbItems = this.breadcrumbService.getBreadcrumbItems(BreadcrumbLabelsEnum.E_DELIVERY_LIST);

    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !!user?.value && !user.loading)).subscribe((res) => {
        this.userCountry = res.value.country;
        this.userPermissions = this.commonDataService.getUserPermissions(res.value, [UserPermissionEnum.CRUD_EDELIVERY]);

        this.setTopButtons();
      })
    );
  }

  setTopButtons() {
    this.displayTopBtns = [];
    if (this.userPermissions[UserPermissionEnum.CRUD_EDELIVERY]) {
      this.displayTopBtns.push({ id: 1, icon: 'add-circle', outline: 'false', key: 'app.list.eDelivery.buttons.add' });
    }
  }

  handleTopBtnClicked(btn) {
    if (btn.id === 1) {
      this.router.navigate(['./flow/e-delivery']);
    }
  }

  handleRowAction(event) {
    if (event.event === ListActionsEnum.EDIT) {
      this.commonDataService.initializeFlow(AppFlowEnum.CREATE_E_DELIVERY, 0, { eDelivery: event.row });
      this.router.navigate(['./flow/e-delivery']);
    } else if (event.event === ListActionsEnum.TOGGLE) {
      if (event.row.canChangeStatus === true) {
        this.eDeliveryService.changeEDeliveryStatus({
          id: event.row.id,
          status:
            event.row.status === EDeliveryStatusEnum.E_DELIVERY_STATUS_INACTIVE
              ? EDeliveryStatusEnum.E_DELIVERY_STATUS_ACTIVE
              : EDeliveryStatusEnum.E_DELIVERY_STATUS_INACTIVE
        });
      }
    } else if (event.event === ListActionsEnum.DELETE) {
      this.selectedEDelivery = event.row;
      this.eDeliveryDialog?.openDialog();
    }
  }

  handleDialogActions(event) {
    if (event === ListActionsEnum.DELETE_E_DELIVERY_DISCARD_CHANGES) {
      this.selectedEDelivery = null;
      this.eDeliveryDialog.closeDialog();
    } else if (event === ListActionsEnum.DELETE_E_DELIVERY) {
      this.eDeliveryDialog.closeDialog();
      this.eDeliveryService.deleteEDelivery(this.selectedEDelivery.id);
      this.selectedEDelivery = null;
    }
  }
}
