import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEditConfirmationComponent } from './user-edit-confirmation.component';

describe('ConfirmationComponent', () => {
  let component: UserEditConfirmationComponent;
  let fixture: ComponentFixture<UserEditConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserEditConfirmationComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(UserEditConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
