import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user-edit-confirmation',
  templateUrl: './user-edit-confirmation.component.html',
  styleUrl: './user-edit-confirmation.component.scss'
})
export class UserEditConfirmationComponent {
  @Input({ required: true })
  userData: any;
  @Input() initialUserData: any;
  @Input() valid: boolean;

  rolesAreDiff(table: string) {
    // Extract ids from both tables
    const ids1 = this.initialUserData[table].map((item) => item.id).sort();
    const ids2 = this.userData[table].map((item) => item.id).sort();

    // Check if the lengths are the same
    if (ids1.length !== ids2.length) {
      return false;
    }

    // Compare the ids element by element
    return ids1.every((id, index) => id === ids2[index]);
  }
}
