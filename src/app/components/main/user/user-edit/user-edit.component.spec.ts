import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { UserEditComponent } from './user-edit.component';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';
import { UserService } from '../../../../services/user/user.service';
import { StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { ValidationApi } from '../../../../services/main/validation/validation.api';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmationDialogComponent } from '../../common/confirmation-dialog/confirmation-dialog.component';
import { UserEditConfirmationComponent } from './user-edit-confirmation/user-edit-confirmation.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('UserEditComponent', () => {
  let component: UserEditComponent;
  let fixture: ComponentFixture<UserEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserEditComponent, ConfirmationDialogComponent, UserEditConfirmationComponent],
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      providers: [{ provide: CommonDataService }, { provide: ValidationApi }, { provide: UserService }],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(UserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
