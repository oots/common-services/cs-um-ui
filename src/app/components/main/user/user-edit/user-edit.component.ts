import { UserPermissionEnum, UserRoleEnum } from 'src/app/core/models/app.enums';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { Router } from '@angular/router';
import { UserProfileModel } from 'src/app/core/models/response/user.profile';
import { CountryModel } from 'src/app/core/models/response/country.model';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { RoleModel } from '../../../models/user/role.model';
import { FormArray } from '@angular/forms';
import { UserRolesModel } from '../../../../core/models/response/user.model';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { ConfirmationDialogComponent } from '../../common/confirmation-dialog/confirmation-dialog.component';
import { DialogConfigModel, DialogTranslationsModel } from '../../common/confirmation-dialog/confirmation-dialog.model';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] }
  ]
})
export class UserEditComponent extends BaseComponent implements OnChanges, OnDestroy {
  @Input()
  allCountries: CountryModel[] = [];

  @Input() set user(value: UserProfileModel) {
    if (!value) {
      return;
    }
    this.userProfile = value;
    this.userPermissions = this.commonDataService.getUserPermissions(this.userProfile, [
      UserPermissionEnum.CRUD_USER,
      UserPermissionEnum.CRUD_MS_USER
    ]);
  }

  @Input() set form(value: any) {
    this.userForm = value;
    this.userPermissions = this.commonDataService.getUserPermissions(
      this.userProfile,
      [UserPermissionEnum.CRUD_USER, UserPermissionEnum.CRUD_MS_USER, UserPermissionEnum.FE_EDIT_MS_COUNTRY],
      this.userForm.get('country').value
    );
  }

  @Output() public saveUser: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('confirmationDialog') confirmationDialog: ConfirmationDialogComponent;
  canLeavePage = false;
  userForm = null;
  userProfile: UserProfileModel;
  countries: any = [];
  userRoles: any[] = [];
  adminRoles: any[] = [];
  userPermissions = {};
  userPermissionEnum = UserPermissionEnum;
  resubmissionSub: Subscription = new Subscription();
  minDate = new Date();
  maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 40));
  maxMonths = 0;
  myRoles: UserRolesModel[];
  confirmationDialogConfig: DialogConfigModel = {
    showLeftMainButton: true,
    showRightMainButton: true,
    showRightSecondaryButton: false,
    showTitle: true
  };
  confirmationDialogTranslations: DialogTranslationsModel = {
    leftMainButtonLabel: 'app.details.user.confirmation.dialog.back',
    rightMainButtonLabel: 'app.details.user.confirmation.dialog.confirm',
    title: 'app.details.user.confirmation.dialog.title'
  };
  initialUserData = null;

  constructor(
    private router: Router,
    private commonDataService: CommonDataService,
    private translateService: TranslateService
  ) {
    super();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngOnChanges() {
    if (this.userProfile && this.allCountries && this.userForm) {
      if (this.userProfile.country && this.userProfile.country !== 'EC') {
        this.userForm.get('country').setValue(this.userProfile.country);
        this.userForm.get('country').disable();
        this.countries = [
          {
            id: this.userProfile.country,
            label: this.userProfile.countryName,
            iconClass: `'eui-flag-icon eui-flag-icon-${this.userProfile.country.toLowerCase()} eui-u-mr-xs'`
          }
        ];
      } else {
        this.countries = this.allCountries.map((item) => {
          return {
            id: item.code,
            label: item.name,
            iconClass: `eui-flag-icon eui-flag-icon-${item.code !== 'EC' ? item.code.toLowerCase() : 'eu'} eui-u-mr-xs`
          };
        });
      }
      //fill form
      this.userForm.get('countryHelper').setValue(this.countries.find((c) => c.id === this.userForm.get('country').value));
    }
  }

  initComponent() {
    this.addSubscription(
      this.commonDataService.assignableRoles$.subscribe((res) => {
        if (res?.value) {
          this.myRoles = res.value.find((r) => this.userProfile.userId == r.id).roles;
          this.fillRoleFields();
          this.userForm
            .get('userRoles')
            .setValue(this.userRoles.filter((c) => this.userForm.get('roles').value.find((r) => r.id === c.id)));
          this.userForm
            .get('adminRoles')
            .setValue(this.adminRoles.filter((c) => this.userForm.get('roles').value.find((r) => r.id === c.id)));
          this.findMaxDate();
          this.userForm.controls['startDate'].valueChanges.subscribe(() => {
            this.calculateMaxDate();
          });
          this.initialUserData = JSON.parse(JSON.stringify(this.userForm.getRawValue()));
        }
      })
    );
    this.checkUserNoPermissions();
  }

  private getAppRoles(allRoles: UserRolesModel[], type: string) {
    return allRoles
      .filter((r) => r.appId === type)
      .map((i) => {
        return {
          id: i.id,
          label: this.translateService.instant('app.manageUser.role.' + i.roleName),
          roleName: i.roleName,
          maxAllowedDuration: i.maxAllowedDuration
        };
      });
  }

  private checkUserNoPermissions() {
    let hasPermission = false;
    if (this.userPermissions[UserPermissionEnum.CRUD_USER] || this.userPermissions[UserPermissionEnum.CRUD_MS_USER]) {
      hasPermission = true;
    }
    if (!hasPermission) {
      this.router.navigate(['/no-permission']);
    }
  }

  showConfirmationDialog() {
    this.userForm.markAllAsTouched();
    this.continue();
  }

  saveUserData() {
    this.saveUser.emit(this.userForm.getRawValue());
  }

  continue() {
    this.resubmissionSub.unsubscribe();
    if (this.userForm.pending) {
      this.resubmissionSub = this.userForm.statusChanges
        .pipe(
          distinctUntilChanged(),
          first(() => !this.userForm.pending)
        )
        .subscribe(() => {
          if (this.userForm.valid) {
            this.confirmationDialog.openDialog();
          }
        });
      this.addSubscription(this.resubmissionSub);
    } else {
      if (this.userForm.valid) {
        this.confirmationDialog.openDialog();
      }
    }
  }

  isFormChanged() {
    const fields = ['userName', 'email', 'userType', 'country'];
    return fields.filter((field) => this.userForm.controls[field].touched).length > 0;
  }

  canDeactivate() {
    if (this.canLeavePage || !this.isFormChanged()) {
      return true;
    } else if (!this.canLeavePage) {
      this.canLeavePage = true;
      return false;
    }
  }

  addCountry(event) {
    this.userForm.get('country').markAllAsTouched();
    this.userForm.get('country').setValue(event.id);
    this.userForm.get('countryHelper').setValue(null);
    this.userForm.get('countryHelper').setValue(event);
    this.fillRoleFields();
  }

  removeCountry() {
    this.userForm.get('country').markAllAsTouched();
    this.userForm.get('country').setValue(null);
    this.userForm.get('countryHelper').setValue(null);
  }

  addRole(event, roleType) {
    this.userForm.get('roles').markAllAsTouched();
    const role = this.commonDataService.createFormFromJson(new RoleModel(event));
    this.userForm.get('roles').push(role);
    if (!this.userForm.get(roleType).value) {
      this.userForm.get(roleType).setValue([]);
    }
    if (this.userForm.get(roleType).value.filter((i) => i.id === event.id).length === 0) {
      this.userForm.get(roleType).value.push(event);
      this.userForm.get(roleType).setValue(this.userForm.get(roleType).value);
    }
    this.findMaxDate();
  }

  removeRole(event, roleType) {
    this.userForm.get('roles').markAllAsTouched();
    const index = this.userForm.get('roles').value.findIndex((r) => r.id === event.id);
    if (index !== -1) (this.userForm.get('roles') as FormArray).removeAt(index);
    this.userForm.get(roleType).setValue(this.userForm.get(roleType).value.filter((i) => event.id !== i.id));
    this.findMaxDate();
  }

  findMaxDate() {
    this.maxMonths = Math.min.apply(
      null,
      this.userForm
        .get('roles')
        .value.filter((r) => r.maxAllowedDuration != null)
        .map((r) => r.maxAllowedDuration)
    );
    this.calculateMaxDate();
  }

  calculateMaxDate() {
    const today = new Date();
    let maxDate = new Date(today.setFullYear(today.getFullYear() + 40));
    if (this.maxMonths) {
      const dt = new Date(Math.max.apply(null, [new Date(this.userForm.get('startDate').value), new Date()]));
      const endt = new Date(this.userForm.get('endDate').value);
      maxDate = new Date(dt.setMonth(dt.getMonth() + this.maxMonths));
      if (endt > maxDate) {
        this.userForm.get('endDate').setValue(null);
      }
    }
    this.maxDate = maxDate;
  }

  slideToggleChange(event) {
    this.userForm.get('status').setValue(event ? 1 : 0);
  }

  private fillRoleFields() {
    this.userRoles = this.getAppRoles(this.myRoles, '22222222-2222-2222-2222-222222222222');
    this.adminRoles = this.getAppRoles(this.myRoles, '11111111-1111-1111-1111-111111111111');
    if (this.userForm.get('country').value != 'EC') {
      this.userRoles = this.userRoles.filter(
        (r) => r.roleName !== UserRoleEnum.ADMIN_USER_MANAGER_DELEGATE.valueOf() && r.roleName !== UserRoleEnum.EC_USER_MANAGER.valueOf()
      );
      this.adminRoles = this.adminRoles.filter((r) => r.roleName !== UserRoleEnum.REQ_ADMINISTRATOR.valueOf());
    }
  }
}
