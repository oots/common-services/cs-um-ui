import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import {
  TableColumnFilter,
  TableDataOptionsModel,
  TableOptionsModel,
  TableSettingsModel
} from 'src/app/core/models/app/table.options.model';
import { TableOptionsEnum, UserPermissionEnum } from 'src/app/core/models/app.enums';
import { filter, map } from 'rxjs/operators';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { ListBaseComponent } from 'src/app/components/base/list.base.component';
import { Router } from '@angular/router';
import { TableColumnFilterModel } from 'src/app/core/models/app/table.column.model';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: '../../common/data-table/data-table-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserListComponent extends ListBaseComponent {
  @Input()
  public set tableDataOptions(value: TableDataOptionsModel) {
    this.defaultTableOptions = {
      ...this.defaultTableOptions,
      option: value
    };
    if (!this.saveFilter && value) {
      this.filterValues = {};
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  @Input()
  public set pageSize(value: number) {
    this.defaultTableOptions = {
      ...this.defaultTableOptions,
      pageSize: value
    };
  }

  @Input() set tableSettings(value: TableSettingsModel) {
    this.settings = {
      ...value,
      noRecordsText: value.noRecordsText || this.settings.noRecordsText,
      entityName: value.entityName || this.settings.entityName,
      hasActions: this.userPermissions[UserPermissionEnum.CRUD_USER] || this.userPermissions[UserPermissionEnum.CRUD_MS_USER]
    };
  }

  @Input() checkedRowId;
  @Input() saveFilter = null;
  @Input() userPermissions = {};
  @Input() userCountry;
  @Input() userId;
  @Input() roles;
  @Input() listFilter: TableColumnFilter[];
  @Input() sortBy: object;

  @Output() public rowAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() public rowRadioSelect: EventEmitter<any> = new EventEmitter<any>();

  tableOptions$ = this.userService.tableOptions$.pipe(
    map((res) => {
      this._tableOptions = res?.value
        ? {
            ...this.defaultTableOptions,
            ...res.value,
            option: this.defaultTableOptions.option
          }
        : this.defaultTableOptions;
      return this._tableOptions;
    })
  );

  data$ = this.userService.list$.pipe(
    map((res) => {
      const resp = {
        listInfo: res?.value?.list
          ? {
              ...res.value,
              list: res.value.list.map((row) => ({
                ...row,
                sameUser: this.userId === row.id,
                expandedUser: false,
                expandedAdmin: false,
                startDate: this.getDate(row, 'startDate'),
                endDate: this.getDate(row, 'endDate'),
                countryCode: row.country,
                checkedRowId: this.checkedRowId,
                ...this.getUserRoles(row.roles || []),
                listActionBtns: this.userService.getActionsForRow(
                  this.userPermissions,
                  {
                    ...row
                  },
                  this.userCountry
                ),
                canChangeStatus:
                  this.userPermissions[UserPermissionEnum.CRUD_USER] ||
                  (this.userPermissions[UserPermissionEnum.CRUD_MS_USER] && row.country === this.userCountry)
              }))
            }
          : this.defaultTableInfo,
        count: res?.value?.count
      };
      return resp;
    })
  );

  columns: TableColumnFilterModel[];
  filterColumns: TableColumnFilterModel[];
  filterValues: any = {};
  defaultTableInfo = { list: [], count: 0 };
  defaultTableOptions: TableOptionsModel = {
    pageNumber: TableOptionsEnum.PAGE_NUMBER,
    pageSize: TableOptionsEnum.PAGE_SIZE_25,
    filter: [],
    sort: [{ fieldName: 'userName', order: 'asc' }]
  };
  settings: TableSettingsModel = {
    entityName: 'user',
    noRecordsText: 'userList',
    hasActions: this.userPermissions[UserPermissionEnum.CRUD_USER] || this.userPermissions[UserPermissionEnum.CRUD_MS_USER]
  };

  //used to persist the latest table options value
  _tableOptions: TableOptionsModel;

  constructor(
    private commonDataService: CommonDataService,
    private userService: UserService,
    private router: Router
  ) {
    super();
  }

  getUserRoles(roles) {
    const res = {
      userApp: [],
      adminApp: []
    };
    res.adminApp = roles.filter((r) => r.appId === '11111111-1111-1111-1111-111111111111');
    res.userApp = roles.filter((r) => r.appId === '22222222-2222-2222-2222-222222222222');
    return res;
  }

  initComponent() {
    const col = this.userService.getUsersListColumns(this.userCountry, this.roles);
    this.columns = col.columns;
    this.filterColumns = col.filterByColumns;

    if (this.saveFilter) {
      this.addSubscription(
        this.commonDataService.filter$.pipe(filter((filterVal) => !filterVal.loading)).subscribe((filterValue) => {
          if (filterValue.value?.type === this.saveFilter) {
            this.filterValues = filterValue.value.filterValue;
            this.setTableOptions({
              ...this._tableOptions,
              pageNumber: TableOptionsEnum.PAGE_NUMBER,
              pageSize: this._tableOptions?.pageSize || this.defaultTableOptions.pageSize,
              filter: filterValue.value.apiParam,
              sort: filterValue.value.sort ? filterValue.value.sort : []
            });
            this.sortBy = filterValue.value.sort ? filterValue.value.sort[0] : {};
          } else {
            this.setNewCurrentFilter();
          }
        })
      );
    } else {
      this.filterValues = {};
      if (this.sortBy) {
        this.defaultTableOptions.sort = [this.sortBy];
      }
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  private setNewCurrentFilter() {
    this.commonDataService.saveCurrentFilter({
      type: this.saveFilter,
      filterValue: {},
      apiParam: [],
      sort: this.defaultTableOptions.sort
    });
  }

  handleRowAction(event) {
    this.rowAction.emit(event);
  }

  handleFilterChanged(tableOptions: TableOptionsModel, event: any) {
    if (this.saveFilter) {
      if (event.resetAllFilters === false) {
        this.commonDataService.saveCurrentFilter({
          type: this.saveFilter,
          filterValue: { ...event.filterSelection },
          apiParam: event.filter,
          sort: this.sortBy ? [this.sortBy] : this.defaultTableOptions.sort[0]
        });
      } else {
        this.filterValues = { ...event.filterSelection };
      }
    } else {
      this.filterValues = { ...event.filterSelection };
      this.setTableOptions({ ...tableOptions, filter: event.filter, pageNumber: 1 });
    }
  }

  setTableOptions(tableOptions) {
    const newFilter = tableOptions.filter ? [...tableOptions.filter] : [];
    if (this.listFilter) {
      this.listFilter.forEach((item) => {
        const idx = newFilter.findIndex((field) => field.fieldName === item.fieldName);

        if (idx === -1) {
          newFilter.push({ fieldName: item.fieldName, fieldValues: [...item.fieldValues] });
        }
      });
    }

    const updatedTableOptions = { ...tableOptions, filter: newFilter };

    this.userService.setTableOption(updatedTableOptions);
  }

  refreshTable() {
    if (this.saveFilter) {
      this.commonDataService.saveCurrentFilter({
        type: this.saveFilter,
        filterValue: {},
        apiParam: []
      });
    } else {
      this.filterValues = {};
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  handleRadioBtnSelect(event) {
    this.rowRadioSelect.emit(event);
  }

  private getDate(row, attributte) {
    return row[attributte]
      ? row[attributte].lastIndexOf('.') == -1
        ? row[attributte] + '.000Z'
        : row[attributte].substring(0, row[attributte].lastIndexOf('.')) + '.000Z'
      : row[attributte];
  }
}
