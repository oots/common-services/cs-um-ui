import { Component, ViewEncapsulation } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { Router } from '@angular/router';
import {
  AppFlowEnum,
  BreadcrumbLabelsEnum,
  ListActionsEnum,
  ListFilterEnum,
  UserPermissionEnum,
  UserStatusEnum
} from 'src/app/core/models/app.enums';
import { BaseComponent } from 'src/app/components/base/base.component';
import { filter } from 'rxjs/operators';
import { BreadcrumbModel } from 'src/app/core/models/app/breadcrumb.model';
import { BreadcrumbService } from 'src/app/services/main/breadcrumb.service';
import { TableDataOptionsModel, TableSettingsModel } from 'src/app/core/models/app/table.options.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserComponent extends BaseComponent {
  userCountry;
  userId;
  roles;
  userPermissions: any = {};
  breadcrumbItems: BreadcrumbModel[] = [];
  displayTopBtns: any[] = [];
  listFilterEnum = ListFilterEnum;
  tableDataOptions: TableDataOptionsModel;
  tableSettings: TableSettingsModel = { hasFilter: true, hasActions: true, tableClass: 'light', hasRowSelection: true };

  constructor(
    private commonDataService: CommonDataService,
    private userService: UserService,
    private breadcrumbService: BreadcrumbService,
    private router: Router
  ) {
    super();
  }

  initComponent() {
    this.breadcrumbItems = this.breadcrumbService.getBreadcrumbItems(BreadcrumbLabelsEnum.USER_LIST);

    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !!user?.value && !user.loading)).subscribe((res) => {
        this.userCountry = res.value.country;
        this.roles = res.value.roles;
        this.userId = res.value.userId;
        this.userPermissions = this.commonDataService.getUserPermissions(res.value, [
          UserPermissionEnum.CRUD_USER,
          UserPermissionEnum.CRUD_MS_USER
        ]);

        this.setTopButtons();
      })
    );
  }

  setTopButtons() {
    this.displayTopBtns = [];
    if (this.userPermissions[UserPermissionEnum.CRUD_USER] || this.userPermissions[UserPermissionEnum.CRUD_MS_USER]) {
      this.displayTopBtns.push({ id: 1, icon: 'add-circle', outline: 'false', key: 'app.list.user.buttons.add' });
    }
  }

  handleTopBtnClicked(btn) {
    if (btn.id === 1) {
      this.router.navigate(['./flow/user']);
    }
  }

  handleRowAction(event) {
    if (event.event === ListActionsEnum.EDIT) {
      this.commonDataService.initializeFlow(AppFlowEnum.CREATE_USER, 0, { user: event.row });
      this.router.navigate(['./flow/user']);
    } else if (event.event === ListActionsEnum.TOGGLE) {
      if (event.row.canChangeStatus === true) {
        this.userService.changeUserStatus({
          id: event.row.id,
          status:
            event.row.status === UserStatusEnum.USER_STATUS_INACTIVE
              ? UserStatusEnum.USER_STATUS_ACTIVE
              : UserStatusEnum.USER_STATUS_INACTIVE
        });
      }
    }
  }
}
