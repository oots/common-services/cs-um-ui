import { ComponentFixture, ComponentFixtureAutoDetect, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { UserFlowComponent } from './user-flow.component';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';
import { UserService } from '../../../../services/user/user.service';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { UserModel } from '../../../../core/models/response/user.model';
import { EuiDialogComponent } from '@eui/components/eui-dialog';
import { AppFlowEnum, ListActionsEnum, UserPermissionEnum } from '../../../../core/models/app.enums';
import { FlowModel, FlowStepModel } from '../../../../core/models/app/flow.model';
import { UserProfileModel } from '../../../../core/models/response/user.profile';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AsyncStoreObject } from '../../../../core/reducers/cs.utils';
import { ActionConfirmationModel } from '../../../../core/models/response/action.confirmation.model';
import { FlowTemplateComponent } from '../../../flow/flow-template/flow-template.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { StubUserEditComponent } from '../../../../testing/stubs.config';
import { ValidationApi } from '../../../../services/main/validation/validation.api';
import { HttpClientModule } from '@angular/common/http';
import { CountryModel } from '../../../../core/models/response/country.model';
import { FormGroup } from '@angular/forms';

describe('UserFlowComponent', () => {
  let component: UserFlowComponent,
    fixture: ComponentFixture<UserFlowComponent>,
    element: DebugElement,
    router: Router,
    spyUserService: any;

  beforeEach(() => {
    spyUserService = jasmine.createSpyObj('UserService', ['resetConfirmationAction', 'saveUser']);

    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      declarations: [UserFlowComponent, FlowTemplateComponent, StubUserEditComponent],
      providers: [
        { provide: CommonDataService },
        { provide: UserService, useValue: spyUserService },
        { provide: ValidationApi },
        { provide: UserService },
        { provide: ComponentFixtureAutoDetect, useValue: true } //used to trigger changeDetection automatically
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFlowComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  describe('initialisation', () => {
    it('should create', () => {
      // ASSERT
      expect(component).toBeTruthy();
    });
  });

  describe('ngOnDestroy', () => {
    it('should set isLeaving to true', () => {
      // ACT
      component.ngOnDestroy();
      // ASSERT
      expect(component.isLeaving).toBeTrue();
    });

    it('should call ngOnDestroy of parent class', () => {
      // ARRANGE
      spyOn(component, 'ngOnDestroy');
      // ACT
      component.ngOnDestroy();
      // ASSERT
      expect(component.ngOnDestroy).toHaveBeenCalled();
    });

    it('should call ngOnDestroy of parent class', () => {
      // ARRANGE
      spyOn(component, 'ngOnDestroy');
      // ACT
      component.ngOnDestroy();
      // ASSERT
      expect(component.ngOnDestroy).toHaveBeenCalled();
    });

    it('should call resetFlow of commonDataService if goToConfirmationPage is falsy', () => {
      // ARRANGE
      spyOn(component['commonDataService'], 'resetFlow');
      component.goToConfirmationPage = false;
      // ACT
      component.ngOnDestroy();
      // ASSERT
      expect(component['commonDataService'].resetFlow).toHaveBeenCalled();
    });

    it('should not call resetFlow of commonDataService if goToConfirmationPage is truthy', () => {
      // ARRANGE
      spyOn(component['commonDataService'], 'resetFlow');
      component.goToConfirmationPage = true;
      // ACT
      component.ngOnDestroy();
      // ASSERT
      expect(component['commonDataService'].resetFlow).not.toHaveBeenCalled();
    });
  });

  describe('handleSaveUser', () => {
    it('should call setWizardError and saveUser of UserService', () => {
      // ARRANGE
      const mockEvent = {} as UserModel;
      spyOn(component, 'setWizardError');
      spyOn(component['userService'], 'saveUser');
      // ACT
      component.handleSaveUser(mockEvent);
      // ASSERT
      expect(component.setWizardError).toHaveBeenCalled();
      expect(component['userService'].saveUser).toHaveBeenCalledWith(mockEvent);
    });
  });

  describe('handleDiscardChanges', () => {
    const redirectLink = '/main/user';
    beforeEach(() => {
      spyOn(component, 'openFlowDialog');
      spyOn(router, 'navigate');
    });

    it('should navigate to "/main/user" if checkStep === "exit" ', () => {
      // ARRANGE
      const checkStep = 'exit';
      // ACT
      component.handleDiscardChanges(checkStep);
      // ASSERT
      expect(component['router'].navigate).toHaveBeenCalledWith([redirectLink]);
      expect(component.openFlowDialog).not.toHaveBeenCalled();
    });

    it('should call openFlowDialog if checkStep !== "exit" ', () => {
      // ARRANGE
      const checkStep = '';
      // ACT
      component.handleDiscardChanges(checkStep);
      // ASSERT
      expect(component['router'].navigate).not.toHaveBeenCalled();
      expect(component.openFlowDialog).toHaveBeenCalledWith('exitProcess');
    });

    it('should call openFlowDialog if handleDiscardChanges is called without input ', () => {
      // ARRANGE
      // ACT
      component.handleDiscardChanges();
      // ASSERT
      expect(component['router'].navigate).not.toHaveBeenCalled();
      expect(component.openFlowDialog).toHaveBeenCalledWith('exitProcess');
    });
  });

  describe('closeFlowDialog', () => {
    it('should call closeDialog of flowDialog and set dialogName==="" ', () => {
      // ARRANGE
      component['flowDialog'] = {
        closeDialog: () => {}
      } as EuiDialogComponent;
      spyOn(component['flowDialog'], 'closeDialog');
      // ACT
      component.closeFlowDialog();
      // ASSERT
      expect(component['flowDialog'].closeDialog).toHaveBeenCalled();
      expect(component.dialogName).toEqual('');
    });
  });

  describe('openFlowDialog', () => {
    beforeEach(() => {
      component['flowDialog'] = {
        openDialog: () => {}
      } as EuiDialogComponent;
      spyOn(component['flowDialog'], 'openDialog');
    });
    it('should set correct values if actionType === "exitProcess"', () => {
      // ARRANGE
      const actionType = 'exitProcess';
      const dialogButtonsValue = [
        {
          id: ListActionsEnum.FLOW1_CANCEL_EXIT_PROCESS,
          label: 'app.flow.1.buttons.cancelExitProcess',
          class: 'eui-buttons eui-u-mr-l'
        },
        {
          id: ListActionsEnum.FLOW1_CONFIRM_EXIT_PROCESS,
          label: 'app.flow.1.buttons.confirmExitProcess',
          class: 'eui-button--primary'
        }
      ];
      // ACT
      component.openFlowDialog(actionType);
      // ASSERT
      expect(component.actionType).toEqual(actionType);
      expect(component.dialogLeftButtons).toEqual([]);
      expect(component.dialogName).toEqual('app.flow.1.popup.exitProcessTitle');
      expect(component.displayComponent).toEqual('confirmMessage');
      expect(component.dialogButtons).toEqual(dialogButtonsValue);
    });

    it('should set correct values if actionType !== "exitProcess"', () => {
      // ARRANGE
      const actionType = '';
      // ACT
      component.openFlowDialog(actionType);
      // ASSERT
      expect(component.actionType).toEqual(actionType);
      expect(component.displayComponent).toEqual(null);
      expect(component.dialogButtons).toEqual([]);
      expect(component.dialogLeftButtons).toEqual([]);
    });

    it('should call openDialog of flowDialog', () => {
      // ARRANGE
      const actionType = '';
      // ACT
      component.openFlowDialog(actionType);
      // ASSERT
      expect(component['flowDialog'].openDialog).toHaveBeenCalled();
    });
  });

  describe('handleDialogActions', () => {
    it('should call closeFlowDialog if event===ListActionsEnum.FLOW1_CANCEL_EXIT_PROCESS ', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_CANCEL_EXIT_PROCESS;
      spyOn(component, 'closeFlowDialog');
      // ACT
      component.handleDialogActions(mockEvent);
      // ASSERT
      expect(component.closeFlowDialog).toHaveBeenCalled();
    });

    it('should call closeFlowDialog and handleDiscardChanges if event===ListActionsEnum.FLOW1_CONFIRM_EXIT_PROCESS ', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_CONFIRM_EXIT_PROCESS;
      spyOn(component, 'closeFlowDialog');
      spyOn(component, 'handleDiscardChanges');
      // ACT
      component.handleDialogActions(mockEvent);
      // ASSERT
      expect(component.closeFlowDialog).toHaveBeenCalled();
      expect(component.handleDiscardChanges).toHaveBeenCalledWith('exit');
    });

    it('should not call any method', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.SELECT;
      spyOn(component, 'closeFlowDialog');
      spyOn(component, 'handleDiscardChanges');
      // ACT
      component.handleDialogActions(mockEvent);
      // ASSERT
      expect(component.closeFlowDialog).not.toHaveBeenCalled();
      expect(component.handleDiscardChanges).not.toHaveBeenCalled();
    });
  });

  describe('handleWizardActions', () => {
    beforeEach(() => {
      component['editUserComponent'] = {
        saveUserData: () => {},
        showConfirmationDialog: () => {}
      } as UserEditComponent;
      spyOn(component['editUserComponent'], 'saveUserData');
      spyOn(component['editUserComponent'], 'showConfirmationDialog');
      spyOn(component, 'setWizardError');
      spyOn(component, 'handleDiscardChanges');
      spyOn(component, 'openFlowDialog');
    });
    it('should call setWizardError and set errorKeys = {}', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_CANCEL_EXIT_PROCESS;
      // ACT
      component.handleWizardActions(mockEvent);
      // ASSERT
      expect(component.errorKeys).toEqual({});
      expect(component.setWizardError).toHaveBeenCalled();
    });

    it('should call handleDiscardChanges if event === ListActionsEnum.FLOW1_DISCARD', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_DISCARD;
      // ACT
      component.handleWizardActions(mockEvent);
      // ASSERT
      expect(component.errorKeys).toEqual({});
      expect(component.setWizardError).toHaveBeenCalled();
      expect(component.handleDiscardChanges).toHaveBeenCalled();
    });

    it('should call saveUserData of editUserComponent if event === ListActionsEnum.FLOW1_ADD', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_ADD;
      // ACT
      component.handleWizardActions(mockEvent);
      // ASSERT
      expect(component.setWizardError).toHaveBeenCalled();
      expect(component.editUserComponent.showConfirmationDialog).toHaveBeenCalled();
    });

    it('should call setWizardError if protocolVersions is falsy', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_CONFIRM;
      const mockProtocolVersions = {
        data: {
          user: {
            protocolVersions: false
          }
        }
      } as FlowModel;
      component.flowData = mockProtocolVersions;
      // ACT
      component.handleWizardActions(mockEvent);
      // ASSERT
      expect(component.setWizardError).toHaveBeenCalledWith('app.flow.1.step.0.error.requiredVersions');
      expect(component.setWizardError).toHaveBeenCalledTimes(2);
    });

    it('should call setWizardError if protocolVersions length === 0', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_CONFIRM;
      const mockProtocolVersions = {
        data: {
          user: {
            protocolVersions: []
          }
        }
      } as FlowModel;
      component.flowData = mockProtocolVersions;
      // ACT
      component.handleWizardActions(mockEvent);
      // ASSERT
      expect(component.setWizardError).toHaveBeenCalledWith('app.flow.1.step.0.error.requiredVersions');
      expect(component.setWizardError).toHaveBeenCalledTimes(2);
    });

    it('should call setWizardError if protocolVersions length === 0', () => {
      // ARRANGE
      const mockEvent = ListActionsEnum.FLOW1_CONFIRM;
      const mockProtocolVersions = {
        data: {
          user: {
            protocolVersions: true
          }
        }
      } as FlowModel;
      component.flowData = mockProtocolVersions;
      // ACT
      component.handleWizardActions(mockEvent);
      // ASSERT
      expect(component.setWizardError).not.toHaveBeenCalledWith('app.flow.1.step.0.error.requiredVersions');
      expect(component.openFlowDialog).toHaveBeenCalledWith('confirmationPopup');
      expect(component.setWizardError).toHaveBeenCalledTimes(1);
    });
  });

  describe('setWizardError', () => {
    beforeEach(() => {
      component.wizardData = {
        lButtons: [],
        buttons: [],
        rButtons: []
      };
    });
    it('should create', () => {
      // ARRANGE
      const mockKey = 'test';
      const expectedResult = {
        lButtons: [],
        buttons: [],
        rButtons: [],
        errMessageKey: 'test'
      };
      // ACT
      component.setWizardError(mockKey);
      // ASSERT
      expect(component.wizardData).toEqual(expectedResult);
    });

    it('should create', () => {
      // ARRANGE
      const expectedResult = {
        lButtons: [],
        buttons: [],
        rButtons: [],
        errMessageKey: undefined
      };
      // ACT
      component.setWizardError();
      // ASSERT
      expect(component.wizardData).toEqual(expectedResult);
    });
  });

  describe('handleStepChanged', () => {
    beforeEach(() => {
      component.wizardData = {
        lButtons: [],
        buttons: ['test'],
        rButtons: []
      };
    });
    it('should set wizardData correctly when event === 0 and flowData.data.user.id not null', () => {
      // ARRANGE
      const mockEvent = 0;
      const mockActiveStep = 1;
      const mockWizardDataSteps = 'mockWizardDataSteps';
      const expectedResult = {
        lButtons: [
          {
            id: ListActionsEnum.FLOW1_DISCARD,
            label: 'app.list.actions.exitProcess',
            leftIcon: 'eui-close',
            class: 'eui-button--basic eui-button--primary'
          }
        ],
        rButtons: [
          {
            id: ListActionsEnum.FLOW1_ADD,
            label: 'app.list.actions.continue',
            class: 'eui-button--primary'
          }
        ],
        buttons: [],
        steps: mockWizardDataSteps,
        activeStep: mockActiveStep
      };

      component.wizardData['steps'] = mockWizardDataSteps;
      component.flowData = {
        data: {
          user: {
            id: '111'
          }
        },
        activeStep: mockActiveStep
      } as FlowModel;
      // ACT
      component.handleStepChanged(mockEvent);
      // ASSERT
      expect(component.wizardData).toEqual(expectedResult);
    });

    it('should set wizardData correctly when event === 0 and flowData.data.user.id = 0', () => {
      // ARRANGE
      const mockEvent = 0;
      const mockActiveStep = 1;
      const mockWizardDataSteps = 'mockWizardDataSteps';
      const expectedResult = {
        lButtons: [
          {
            id: ListActionsEnum.FLOW1_DISCARD,
            label: 'app.list.actions.exitProcess',
            leftIcon: 'eui-close',
            class: 'eui-button--basic eui-button--primary'
          }
        ],
        rButtons: [
          {
            id: ListActionsEnum.FLOW1_ADD,
            label: 'app.list.actions.continue',
            class: 'eui-button--primary'
          }
        ],
        buttons: [],
        steps: mockWizardDataSteps,
        activeStep: mockActiveStep
      };

      component.wizardData['steps'] = mockWizardDataSteps;
      component.flowData = {
        data: {
          user: {
            id: null
          }
        },
        activeStep: mockActiveStep
      } as FlowModel;
      // ACT
      component.handleStepChanged(mockEvent);
      // ASSERT
      expect(component.wizardData).toEqual(expectedResult);
    });

    it('should set wizardData correctly when event !== 0 and wizardData.steps is truthy', () => {
      // ARRANGE
      const mockEvent = 1;
      const mockActiveStep = 1;
      const mockWizardDataSteps = 'mockWizardDataSteps';
      const expectedResult = {
        lButtons: [],
        rButtons: [],
        buttons: [],
        steps: mockWizardDataSteps,
        activeStep: mockActiveStep
      };

      component.wizardData['steps'] = mockWizardDataSteps;
      component.flowData = {
        data: {
          user: {
            id: '111'
          }
        },
        activeStep: mockActiveStep
      } as FlowModel;
      // ACT
      component.handleStepChanged(mockEvent);
      // ASSERT
      expect(component.wizardData).toEqual(expectedResult);
    });

    it('should set wizardData correctly when event !== 0 and wizardData.steps is falsy', () => {
      // ARRANGE
      const mockEvent = 1;
      const mockActiveStep = 1;
      const mockFlowDataSteps = {};
      const expectedResult = {
        lButtons: [],
        rButtons: [],
        buttons: [],
        steps: [mockFlowDataSteps],
        activeStep: mockActiveStep
      };

      component.wizardData['steps'] = null;
      component.flowData = {
        data: {
          user: {
            id: '111'
          }
        },
        activeStep: mockActiveStep,
        steps: [mockFlowDataSteps] as FlowStepModel[]
      } as FlowModel;
      // ACT
      component.handleStepChanged(mockEvent);
      // ASSERT
      expect(component.wizardData).toEqual(expectedResult);
    });
  });

  describe('initFlowStep1', () => {
    it('should call window.scrollTo if activeStep !== flow.activeStep', () => {
      // ARRANGE
      const mockFlowData = {
        activeStep: 4,
        data: {
          user: {
            id: '111'
          }
        }
      } as FlowModel;
      spyOn(window, 'scrollTo');
      component.flowData = { activeStep: 5 } as FlowModel;
      component.userProfile = {};
      // ACT
      component.initFlowStep1(mockFlowData);
      // ASSERT
      expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
    });

    it('should call window.scrollTo if activeStep === flow.activeStep', () => {
      // ARRANGE
      const mockFlowData = {
        activeStep: 4,
        data: {
          user: {
            id: '111'
          }
        }
      } as FlowModel;
      spyOn(window, 'scrollTo');
      component.flowData = { activeStep: 4 } as FlowModel;
      component.userProfile = {};
      // ACT
      component.initFlowStep1(mockFlowData);
      // ASSERT
      expect(window.scrollTo).not.toHaveBeenCalled();
    });

    it('should call handleStepChanged with activeStep', () => {
      // ARRANGE
      const mockFlowData = {
        activeStep: 5,
        data: {
          user: {
            id: '111'
          }
        }
      } as FlowModel;
      spyOn(component, 'handleStepChanged');
      component.userProfile = {} as UserProfileModel;
      // ACT
      component.initFlowStep1(mockFlowData);
      // ASSERT
      expect(component.handleStepChanged).toHaveBeenCalledWith(5);
    });

    it('should set flowData correctly if id>1', () => {
      // ARRANGE
      const mockFlowData = {
        activeStep: 4,
        data: {
          user: {
            id: '111'
          }
        }
      } as FlowModel;
      const expectedFlowData = {
        ...mockFlowData,
        editFlow: true
      };
      component.flowData = { activeStep: 4 } as FlowModel;
      component.userProfile = {};
      // ACT
      component.initFlowStep1(mockFlowData);
      // ASSERT
      expect(component.flowData).toEqual(expectedFlowData);
    });

    it('should set flowData correctly if id===0', () => {
      // ARRANGE
      const mockFlowData = {
        activeStep: 4,
        data: {
          user: {
            id: null
          }
        }
      } as FlowModel;
      const expectedFlowData = {
        ...mockFlowData,
        editFlow: false
      };
      component.flowData = { activeStep: 4 } as FlowModel;
      component.userProfile = {};
      // ACT
      component.initFlowStep1(mockFlowData);
      // ASSERT
      expect(component.flowData).toEqual(expectedFlowData);
    });

    it('should set userPermissions correctly if id>0', () => {
      // ARRANGE
      const mockFlowData = {
        activeStep: 4,
        data: {
          user: {
            id: '111',
            owner: 'test'
          }
        }
      } as FlowModel;
      component.userProfile = {} as UserProfileModel;
      spyOn(component['commonDataService'], 'getUserPermissions');
      // ACT
      component.initFlowStep1(mockFlowData);
      // ASSERT'
      expect(component['commonDataService'].getUserPermissions).toHaveBeenCalledWith(
        {},
        [UserPermissionEnum.CRUD_USER, UserPermissionEnum.FE_EDIT_MS_COUNTRY],
        'test'
      );
    });

    it('should set userPermissions correctly if id>0', () => {
      // ARRANGE
      const mockFlowData = {
        activeStep: 4,
        data: {
          user: {
            id: null
          }
        }
      } as FlowModel;
      component.userProfile = {};
      spyOn(component['commonDataService'], 'getUserPermissions');
      // ACT
      component.initFlowStep1(mockFlowData);
      // ASSERT
      expect(component['commonDataService'].getUserPermissions).not.toHaveBeenCalled();
    });
  });

  describe('initComponent', () => {
    beforeEach(() => {
      component['userService'].profile$ = of({ value: {} as UserProfileModel } as AsyncStoreObject<UserProfileModel>);
      component['userService'].actionConfirmation$ = of({} as AsyncStoreObject<ActionConfirmationModel>);
      component['commonDataService'].flow$ = of({} as AsyncStoreObject<FlowModel>);
      component['commonDataService'].countries$ = of({} as AsyncStoreObject<CountryModel[]>);
      // spyOn(component['commonDataService'], 'handleActionConfirmation').and.returnValue({ action: 1, errorKey: true });
      spyOn(component, 'initFlowStep1');
      spyOn(router, 'navigate');
    });
    it('should call getUserPermissions and set userProfile', () => {
      spyOn(component['commonDataService'], 'getUserPermissions');
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['userProfile']).toEqual({});
      expect(component['commonDataService'].getUserPermissions).toHaveBeenCalledWith({}, [
        UserPermissionEnum.CRUD_USER,
        UserPermissionEnum.FE_EDIT_MS_COUNTRY
      ]);
    });

    it('should not call getUserPermissions and not set userProfile if value of profile$ is falsy', () => {
      // ARRANGE
      component['userService'].profile$ = of({} as AsyncStoreObject<UserProfileModel>);
      // ACT
      component.initComponent();
      spyOn(component['commonDataService'], 'getUserPermissions');
      // ASSERT
      expect(component['userProfile']).toBeUndefined();
      expect(component['commonDataService'].getUserPermissions).not.toHaveBeenCalled();
    });

    it('should call nothing if isLeaving is truthy', () => {
      // ARRANGE
      component['commonDataService'].flow$ = of({} as AsyncStoreObject<FlowModel>);
      component.isLeaving = true;
      spyOn(component['commonDataService'], 'initializeFlow');
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['commonDataService'].initializeFlow).not.toHaveBeenCalled();
      expect(component.initFlowStep1).not.toHaveBeenCalled();
    });

    it('should call nothing if flow$ is falsy', () => {
      // ARRANGE
      component['commonDataService'].flow$ = of(null);
      component.isLeaving = true;
      spyOn(component['commonDataService'], 'initializeFlow');
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['commonDataService'].initializeFlow).not.toHaveBeenCalled();
      expect(component.initFlowStep1).not.toHaveBeenCalled();
    });

    it('should call initializeFlow if value is falsy', () => {
      // ARRANGE
      component.isLeaving = false;
      spyOn(component['commonDataService'], 'initializeFlow');
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['commonDataService'].initializeFlow).toHaveBeenCalledWith(AppFlowEnum.CREATE_USER);
    });

    it('should call initFlowStep1 if value is truthy', () => {
      // ARRANGE
      component.isLeaving = false;
      component['commonDataService'].flow$ = of({ value: {} } as AsyncStoreObject<FlowModel>);
      // ACT
      component.initComponent();
      // ASSERT
      expect(component.initFlowStep1).toHaveBeenCalledWith({});
    });

    it('should call handleActionConfirmation and resetConfirmationAction', () => {
      // ARRANGE
      spyOn(component['commonDataService'], 'handleActionConfirmation').and.returnValue({ action: '1', errorKey: false, value: 1 });
      spyOn(component['userService'], 'resetConfirmationAction');
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['commonDataService'].handleActionConfirmation).toHaveBeenCalled();
      expect(component['userService'].resetConfirmationAction).toHaveBeenCalled();
    });

    it('should call navigate if action is truthy and errorKey is falsy', () => {
      // ARRANGE
      component.form = new FormGroup<any>({});
      spyOn(component['commonDataService'], 'handleActionConfirmation').and.returnValue({ action: '1', errorKey: false, value: 1 });
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['router'].navigate).toHaveBeenCalledWith(['main/confirmation']);
    });

    it('should not call navigate if action is falsy and errorKey is falsy', () => {
      // ARRANGE
      spyOn(component['commonDataService'], 'handleActionConfirmation').and.returnValue({ action: '0', errorKey: false, value: 1 });
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['router'].navigate).not.toHaveBeenCalled();
    });

    it('should not call navigate if action is truthy and errorKey is truthy', () => {
      // ARRANGE
      spyOn(component['commonDataService'], 'handleActionConfirmation').and.returnValue({ action: '1', errorKey: true, value: 1 });
      // ACT
      component.initComponent();
      // ASSERT
      expect(component['router'].navigate).not.toHaveBeenCalled();
    });

    it('should set navigateUrl if event is instanceof NavigationStart', fakeAsync(() => {
      // ARRANGE
      const mocURL = 'http://localhost:4200/#/';

      component['router'] = { events: of(new NavigationStart(0, mocURL)) } as Router;
      fixture.detectChanges();
      // ACT
      component.initComponent();
      tick();
      // ASSERT
      expect(component.navigateUrl).toEqual(mocURL);
    }));

    it('should not set navigateUrl if event is not instanceof NavigationStart', () => {
      // ARRANGE
      const mockNavigationStart = { url: 'test' } as NavigationEnd;
      component['router'] = { events: of(mockNavigationStart) } as Router;
      // ACT
      component.initComponent();
      // ASSERT
      expect(component.navigateUrl).toBeUndefined();
    });
  });

  describe('UI testing', () => {
    it('should check if app-flow-template exists in the UI', () => {
      // ASSERT
      expect(element.queryAll(By.directive(FlowTemplateComponent))).toBeTruthy();
    });

    it('should check if app-user-edit exists in the UI', fakeAsync(() => {
      // ASSERT
      expect(element.queryAll(By.directive(UserEditComponent))).toBeTruthy();
    }));
  });
});
