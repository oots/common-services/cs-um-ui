import { ActionConfirmationEnum, AppFlowEnum, ListActionsEnum, UserPermissionEnum } from 'src/app/core/models/app.enums';
import { NavigationStart, Router } from '@angular/router';
import { Component, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { EuiDialogComponent } from '@eui/components/eui-dialog';
import { FlowModel } from 'src/app/core/models/app/flow.model';
import { ListActionButtonModel } from 'src/app/core/models/response/list.action.button.model';
import { UserProfileModel } from 'src/app/core/models/response/user.profile';
import { UserService } from 'src/app/services/user/user.service';
import { filter } from 'rxjs/operators';
import { UserEditComponent } from 'src/app/components/main/user/user-edit/user-edit.component';
import { CountryModel } from 'src/app/core/models/response/country.model';
import { FormGroup } from '@angular/forms';
import { UserWrapperModel } from '../../../models/user/user-wrapper.model';

@Component({
  selector: 'app-user-flow',
  templateUrl: './user-flow.component.html',
  styleUrls: ['./user-flow.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserFlowComponent extends BaseComponent implements OnDestroy {
  navigateUrl: string;
  flowData: FlowModel;
  wizardData: any = {
    lButtons: [],
    buttons: [],
    rButtons: []
  };
  isLeaving = false;
  goToConfirmationPage = false;
  errorKeys: any = {};
  displayComponent: string;
  listActionsEnum = ListActionsEnum;
  actionType: string;
  dialogName: string;
  dialogButtons: ListActionButtonModel[] = [];
  dialogLeftButtons: ListActionButtonModel[] = [];
  userProfile: UserProfileModel;
  userPermissions: any = {};
  allCountries: CountryModel[] = [];
  form: FormGroup;

  @ViewChild('flowDialog') flowDialog: EuiDialogComponent;
  @ViewChild('editUserComponent') editUserComponent: UserEditComponent;

  constructor(
    private router: Router,
    private userService: UserService,
    private commonDataService: CommonDataService
  ) {
    super();
  }

  ngOnDestroy() {
    this.isLeaving = true;
    if (!this.goToConfirmationPage) {
      this.commonDataService.resetFlow();
    }
    super.ngOnDestroy();
  }

  initComponent() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.navigateUrl = event.url;
      }
    });

    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !!user?.value && !user.loading)).subscribe((res) => {
        this.userProfile = res.value;
        this.userPermissions = this.commonDataService.getUserPermissions(this.userProfile, [
          UserPermissionEnum.CRUD_USER,
          UserPermissionEnum.FE_EDIT_MS_COUNTRY
        ]);
      })
    );

    this.addSubscription(
      this.commonDataService.countries$.subscribe((res) => {
        if (res?.value) {
          this.allCountries = res.value;
        }
      })
    );

    this.addSubscription(
      this.commonDataService.flow$.subscribe((flow) => {
        if (this.isLeaving || !flow) {
          return;
        }
        if (!flow.value) {
          this.commonDataService.initializeFlow(AppFlowEnum.CREATE_USER);
        } else {
          this.initFlowStep1(flow.value);
        }
      })
    );

    this.addSubscription(
      this.userService.actionConfirmation$.subscribe((confirmation) => {
        const result = this.commonDataService.handleActionConfirmation(confirmation, [ActionConfirmationEnum.USER_SAVE]);
        if (result.action && !result.errorKey && this.form) {
          const savedUser = this.form.getRawValue();
          this.commonDataService.setFlow({ ...this.flowData, data: savedUser });
          this.goToConfirmationPage = true;
          this.router.navigate(['main/confirmation']);
        }
        this.userService.resetConfirmationAction();
      })
    );
  }

  initFlowStep1(flow) {
    this.form = this.commonDataService.createFormFromJson(new UserWrapperModel(flow.data?.user));
    if (flow.data?.user.startDate == null) {
      this.form.get('user').get('startDate').setValue(new Date());
    }
    if (this.userProfile.userId === flow.data?.user.id) {
      this.form.get('user').get('statusHelper').disable();
    }
    const activeStep = this.flowData?.activeStep || 0;
    const editFlow = !!flow.data?.user?.id;
    if (activeStep !== flow.activeStep) {
      window.scrollTo(0, 0);
    }
    this.flowData = {
      ...flow,
      editFlow
    };
    if (editFlow === true) {
      this.userPermissions = this.commonDataService.getUserPermissions(
        this.userProfile,
        [UserPermissionEnum.CRUD_USER, UserPermissionEnum.FE_EDIT_MS_COUNTRY],
        flow.data.user.owner
      );
    }
    this.handleStepChanged(this.flowData.activeStep);
  }

  handleStepChanged(event) {
    let wizardButtons = {
      lButtons: [],
      rButtons: [],
      buttons: []
    };

    if (event === 0) {
      wizardButtons = {
        ...wizardButtons,
        lButtons: [
          {
            id: ListActionsEnum.FLOW1_DISCARD,
            label: 'app.list.actions.exitProcess',
            leftIcon: 'eui-close',
            class: 'eui-button--basic eui-button--primary'
          }
        ],
        rButtons: [
          {
            id: ListActionsEnum.FLOW1_ADD,
            label: 'app.list.actions.continue',
            class: 'eui-button--primary'
          }
        ]
      };
    }

    this.wizardData = {
      ...wizardButtons,
      steps: this.wizardData.steps || this.flowData.steps,
      activeStep: this.flowData.activeStep
    };
  }

  setWizardError(key = undefined) {
    this.wizardData = {
      ...this.wizardData,
      errMessageKey: key
    };
  }

  handleWizardActions(event) {
    this.errorKeys = {};
    this.setWizardError();
    switch (event) {
      case ListActionsEnum.FLOW1_DISCARD:
        this.handleDiscardChanges();
        break;
      case ListActionsEnum.FLOW1_ADD:
        this.editUserComponent.showConfirmationDialog();
        break;
      case ListActionsEnum.FLOW1_CONFIRM:
        if (!this.flowData.data.user.protocolVersions || this.flowData.data.user.protocolVersions.length === 0) {
          this.setWizardError('app.flow.1.step.0.error.requiredVersions');
        } else {
          this.openFlowDialog('confirmationPopup');
        }
        break;
    }
  }

  handleDialogActions(event) {
    if (event === ListActionsEnum.FLOW1_CANCEL_EXIT_PROCESS) {
      this.closeFlowDialog();
    } else if (event === ListActionsEnum.FLOW1_CONFIRM_EXIT_PROCESS) {
      this.closeFlowDialog();
      this.handleDiscardChanges('exit');
    }
  }

  openFlowDialog(actionType) {
    this.actionType = actionType;
    this.displayComponent = null;
    this.dialogButtons = [];
    this.dialogLeftButtons = [];
    if (this.actionType === 'exitProcess') {
      this.dialogName = 'app.flow.1.popup.exitProcessTitle';
      this.displayComponent = 'confirmMessage';
      this.dialogButtons = [
        {
          id: ListActionsEnum.FLOW1_CANCEL_EXIT_PROCESS,
          label: 'app.flow.1.buttons.cancelExitProcess',
          class: 'eui-buttons eui-u-mr-l'
        },
        {
          id: ListActionsEnum.FLOW1_CONFIRM_EXIT_PROCESS,
          label: 'app.flow.1.buttons.confirmExitProcess',
          class: 'eui-button--primary'
        }
      ];
    }
    this.flowDialog.openDialog();
  }

  closeFlowDialog() {
    this.flowDialog.closeDialog();
    this.dialogName = '';
  }

  handleDiscardChanges(checkStep = 'confirm') {
    if (checkStep === 'exit') {
      this.router.navigate(['/main/user']);
    } else {
      this.openFlowDialog('exitProcess');
    }
  }

  handleSaveUser(event) {
    this.setWizardError();
    this.userService.saveUser(event);
  }
}
