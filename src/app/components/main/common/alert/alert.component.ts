import { Component, Input, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from '../../../base/base.component';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AlertComponent extends BaseComponent {
  @Input() titleKey = 'app.error.notfound.header';
  @Input() headerKey = 'app.error.notfound.text';
  @Input() links: any[] = [
    {
      url: '/main',
      labelKey: 'app.error.goToHomePage'
    }
  ];
}
