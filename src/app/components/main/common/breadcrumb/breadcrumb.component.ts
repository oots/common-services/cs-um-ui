import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from '../../../base/base.component';
import { BreadcrumbModel } from 'src/app/core/models/app/breadcrumb.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BreadcrumbComponent extends BaseComponent {
  @Input() items: BreadcrumbModel[];
  @Output() public itemClicked: EventEmitter<any> = new EventEmitter<any>();

  constructor(private router: Router) {
    super();
  }

  trackByFn(index) {
    return index;
  }

  handleButtonClick(url: string) {
    this.router.navigate([url]);
  }
}
