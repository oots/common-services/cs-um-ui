import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { BreadcrumbComponent } from './breadcrumb.component';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [BreadcrumbComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate on button click', () => {
    router.navigate = jasmine.createSpy();
    fixture.detectChanges();

    component.handleButtonClick('/main');

    expect(router.navigate).toHaveBeenCalledWith(['/main']);
  });

  it('should return index in the trackByFn', () => {
    const index = 1;
    const trackByFunctionResult = component.trackByFn(index);

    expect(trackByFunctionResult).toEqual(index);
  });
});
