import { Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { EventTypeEnum, TableColumnFilterTypeEnum, UserStatusEnum } from 'src/app/core/models/app.enums';
import { TableColumnFilter, TableExternalFilter } from 'src/app/core/models/app/table.options.model';
import { BaseComponent } from 'src/app/components/base/base.component';
import { TranslateService } from '@ngx-translate/core';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { TreeDataModel } from '@eui/components/eui-tree';
import { FormControl } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] }
  ]
})
export class TableFilterComponent extends BaseComponent {
  @Input() filter: TableColumnFilter[] = [];
  @Input() columns: any[] = [];
  @Input() filterValues: any = {};
  @Input() design: string = '';
  @Input() align: string = 'top';
  @Input() hasECCountry = true;
  @Output() public filterChanged: EventEmitter<TableExternalFilter> = new EventEmitter<TableExternalFilter>();
  @ViewChild('countriesTree') public countriesTree: any;

  filterTypeEnum = TableColumnFilterTypeEnum;
  chipsList: any[] = [];
  lifeEventsChipsList: any[] = [];
  levelCodesChipsList: any[] = [];
  countries: any[] = [];
  statuses: any[] = [
    { id: UserStatusEnum.USER_STATUS_ACTIVE, name: this.translateService.instant('app.data.user.status_1') },
    { id: UserStatusEnum.USER_STATUS_INACTIVE, name: this.translateService.instant('app.data.user.status_0') }
  ];
  eventTypes: any[] = [
    { id: EventTypeEnum.USER_CREATED, name: this.translateService.instant('app.list.event.table.status.USER_CREATED') },
    { id: EventTypeEnum.USER_UPDATED_DETAILS, name: this.translateService.instant('app.list.event.table.status.USER_UPDATED_DETAILS') },
    { id: EventTypeEnum.USER_UPDATED_ROLES, name: this.translateService.instant('app.list.event.table.status.USER_UPDATED_ROLES') },
    { id: EventTypeEnum.USER_DISABLED, name: this.translateService.instant('app.list.event.table.status.USER_DISABLED') },
    { id: EventTypeEnum.USER_ENABLED, name: this.translateService.instant('app.list.event.table.status.USER_ENABLED') },
    { id: EventTypeEnum.USER_UPDATED_DATES, name: this.translateService.instant('app.list.event.table.status.USER_UPDATED_DATES') },
    { id: EventTypeEnum.EDELIVERY_CREATED, name: this.translateService.instant('app.list.event.table.status.EDELIVERY_CREATED') },
    {
      id: EventTypeEnum.EDELIVERY_UPDATED_DETAILS,
      name: this.translateService.instant('app.list.event.table.status.EDELIVERY_UPDATED_DETAILS')
    },
    { id: EventTypeEnum.EDELIVERY_DISABLED, name: this.translateService.instant('app.list.event.table.status.EDELIVERY_DISABLED') },
    { id: EventTypeEnum.EDELIVERY_ENABLED, name: this.translateService.instant('app.list.event.table.status.EDELIVERY_ENABLED') }
  ];
  countriesList: any[] = [];
  statusList: any[] = [];
  eventTypeList: any[] = [];
  resetAllFilters = false;
  statusTreeData: TreeDataModel = [];
  eventTypeTreeData: TreeDataModel = [];
  countryTreeData: TreeDataModel = [];
  countryFilterInputControl = new FormControl();
  allSelected = false;

  constructor(
    private commonDataService: CommonDataService,
    private translateService: TranslateService
  ) {
    super();
  }

  initComponent() {
    this.setStatusDropdownValues();
    this.setEventTypeDropdownValues();
    this.resetStatusFilter(true);
    this.resetEventTypeFilter(true);
    this.subscribeToCountriesList();
    this.subscribeToSearchControlChanges();
  }

  resetCountriesFilter(setChips = false) {
    this.countriesList = [
      ...this.countries.map((country) => ({
        id: country.code,
        label: country.name,
        iconClass:
          country.code?.toLowerCase() === 'ec'
            ? 'eui-flag-icon eui-flag-icon-eu'
            : 'eui-flag-icon eui-flag-icon-'.concat(country.code?.toLowerCase())
      }))
    ];

    const initialFilterValue =
      this.filterValues.jurisdictionCountries ||
      this.filterValues.ownerCountries ||
      this.filterValues.countryCodeCountries ||
      this.filterValues.countryCountries;
    this.chipsList =
      setChips && initialFilterValue ? this.countriesList.filter((country) => initialFilterValue.indexOf(country.id) > -1) : [];
    this.countriesList = this.countriesList.filter((country) => !this.chipsList.find((chip) => chip.id === country.id));
    this.resetCountryTreeData();
  }

  resetStatusFilter(setChips = false) {
    const statusObj = this.filterValues.statusStatus;
    this.statusList = [
      ...this.eventTypes.map((e) => ({
        id: e.id,
        label: e.name
      }))
    ];
    this.chipsList = setChips && statusObj ? this.statusList.filter((status) => statusObj.indexOf(status.id) > -1) : [];
    this.resetStatusTreeData();
  }

  resetEventTypeFilter(setChips = false) {
    const statusObj = this.filterValues.statusStatus;
    this.eventTypeList = [
      ...this.statuses.map((status) => ({
        id: status.id,
        label: status.name
      }))
    ];
    this.chipsList = setChips && statusObj ? this.eventTypeList.filter((status) => statusObj.indexOf(status.id) > -1) : [];
    this.resetEventTypeTreeData();
  }

  changeFilterValue(col: any, event: any, sufix: string = 'Value') {
    if (!col) {
      return;
    }
    const res: TableExternalFilter = { filter: [], filterSelection: {}, resetAllFilters: this.resetAllFilters };
    let filterSelection = { ...this.filterValues };

    switch (col.filterType) {
      case TableColumnFilterTypeEnum.DB_SELECT:
        filterSelection = {
          ...filterSelection,
          [col.filterId.concat(sufix)]: event?.value
        };
        break;
      case TableColumnFilterTypeEnum.INPUT:
      case TableColumnFilterTypeEnum.SELECT:
        filterSelection = {
          ...filterSelection,
          [col.filterId.concat(sufix)]: event
        };
        break;
      case TableColumnFilterTypeEnum.TIMESTAMP:
        filterSelection = {
          ...filterSelection,
          [col.filterId.concat(sufix)]: event
        };
        break;
      case TableColumnFilterTypeEnum.COUNTRIES:
      case TableColumnFilterTypeEnum.STATUS:
      case TableColumnFilterTypeEnum.EVENT_TYPE:
        filterSelection = {
          ...filterSelection,
          [col.filterId.concat(sufix)]: this.chipsList.length > 0 ? this.chipsList.map((chip) => chip.id) : null
        };
        break;
    }

    Object.keys(filterSelection)
      .filter((key) => !!filterSelection[key])
      .forEach((objKey) => {
        res.filterSelection[objKey] = filterSelection[objKey];
      });

    const endsWith = ['Value', 'start', 'end', 'lifeEvents', 'countries', 'levelCodes', 'status', 'eventType'];

    Object.keys(res.filterSelection).forEach((key) => {
      const sufixValue = endsWith.find((item) => key.endsWith(item));
      let fieldValue = res.filterSelection[key];
      switch (sufixValue) {
        case 'Value':
          fieldValue = [fieldValue];
          res.filter.push({
            fieldName: key.replace(sufixValue, ''),
            fieldValues: fieldValue
          });
          break;
        case 'start':
        case 'end':
          const filterId = key.replace(sufixValue, '');
          const startValue = res.filterSelection[filterId.concat('start')];
          const endValue = res.filterSelection[filterId.concat('end')];
          const f = res.filter.filter((f) => f.fieldName === filterId);
          if (f.length > 0) {
            f[0].fieldValues = [startValue, endValue].map((item) => item || '');
          } else {
            res.filter.push({
              fieldName: filterId,
              fieldValues: [startValue, endValue].map((item) => item || '')
            });
          }
          break;
        case 'lifeEvents':
        case 'countries':
        case 'levelCodes':
        case 'status':
        case 'eventType':
          const fieldName = key.replace(sufixValue, '');
          res.filter.push({
            fieldName,
            fieldValues: fieldValue
          });
          break;
      }
    });
    this.filterChanged.emit(res);
  }

  trackByFn(index) {
    return index;
  }

  getFilterNewValues(filterType: string, valueObj) {
    let result;
    if (filterType === TableColumnFilterTypeEnum.DB_SELECT) {
      result = valueObj?.value ? [valueObj.value] : [];
    } else {
      result = !valueObj || valueObj === '' ? [] : [valueObj];
    }

    return result ? result : [];
  }

  private formatIntervalDate(dateParam: any): string {
    const date = new Date(dateParam);
    if (!dateParam || !(date instanceof Date)) {
      return null;
    }

    if (date < new Date('2022-01-01')) {
      return null;
    }

    const month = date.getMonth() + 1;
    return date
      .getFullYear()
      .toString()
      .concat(month > 9 ? '-' : '-0')
      .concat(month.toString())
      .concat(date.getDate() > 9 ? '-' : '-0')
      .concat(date.getDate().toString());
  }

  onRemoveChip(event) {
    this.chipsList = [...this.chipsList.filter((chip) => chip.id !== event.id)];
    switch (event.type) {
      case TableColumnFilterTypeEnum.COUNTRY:
        this.resetCountryData();
        break;
      case TableColumnFilterTypeEnum.STATUS:
        this.resetStatusData();
        break;
      case TableColumnFilterTypeEnum.EVENT_TYPE:
        this.resetEventTypeData();
        break;
    }
  }

  onRemoveAll() {
    this.chipsList = [];
    this.countriesList = [...this.countriesList].sort((a, b) => (a.label < b.label ? -1 : 1));
    this.resetCountryTreeData();
    this.statusList = [...this.statusList].sort((a, b) => (a.label < b.label ? -1 : 1));
    this.eventTypeList = [...this.eventTypeList].sort((a, b) => (a.label < b.label ? -1 : 1));
    this.resetStatusTreeData();
    this.resetEventTypeTreeData();
    this.filterChanged.emit({ filter: [], filterSelection: {}, resetAllFilters: false });
  }

  private resetCountryData() {
    this.countriesList = [...this.countriesList].sort((a, b) => (a.label < b.label ? -1 : 1));
    this.resetCountryTreeData();
    this.changeFilterValue(
      this.columns.find((col) => col.filterType === TableColumnFilterTypeEnum.COUNTRIES),
      '',
      'countries'
    );
  }

  private resetStatusData() {
    this.statusList = [...this.statusList].sort((a, b) => (a.label < b.label ? -1 : 1));
    this.resetStatusTreeData();
    this.changeFilterValue(
      this.columns.find((col) => col.filterType === TableColumnFilterTypeEnum.STATUS),
      '',
      'status'
    );
  }
  private resetEventTypeData() {
    this.resetEventTypeTreeData();
    this.changeFilterValue(
      this.columns.find((col) => col.filterType === TableColumnFilterTypeEnum.EVENT_TYPE),
      '',
      'eventType'
    );
  }

  handleCountriesSelection(event) {
    if (event) {
      const addedId = event?.added[0]?.node.treeContentBlock.id;
      const removedId = event?.removed[0]?.node.treeContentBlock.id;

      if (addedId === 'select-all') {
        this.countriesTree?.setAllSelection(true);
        this.allSelected = true;
      } else if (removedId === 'select-all') {
        this.countriesTree?.setAllSelection(false);
      } else if (this.allSelected) {
        this.updateChipsList(event.selection.filter((item) => item.node.treeContentBlock.id !== 'select-all'));
      } else {
        if (event.added.length) {
          this.addToChipsList(event.added[0].node.treeContentBlock);
        } else if (event.removed.length) {
          this.removeFromChipsList(event.removed[0].node.treeContentBlock);
        }
      }
    }
  }

  listSelection(event, filter) {
    if (event?.added.length) {
      this.chipsList.push(event.added[0].node.treeContentBlock);
    } else if (event?.removed.length) {
      this.chipsList = [...this.chipsList.filter((chip) => chip.id !== event?.removed[0].node.treeContentBlock.id)];
    }

    this.changeFilterValue(
      this.columns.find((col) => col.filterType === filter),
      '',
      filter
    );
  }

  getSelectedDate(event: Date, filter) {
    this.changeFilterValue(
      this.columns.find((col) => col.filterType === TableColumnFilterTypeEnum.TIMESTAMP),
      event,
      filter
    );
  }

  private setStatusDropdownValues() {
    this.statusTreeData = this.statuses.map((status) => ({
      node: {
        treeContentBlock: {
          id: status.id,
          label: status.name,
          type: 'status'
        }
      }
    }));
  }

  private setEventTypeDropdownValues() {
    this.eventTypeTreeData = this.eventTypes.map((e) => ({
      node: {
        treeContentBlock: {
          id: e.id,
          label: e.name,
          type: 'eventType'
        }
      }
    }));
  }

  private addToChipsList(block: any) {
    this.chipsList.push(block);
    this.changeFilterValue(
      this.columns.find((col) => col.filterType === TableColumnFilterTypeEnum.COUNTRIES),
      '',
      'countries'
    );
  }

  private updateChipsList(selection: any[]) {
    this.chipsList = selection.map((item) => item.node.treeContentBlock);
    this.changeFilterValue(
      this.columns.find((col) => col.filterType === TableColumnFilterTypeEnum.COUNTRIES),
      '',
      'countries'
    );
  }

  private removeFromChipsList(block: any) {
    this.chipsList = this.chipsList.filter((chip) => chip.id !== block.id);
    this.changeFilterValue(
      this.columns.find((col) => col.filterType === TableColumnFilterTypeEnum.COUNTRIES),
      '',
      'countries'
    );
  }

  private subscribeToSearchControlChanges() {
    this.countryFilterInputControl.valueChanges.subscribe((val) => {
      this.countriesTree?.filterTerm(val);
    });
  }

  private subscribeToCountriesList() {
    this.addSubscription(
      this.commonDataService.countries$.subscribe((res) => {
        if (res?.value) {
          this.countries = res.value;
          if (!this.hasECCountry) {
            this.countries = this.countries.filter((country) => country.code !== 'EC');
          }
          const selectAllNode = {
            node: {
              treeContentBlock: {
                id: 'select-all',
                label: 'Select All',
                type: 'selectAll'
              }
            }
          };
          this.countryTreeData = [
            selectAllNode,
            ...this.countries.map((country) => {
              return {
                node: {
                  treeContentBlock: {
                    id: country.code,
                    label: country.name,
                    type: 'country',
                    iconClass:
                      country.code?.toLowerCase() === 'ec'
                        ? 'eui-flag-icon eui-flag-icon-eu'
                        : 'eui-flag-icon eui-flag-icon-'.concat(country.code?.toLowerCase())
                  }
                }
              };
            })
          ];
          this.resetCountriesFilter(true);
        }
      })
    );
  }

  private resetCountryTreeData() {
    this.countryTreeData = this.countryTreeData.map((item) => {
      return {
        node: {
          ...item.node,
          isSelected: this.chipsList.find((chip) => chip.id === item.node.treeContentBlock.id)
        }
      };
    });
  }

  private resetStatusTreeData() {
    this.statusTreeData = this.statusTreeData.map((item) => {
      return {
        node: {
          ...item.node,
          isSelected: this.chipsList.find((chip) => chip.id === item.node.treeContentBlock.id)
        }
      };
    });
  }
  private resetEventTypeTreeData() {
    this.eventTypeTreeData = this.eventTypeTreeData.map((item) => {
      return {
        node: {
          ...item.node,
          isSelected: this.chipsList.find((chip) => chip.id === item.node.treeContentBlock.id)
        }
      };
    });
  }

  getCount(type) {
    return this.chipsList.filter((v) => v.type === type).length;
  }
}
