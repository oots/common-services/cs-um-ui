import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextColumnComponent } from './text-column.component';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('TextColumnComponent', () => {
  let component: TextColumnComponent;
  let fixture: ComponentFixture<TextColumnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TextColumnComponent],
      imports: [TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(TextColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
