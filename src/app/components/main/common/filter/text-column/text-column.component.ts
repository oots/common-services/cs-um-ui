import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { TableColumnFilterModel } from 'src/app/core/models/app/table.column.model';

@Component({
  selector: 'app-text-column',
  templateUrl: './text-column.component.html',
  styleUrls: ['./text-column.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TextColumnComponent extends BaseComponent {
  @Input() column: TableColumnFilterModel;
  @Input() design: string = '';
  @Output() public componentValueChange: EventEmitter<string> = new EventEmitter<string>();
  model: string;

  constructor() {
    super();
  }

  @Input() set value(value: string) {
    this.model = value ? value : '';
  }

  handleValueChange(event: string) {
    this.componentValueChange.emit(event);
  }
}
