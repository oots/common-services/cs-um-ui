import { Component, Input, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from '../../../base/base.component';
import { StatusEnum } from 'src/app/core/models/app.enums';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StatusComponent extends BaseComponent {
  @Input() status: string;
  statusEnum = StatusEnum;

  constructor() {
    super();
  }
}
