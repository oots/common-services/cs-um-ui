import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-form-err-msg',
  templateUrl: './form-err-msg.component.html',
  styleUrls: ['./form-err-msg.component.scss']
})
export class FormErrMsgComponent {
  @Input() condition = false;
  @Input() messageKey;

  constructor() {}
}
