import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormErrMsgComponent } from './form-err-msg.component';

describe('FormErrMsgComponent', () => {
  let component: FormErrMsgComponent;
  let fixture: ComponentFixture<FormErrMsgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormErrMsgComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(FormErrMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
