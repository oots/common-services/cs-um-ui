import { Component, Input, ViewEncapsulation } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

import { BaseComponent } from '../../../base/base.component';
import { MenuService } from '../../../../services/main/menu.service';
import { UserProfileModel } from 'src/app/core/models/response/user.profile';
import { UserService } from 'src/app/services/user/user.service';
import { MainMenuItemsEnum, UserPermissionEnum, UserTypesEnum } from 'src/app/core/models/app.enums';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { filter } from 'rxjs';

@Component({
  selector: 'app-top-menu-bar',
  templateUrl: './top-menu-bar.component.html',
  styleUrls: ['./top-menu-bar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopMenuBarComponent extends BaseComponent {
  @Input() displayMenu: boolean = true;
  @Input() displayUserMenu: boolean = true;

  activeIndex = 0;
  userTypesEnum = UserTypesEnum;
  userProfile: UserProfileModel;
  navigateUrl: string;

  menuItems = [
    {
      label: MainMenuItemsEnum.MENU_DASHBOARD,
      url: './'
    },
    {
      label: MainMenuItemsEnum.MENU_USER,
      url: './user'
    },
    {
      label: MainMenuItemsEnum.MENU_E_DELIVERY,
      url: './e-delivery'
    },
    {
      label: MainMenuItemsEnum.MENU_EVENTS,
      url: './events'
    }
  ];

  constructor(
    private menuService: MenuService,
    private userService: UserService,
    private commonDataService: CommonDataService,
    private router: Router
  ) {
    super();
  }

  initComponent() {
    this.activeIndex = this.menuService.getActiveIndex(this.router.url);
    this.addSubscription(
      this.router.events.subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.navigateUrl = event.url;
          this.activeIndex = this.menuService.getActiveIndex(this.navigateUrl);
        }
      })
    );

    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !user.loading)).subscribe((userProfile) => {
        if (userProfile.value) {
          const userPermissions = this.commonDataService.getUserPermissions(userProfile.value, [UserPermissionEnum.USER_APP_VIEW]);
          if (userPermissions[UserPermissionEnum.USER_APP_VIEW]) {
            this.userProfile = userProfile.value;
          }
        }
      })
    );
  }

  logout() {
    document.cookie = 'SESSION=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    this.router.navigate(['/login']);
  }

  trackByFn(index) {
    return index;
  }
}
