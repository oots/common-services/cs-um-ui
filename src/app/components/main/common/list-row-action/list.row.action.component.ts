import { Component, EventEmitter, Input, Output } from '@angular/core';

import { BaseComponent } from '../../../base/base.component';
import { ListActionButtonModel } from 'src/app/core/models/response/list.action.button.model';
import { ListActionsEnum } from 'src/app/core/models/app.enums';

@Component({
  selector: 'app-list-row-action',
  templateUrl: './list.row.action.component.html',
  styleUrls: ['./list.row.action.component.scss']
})
export class ListRowActionComponent extends BaseComponent {
  @Input() buttons: ListActionButtonModel[] = [];
  @Output() public btnClicked: EventEmitter<string> = new EventEmitter<string>();

  actions = ListActionsEnum;

  constructor() {
    super();
  }

  handleBtnClicked(btn: ListActionButtonModel) {
    if (!btn.disabled) {
      this.btnClicked.emit(btn.id);
    }
  }

  trackByFn(index) {
    return index;
  }
}
