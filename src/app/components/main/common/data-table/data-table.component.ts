import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { ListBaseComponent } from 'src/app/components/base/list.base.component';
import { DateFormatEnum, ListActionsEnum } from 'src/app/core/models/app.enums';
import { TableOptionsModel, TableSettingsModel } from 'src/app/core/models/app/table.options.model';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from '../../../../services/user/user.service';
import { countriesData } from '../../../../core/config/constants.config';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DataTableComponent extends ListBaseComponent {
  @Input() set tableData(value: any) {
    if (value) {
      const checkedRow = value.listInfo.list.find((row) => !!row.checkedRowId && row.id === row.checkedRowId);
      setTimeout(() => {
        if (checkedRow) {
          this.selectionForm.controls.radioBtn.setValue(checkedRow.checkedRowId?.toString());
        } else {
          this.selectionForm.controls.radioBtn.setValue(undefined);
        }
      });

      this.data = value;
    }
  }

  data: any;
  tableSettings: TableSettingsModel = {
    addBtnKey: 'add',
    hasFilter: false,
    hasActions: false,
    hasAddBtn: false,
    expandableRows: false,
    noRecordsText: 'default',
    entityName: '',
    labelName: '',
    info: '',
    hidePaginator: false,
    hasLink: false,
    hasRadioBtn: false,
    countryName: '',
    countryCode: '',
    sortDataList: false,
    tableClass: '',
    filterPosition: 'top',
    hasRowSelection: false
  };

  @Input() tableOptions: TableOptionsModel = this.defaultTableOptions;
  @Input() tableColumns: any = [];
  @Input() sortBy: any = {};

  @Input() tableFilterColumns: any = [];
  @Input() tableFilterValues: any = {};

  @Output() public filterChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() public rowActionBtnClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() public addBtnClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() public linkBtnClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() public tableOptionsChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() public radioBtnChecked: EventEmitter<any> = new EventEmitter<any>();
  @Output() public sortDataList: EventEmitter<any> = new EventEmitter<any>();

  dateFormat = DateFormatEnum;
  selectedRowId = 0;
  idSchemes;

  @Input() set settings(value: TableSettingsModel) {
    if (value) {
      const settings = { ...this.tableSettings };
      Object.keys(value).forEach((key) => {
        if (value[key] !== undefined) {
          settings[key] = value[key];
        }
      });
      this.tableSettings = { ...settings };
    }
  }

  selectionForm = new FormGroup({
    radioBtn: new FormControl(null)
  });

  constructor(
    public userService: UserService,
    private commonDataService: CommonDataService
  ) {
    super();
  }

  initComponent() {
    this.addSubscription(
      this.selectionForm.controls.radioBtn.valueChanges.subscribe((ctrl) => {
        this.radioBtnChecked.emit(this.data?.listInfo?.list?.find((row) => ctrl === row.id?.toString()));
      })
    );

    this.addSubscription(
      this.commonDataService.schemes$.subscribe((res) => {
        if (res?.value) {
          this.idSchemes = res.value.map((item) => ({ ...item, text: item.url }));
        }
      })
    );
  }

  handleFilterChanged(event) {
    this.filterChanged.emit(event);
  }

  handleAddBtnClicked() {
    this.addBtnClicked.emit();
  }

  handleLinkBtnClicked() {
    this.linkBtnClicked.emit();
  }

  handleDTSelectRow(row) {
    if (this.tableSettings.hasRowSelection) {
      this.selectedRowId = this.selectedRowId === row.id ? 0 : row.id;
      this.handleDTRowAction(ListActionsEnum.SELECT, row);
    }
  }

  handleDTRowAction(event, row) {
    this.rowActionBtnClicked.emit({ event, row });
  }

  protected override setTableOptions(tableOptions) {
    this.selectionForm.controls.radioBtn.setValue(null);
    this.tableOptionsChanged.emit(tableOptions);
  }

  toggleDescription(row: any) {
    row.expanded = !row.expanded;
  }

  onSortChange(event: any, col: string, tableOptions: TableOptionsModel) {
    if (this.tableSettings.sortDataList === true) {
      const sort = this.calculateNextSorting(col, tableOptions.sort)[0]?.order || 'asc';
      this.sortDataList.emit({ sort, col, tableOptions });
    } else {
      super.onSortChange(event, col, tableOptions);
    }
  }

  handleGoDetailsClicked(row) {
    this.rowActionBtnClicked.emit({ event: ListActionsEnum.VIEW, row });
  }

  handleDTDetailsRow(row) {
    row.opened = !row.opened;
  }

  handleToggleChanged(row) {
    if (!row.sameUser && !this.userService.expiredDate(row.endDate)) {
      this.rowActionBtnClicked.emit({ event: ListActionsEnum.TOGGLE, row });
    }
  }

  toggleDomains(row, event, column) {
    event.stopPropagation();
    row[column] = !row[column];
  }

  getCountryName(code) {
    return countriesData.find((a) => a.code === code).name;
  }

  getSchema(id) {
    return this.idSchemes.find((item) => item.id === id);
  }
}
