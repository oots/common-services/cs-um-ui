import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToogleRowComponent } from './toogle-row.component';
import { StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { CommonDataService } from '../../../../../services/main/common-data/common-data.service';
import { ValidationApi } from '../../../../../services/main/validation/validation.api';
import { UserService } from '../../../../../services/user/user.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

describe('ToogleRowComponent', () => {
  let component: ToogleRowComponent;
  let fixture: ComponentFixture<ToogleRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ToogleRowComponent],
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      providers: [{ provide: CommonDataService }, { provide: ValidationApi }, { provide: UserService }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(ToogleRowComponent);
    component = fixture.componentInstance;
    component.col = { id: 'test' };
    component.row = {};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
