import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../../../../../services/user/user.service';

@Component({
  selector: 'app-toogle-row',
  templateUrl: './toogle-row.component.html',
  styleUrl: './toogle-row.component.scss'
})
export class ToogleRowComponent implements OnInit, OnChanges {
  @Input() row;
  @Input() col;

  @ViewChild('toogle') toogle;

  public form: FormGroup;
  listUpdate = 0;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      slideControl: {
        value: this.row[this.col.id] === 1,
        disabled: this.row['sameUser'] || this.userService.expiredDate(this.row.endDate) || !this.row['canChangeStatus']
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.row) {
      if (this.form && this.form.get('slideControl') && this.row['canChangeStatus']) {
        this.listUpdate++;
        if (
          changes.row.currentValue.status !== changes.row.previousValue.status ||
          this.toogle.id != this.row.id ||
          (changes.row.currentValue.status !== this.form.get('slideControl').value && this.listUpdate == 2)
        ) {
          this.form.get('slideControl').patchValue(this.row[this.col.id] === 1);
          this.listUpdate = 0;
        }
        if (this.row['sameUser'] || this.userService.expiredDate(this.row.endDate)) {
          this.form.get('slideControl').disable();
        } else {
          this.form.get('slideControl').enable();
        }
      }
    }
  }
}
