export interface DialogTranslationsModel {
  title: string;
  rightMainButtonLabel?: string;
  rightSecondaryButtonLabel?: string;
  leftMainButtonLabel?: string;
}

export interface DialogConfigModel {
  showTitle: boolean;
  showRightMainButton: boolean;
  showRightSecondaryButton: boolean;
  showLeftMainButton: boolean;
}
