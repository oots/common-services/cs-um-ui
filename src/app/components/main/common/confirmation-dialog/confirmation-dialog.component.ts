import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { EuiDialogComponent } from '@eui/components/eui-dialog';
import { DialogConfigModel, DialogTranslationsModel } from './confirmation-dialog.model';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrl: './confirmation-dialog.component.scss'
})
export class ConfirmationDialogComponent {
  @Input() config: DialogConfigModel;
  @Input() translations: DialogTranslationsModel;
  @Output() accept: EventEmitter<void> = new EventEmitter();
  @ViewChild('confirmation') confirmDialog: EuiDialogComponent;

  public openDialog() {
    this.confirmDialog.openDialog();
  }

  public closeDialog() {
    this.confirmDialog.closeDialog();
  }

  protected onAccept() {
    this.confirmDialog.closeDialog();
    this.accept.emit();
  }
}
