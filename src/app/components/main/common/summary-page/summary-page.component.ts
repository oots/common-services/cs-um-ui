import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../../../base/base.component';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';
import { FlowModel } from 'src/app/core/models/app/flow.model';

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrl: './summary-page.component.scss'
})
export class SummaryPageComponent extends BaseComponent implements OnInit, OnDestroy {
  flow: FlowModel;

  constructor(
    private router: Router,
    private commonDataService: CommonDataService
  ) {
    super();
  }

  initComponent() {
    this.addSubscription(
      this.commonDataService.flow$.subscribe((flow) => {
        this.flow = flow.value;
        if (!this.flow) {
          this.router.navigate(['main']);
        }
      })
    );
  }
  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.commonDataService.resetFlow();
  }

  handleSummeryButton() {
    this.router.navigate(['main/user']);
  }
  handleNextAction(url) {
    this.router.navigate([url]);
  }
}
