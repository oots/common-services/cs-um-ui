import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../../../base/base.component';
import { I18nService } from '@eui/core';
import { ListActionButtonModel } from 'src/app/core/models/response/list.action.button.model';
import { ListActionsEnum } from 'src/app/core/models/app.enums';
import { UserProfileModel } from 'src/app/core/models/response/user.profile';
import { UserService } from 'src/app/services/user/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-top-user-bar',
  templateUrl: './top-user-bar.component.html',
  styleUrls: ['./top-user-bar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopUserBarComponent extends BaseComponent {
  language$ = this.i18nService.getState().pipe(
    map((language: any) => {
      return language?.activeLang || 'en';
    })
  );

  @Input() displayUserMenu: boolean = true;

  userProfile: UserProfileModel;
  listActionsEnum = ListActionsEnum;
  dialogButtons: ListActionButtonModel[] = [];

  constructor(
    private i18nService: I18nService,
    private userService: UserService,
    private router: Router
  ) {
    super();
  }

  initComponent() {
    this.addSubscription(
      this.userService.profile$.subscribe((userProfile) => {
        if (!userProfile?.value) {
          return;
        }
        this.userProfile = userProfile.value;
      })
    );
  }

  logout() {
    this.router.navigate(['/login']);
  }

  eraseCookie(name) {
    document.cookie = name + '=; Path=/user-mgmt/api; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }
}
