import { UserPermissionEnum } from '../../core/models/app.enums';
import { Component, OnDestroy } from '@angular/core';

import { BaseComponent } from '../base/base.component';
import { CommonDataService } from '../../services/main/common-data/common-data.service';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent extends BaseComponent implements OnDestroy {
  constructor(
    private router: Router,
    private userService: UserService,
    private commonDataService: CommonDataService
  ) {
    super();
  }

  ngOnDestroy() {
    this.commonDataService.setConfirmationAction(null);
    super.ngOnDestroy();
  }

  initComponent(): void {
    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !!user?.value && !user.loading)).subscribe((res) => {
        const userPermissions = this.commonDataService.getUserPermissions(res.value, [UserPermissionEnum.USER_APP_VIEW]);
        if (!userPermissions[UserPermissionEnum.USER_APP_VIEW]) {
          this.router.navigate(['/no-permission']);
        } else {
          this.commonDataService.getCountriesList();
        }
      })
    );
  }
}
