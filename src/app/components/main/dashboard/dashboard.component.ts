import { Component, ViewEncapsulation } from '@angular/core';

import { BaseComponent } from '../../base/base.component';
import { UserService } from 'src/app/services/user/user.service';
import { UserTypesEnum } from 'src/app/core/models/app.enums';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent extends BaseComponent {
  profile$ = this.userService.profile$.pipe(
    map((res) => {
      if (!res && !res.value) {
        return null;
      }
      return res.value;
    })
  );

  version = environment.version;
  userTypesEnum = UserTypesEnum;

  constructor(private userService: UserService) {
    super();
  }
}
