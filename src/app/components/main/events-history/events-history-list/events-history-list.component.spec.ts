import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EventsHistoryListComponent } from './events-history-list.component';
import { StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';
import { ValidationApi } from '../../../../services/main/validation/validation.api';
import { UserService } from '../../../../services/user/user.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { EventService } from '../../../../services/events/event.service';

describe('EventsHistoryListComponent', () => {
  let component: EventsHistoryListComponent;
  let fixture: ComponentFixture<EventsHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventsHistoryListComponent],
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      providers: [{ provide: CommonDataService }, { provide: ValidationApi }, { provide: UserService }, { provide: EventService }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(EventsHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
