import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ListBaseComponent } from '../../../base/list.base.component';
import {
  TableColumnFilter,
  TableDataOptionsModel,
  TableOptionsModel,
  TableSettingsModel
} from '../../../../core/models/app/table.options.model';
import { EventListColumnsEnum, TableOptionsEnum } from '../../../../core/models/app.enums';
import { filter, map } from 'rxjs/operators';
import { TableColumnFilterModel } from '../../../../core/models/app/table.column.model';
import { CommonDataService } from '../../../../services/main/common-data/common-data.service';
import { Router } from '@angular/router';
import { EventService } from '../../../../services/events/event.service';

@Component({
  selector: 'app-events-history-list',
  templateUrl: '../../common/data-table/data-table-list.component.html',
  styleUrl: './events-history-list.component.scss',
  encapsulation: ViewEncapsulation.None
})
export class EventsHistoryListComponent extends ListBaseComponent {
  @Input()
  public set tableDataOptions(value: TableDataOptionsModel) {
    this.defaultTableOptions = {
      ...this.defaultTableOptions,
      option: value
    };
    if (!this.saveFilter && value) {
      this.filterValues = {};
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  @Input()
  public set pageSize(value: number) {
    this.defaultTableOptions = {
      ...this.defaultTableOptions,
      pageSize: value
    };
  }

  @Input() set tableSettings(value: TableSettingsModel) {
    this.settings = {
      ...value,
      noRecordsText: value.noRecordsText || this.settings.noRecordsText,
      entityName: value.entityName || this.settings.entityName,
      hasActions: false,
      hasRowSelection: true,
      expandableRows: true
    };
  }

  @Input() checkedRowId;
  @Input() saveFilter = null;
  @Input() userPermissions = {};
  @Input() userCountry;
  @Input() userId;
  @Input() roles;
  @Input() listFilter: TableColumnFilter[];
  @Input() sortBy: any;

  tableOptions$ = this.eventService.tableOptions$.pipe(
    map((res) => {
      this._tableOptions = res?.value
        ? {
            ...this.defaultTableOptions,
            ...res.value,
            option: this.defaultTableOptions.option
          }
        : this.defaultTableOptions;
      return this._tableOptions;
    })
  );

  data$ = this.eventService.list$.pipe(
    map((res) => {
      const resp = {
        listInfo: res?.value?.list
          ? {
              ...res.value,
              list: res.value.list.map((row) => ({
                ...row,
                opened: false,
                diffPayloadNew: JSON.parse(row.diffPayloadNew),
                diffPayloadOld: JSON.parse(row.diffPayloadOld)
              }))
            }
          : this.defaultTableInfo,
        count: res?.value?.count
      };
      return resp;
    })
  );

  columns: TableColumnFilterModel[];
  filterColumns: TableColumnFilterModel[];
  filterValues: any = {};
  defaultTableInfo = { list: [], count: 0 };
  defaultTableOptions: TableOptionsModel = {
    pageNumber: TableOptionsEnum.PAGE_NUMBER,
    pageSize: TableOptionsEnum.PAGE_SIZE_25,
    filter: [],
    sort: [{ fieldName: EventListColumnsEnum.DATE_AND_TIME, order: 'desc' }]
  };
  settings: TableSettingsModel = {
    entityName: 'event',
    noRecordsText: 'eventsList',
    hasActions: false,
    hasRowSelection: true,
    expandableRows: true
  };

  //used to persist the latest table options value
  _tableOptions: TableOptionsModel;

  constructor(
    private commonDataService: CommonDataService,
    private eventService: EventService,
    private router: Router
  ) {
    super();
  }

  initComponent() {
    this.commonDataService.getSchemesList();
    const col = this.eventService.getEventsListColumns();
    this.columns = col.columns;
    this.filterColumns = col.filterByColumns;

    if (this.saveFilter) {
      this.addSubscription(
        this.commonDataService.filter$.pipe(filter((filterVal) => !filterVal.loading)).subscribe((filterValue) => {
          if (filterValue.value?.type === this.saveFilter) {
            this.filterValues = filterValue.value.filterValue;
            this.setTableOptions({
              ...this._tableOptions,
              pageNumber: TableOptionsEnum.PAGE_NUMBER,
              pageSize: this._tableOptions?.pageSize || this.defaultTableOptions.pageSize,
              filter: filterValue.value.apiParam,
              sort: filterValue.value.sort ? filterValue.value.sort : []
            });
            this.sortBy = filterValue.value.sort ? filterValue.value.sort[0] : {};
          } else {
            this.setNewCurrentFilter();
          }
        })
      );
    } else {
      this.filterValues = {};
      if (this.sortBy) {
        this.defaultTableOptions.sort = [this.sortBy];
      }
      this.setTableOptions({ ...this.defaultTableOptions });
    }
  }

  private setNewCurrentFilter() {
    this.commonDataService.saveCurrentFilter({
      type: this.saveFilter,
      filterValue: {},
      apiParam: [],
      sort: this.defaultTableOptions.sort
    });
  }

  handleRowAction(event) {
    console.log(event);
  }

  handleFilterChanged(tableOptions: TableOptionsModel, event: any) {
    if (this.saveFilter) {
      if (event.resetAllFilters === false) {
        this.commonDataService.saveCurrentFilter({
          type: this.saveFilter,
          filterValue: { ...event.filterSelection },
          apiParam: event.filter,
          sort: this.sortBy ? [this.sortBy] : this.defaultTableOptions.sort[0]
        });
      } else {
        this.filterValues = { ...event.filterSelection };
      }
    } else {
      this.filterValues = { ...event.filterSelection };
      this.setTableOptions({ ...tableOptions, filter: event.filter, pageNumber: 1 });
    }
  }

  setTableOptions(tableOptions) {
    const newFilter = tableOptions.filter ? [...tableOptions.filter] : [];
    if (this.listFilter) {
      this.listFilter.forEach((item) => {
        const idx = newFilter.findIndex((field) => field.fieldName === item.fieldName);

        if (idx === -1) {
          newFilter.push({ fieldName: item.fieldName, fieldValues: [...item.fieldValues] });
        }
      });
    }

    const updatedTableOptions = { ...tableOptions, filter: newFilter };

    this.eventService.setTableOption(updatedTableOptions);
  }

  private getDate(row, attributte) {
    return row[attributte]
      ? row[attributte].lastIndexOf('.') == -1
        ? row[attributte] + '.000Z'
        : row[attributte].substring(0, row[attributte].lastIndexOf('.')) + '.000Z'
      : row[attributte];
  }
}
