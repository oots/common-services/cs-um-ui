import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { EventsHistoryComponent } from './events-history.component';
import { StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { CommonDataService } from '../../../services/main/common-data/common-data.service';
import { ValidationApi } from '../../../services/main/validation/validation.api';
import { UserService } from '../../../services/user/user.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BreadcrumbService } from '../../../services/main/breadcrumb.service';

describe('EventsHistoryComponent', () => {
  let component: EventsHistoryComponent;
  let fixture: ComponentFixture<EventsHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventsHistoryComponent],
      imports: [StoreModule.forRoot({}), RouterTestingModule.withRoutes([]), TranslateModule.forRoot(), HttpClientModule],
      providers: [{ provide: CommonDataService }, { provide: BreadcrumbService }, { provide: ValidationApi }, { provide: UserService }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(EventsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
