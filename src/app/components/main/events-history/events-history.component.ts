import { Component, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { BreadcrumbModel } from '../../../core/models/app/breadcrumb.model';
import { BreadcrumbLabelsEnum, ListFilterEnum, UserPermissionEnum } from '../../../core/models/app.enums';
import { filter } from 'rxjs/operators';
import { CommonDataService } from '../../../services/main/common-data/common-data.service';
import { UserService } from '../../../services/user/user.service';
import { BreadcrumbService } from '../../../services/main/breadcrumb.service';
import { Router } from '@angular/router';
import { TableDataOptionsModel, TableSettingsModel } from '../../../core/models/app/table.options.model';

@Component({
  selector: 'app-events-history',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './events-history.component.html',
  styleUrl: './events-history.component.scss'
})
export class EventsHistoryComponent extends BaseComponent {
  breadcrumbItems: BreadcrumbModel[] = [];
  userCountry;
  userId;
  roles;
  userPermissions: any = {};
  listFilterEnum = ListFilterEnum;
  tableDataOptions: TableDataOptionsModel;
  tableSettings: TableSettingsModel = {
    hasFilter: true,
    hasActions: false,
    tableClass: 'light',
    hasRowSelection: true,
    expandableRows: true
  };

  constructor(
    private commonDataService: CommonDataService,
    private userService: UserService,
    private breadcrumbService: BreadcrumbService,
    private router: Router
  ) {
    super();
  }

  initComponent() {
    this.breadcrumbItems = this.breadcrumbService.getBreadcrumbItems(BreadcrumbLabelsEnum.EVENTS_LIST);

    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !!user?.value && !user.loading)).subscribe((res) => {
        this.userCountry = res.value.country;
        this.roles = res.value.roles;
        this.userId = res.value.userId;
        this.userPermissions = this.commonDataService.getUserPermissions(res.value, [
          UserPermissionEnum.CRUD_USER,
          UserPermissionEnum.CRUD_MS_USER
        ]);
      })
    );
  }
}
