import { Component } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent {
  constructor(
    private userService: UserService,
    private router: Router
  ) {
    super();
  }

  initComponent() {
    setTimeout(() => {
      this.userService.resetUserProfile();
    }, 0);
    document.cookie = 'SESSION=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    if (environment.adminAppHomeUrl) {
      document.location = environment.adminAppHomeUrl as string;
    }
  }

  handleLoginClicked() {
    this.router.navigate(['/main']);
  }
}
