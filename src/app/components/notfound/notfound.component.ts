import { Component } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserService } from 'src/app/services/user/user.service';
import { UserPermissionEnum } from 'src/app/core/models/app.enums';
import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';

@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.scss']
})
export class NotfoundComponent extends BaseComponent {
  type = 'pageNotFound';
  data;
  hideRedirectionLink = false;
  feature = this.router.getCurrentNavigation()?.extras?.state?.['feature'] || undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private commonDataService: CommonDataService,
    private userService: UserService
  ) {
    super();
  }

  initComponent() {
    this.addSubscription(
      this.userService.profile$.pipe(filter((user) => !user.loading)).subscribe((userProfile) => {
        const userAppPermissions = this.commonDataService.getUserPermissions(userProfile.value, [UserPermissionEnum.USER_APP_VIEW]);
        const csAppPermissions = this.commonDataService.getUserPermissions(userProfile.value, [UserPermissionEnum.CS_APP_VIEW]);
        const userNotRegistered = userProfile?.error?.status === 404 || (!userAppPermissions && !csAppPermissions);
        this.type = this.route.snapshot.data['type'];
        switch (this.type) {
          case 'pageNotFound':
            this.data = {
              titleKey: 'app.error.notfound.title',
              headerKey: 'app.error.notfound.header',
              headerText: 'app.error.notfound.text',
              links: [{ url: '/user-mgmt/main', labelKey: 'app.error.goToHomePage' }]
            };
            break;
          case 'pageNotAllowed':
            this.data = {
              titleKey: 'app.error.notallowed.title',
              headerKey: 'app.error.notallowed.header',
              links: []
            };
            break;
          case 'userNotRegistered':
            this.data = {
              titleKey: 'app.error.notregistered.title',
              headerKey: userNotRegistered ? 'app.error.notregistered.header2' : 'app.error.notregistered.header',
              headerText: 'app.error.notregistered.headerText',
              links: []
            };
            break;
          case 'systemOutage':
            this.data = {
              titleKey: 'app.error.systemoutage.title',
              headerKey: 'app.error.systemoutage.header',
              header2Key: 'app.error.systemoutage.header2'
            };
            break;
        }
      })
    );
  }

  goToHome() {
    window.location.href = `${window.location.origin}/home`;
  }
}
