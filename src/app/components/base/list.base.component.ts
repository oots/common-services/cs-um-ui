import { Directive } from '@angular/core';
import { BaseComponent } from './base.component';
import { TableColumnFilterTypeEnum, TableOptionsEnum } from '../../core/models/app.enums';
import { TableColumnFilter, TableColumnSort, TableOptionsModel } from 'src/app/core/models/app/table.options.model';
import { TableColumnFilterModel } from 'src/app/core/models/app/table.column.model';
import { PaginationEvent } from '@eui/components/eui-table';

@Directive()
export abstract class ListBaseComponent extends BaseComponent {
  actionInProgres = false;
  openFilter = false;
  sortBy: object = {};
  multipleCountrySelection = true;
  displayOnlyCountrySelection = true;
  filter: object = {};
  filterValues: object = {};
  protected columns: TableColumnFilterModel[] = [];
  protected filterColumns: TableColumnFilterModel[] = [];
  protected defaultTableOptions: TableOptionsModel = {
    pageNumber: TableOptionsEnum.PAGE_NUMBER,
    pageSize: TableOptionsEnum.PAGE_SIZE,
    filter: [],
    sort: []
  };
  protected defaultTableInfo = {
    list: [],
    count: 0
  };
  protected defaultPageSizeOptions = [5, 10, 25, 50, 100];

  protected constructor() {
    super();
  }

  onPageChange(e: PaginationEvent, tableOptions: TableOptionsModel) {
    if (e.pageSize === tableOptions.pageSize && e.page + 1 === tableOptions.pageNumber) {
      return;
    }

    this.setTableOptions({
      ...tableOptions,
      pageSize: e.pageSize,
      pageNumber: e.page + 1
    });
  }

  onSortChange(event: any, col: string, tableOptions: TableOptionsModel) {
    event.preventDefault();

    this.setTableOptions({
      ...tableOptions,
      pageNumber: 1,
      sort: this.calculateNextSorting(col, tableOptions.sort)
    });
  }

  protected changeJurisdictionFilterValue(
    col: TableColumnFilterModel,
    tableOptions: TableOptionsModel,
    event: any,
    type: string = 'Value'
  ) {
    const newEvent = event?.countries?.length > 0 || !!event?.country ? event : null;
    this.changeFilterValue(col, tableOptions, newEvent, type);
  }

  protected changeFilterValue(col: TableColumnFilterModel, tableOptions: TableOptionsModel, event: any, type: string = 'Value') {
    if (!event) {
      delete this.filter[col.filterId.concat(type)];
    } else {
      this.filter = {
        ...this.filter,
        [col.filterId.concat(type)]: event
      };
    }
    this.handleTableFilterValueChanged(tableOptions, this.filter);
  }

  protected handleTableFilterValueChanged(tableOptions: TableOptionsModel, filter: any) {
    const newFilter: TableColumnFilter[] = [];

    this.columns
      .filter((column) => column.filterId)
      .forEach((col) => {
        const value = filter[col.filterId.concat('Value')];
        if (!!value) {
          if (col.filterType === TableColumnFilterTypeEnum.COUNTRIES) {
            newFilter.push({
              fieldName: col.filterId,
              fieldValues: value.countries.map((country) => country.code)
            });
          } else {
            newFilter.push({
              fieldName: col.filterId,
              fieldValues: [typeof value === 'string' ? value : value?.value]
            });
          }
        }
      });

    this.setTableOptions({
      ...tableOptions,
      pageNumber: 1,
      filter: newFilter
    });
  }

  protected abstract setTableOptions(tableOptions: TableOptionsModel);

  protected trackByFn(index) {
    return index;
  }

  private formatIntervalDate(date: Date): string {
    const month = date.getMonth() + 1;
    return date
      .getFullYear()
      .toString()
      .concat(month > 9 ? '-' : '-0')
      .concat(month.toString())
      .concat(date.getDate() > 9 ? '-' : '-0')
      .concat(date.getDate().toString());
  }

  protected calculateNextSorting(col: string, sort: TableColumnSort[] = []): TableColumnSort[] {
    const currentSort: TableColumnSort = sort.length > 0 ? sort[0] : null;

    if (!currentSort || currentSort.fieldName !== col) {
      return [{ fieldName: col, order: 'asc' }];
    }

    return currentSort.order === 'asc' ? [{ fieldName: col, order: 'desc' }] : [];
  }

  /* eslint-disable @typescript-eslint/no-unused-vars */
  handleRadioBtnSelect(param) {}

  /* eslint-disable @typescript-eslint/no-unused-vars */
  handleSortDataList(param) {}
}
