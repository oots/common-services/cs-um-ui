import { Directive, OnDestroy, OnInit } from '@angular/core';

import { Subject, Subscription } from 'rxjs';

@Directive()
export abstract class BaseComponent implements OnInit, OnDestroy {
  destroyed = new Subject<void>();
  subscriptions: Subscription[] = [];
  languages = [
    { value: 'bg', text: 'български (bg)' },
    { value: 'cs', text: 'čeština (cs)' },
    { value: 'da', text: 'dansk (da)' },
    { value: 'de', text: 'Deutsch (de)' },
    { value: 'et', text: 'eesti (et)' },
    { value: 'el', text: 'ελληνικά (el)' },
    { value: 'en', text: 'English (en)' },
    { value: 'es', text: 'español (es)' },
    { value: 'fr', text: 'français (fr)' },
    { value: 'ga', text: 'Gaeilge (ga)' },
    { value: 'hr', text: 'hrvatski (hr)' },
    { value: 'it', text: 'italiano (it)' },
    { value: 'lv', text: 'latviešu (lv)' },
    { value: 'lt', text: 'lietuvių (lt)' },
    { value: 'hu', text: 'magyar (hu)' },
    { value: 'mt', text: 'Malti (mt)' },
    { value: 'nl', text: 'Nederlands (nl)' },
    { value: 'pl', text: 'polski (pl)' },
    { value: 'pt', text: 'português (pt)' },
    { value: 'ro', text: 'română (ro)' },
    { value: 'sk', text: 'slovenčina (sk)' },
    { value: 'sl', text: 'slovenščina (sl)' },
    { value: 'fi', text: 'suomi (fi)' },
    { value: 'sv', text: 'svenska (sv)' }
  ];

  ngOnInit(): void {
    this.initComponent();
  }

  initComponent(): void {
    // eslint-disable-next-line no-empty-function
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => {
      if (subscription && !subscription.closed) {
        subscription.unsubscribe();
      }
    });
    this.destroyed.next();
    this.destroyed.complete();
  }

  protected addSubscription(subscription: Subscription): void {
    if (this.subscriptions) {
      this.subscriptions.push(subscription);
    }
  }

  protected trackByFn(index) {
    return index;
  }

  handleAddBtnClicked() {
    // defined for the table classes that don't define add button click
  }

  handleLinkBtnClicked() {
    // defined for the table classes that don't define add button click
  }
}
