import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent]
    });
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    // @ts-expect-error This line is expected to cause a TypeScript error
    component._window = {
      location: {
        href: '',
        ancestorOrigins: undefined,
        hash: '',
        host: '',
        hostname: '',
        origin: '',
        pathname: '',
        port: '',
        protocol: '',
        search: '',
        assign: function (): void {
          throw new Error('Function not implemented.');
        },
        reload: function (): void {
          throw new Error('Function not implemented.');
        },
        replace: function (): void {
          throw new Error('Function not implemented.');
        }
      }
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
