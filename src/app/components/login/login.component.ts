import { Component } from '@angular/core';
import { BaseComponent } from 'src/app/components/base/base.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent {
  _window = window;
  initComponent() {
    document.cookie = 'SESSION=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    this._window.location.href = environment.loginUrl as string;
  }
}
