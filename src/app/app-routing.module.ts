import { RouterModule, Routes } from '@angular/router';
import { authGuard } from './core/guards/authenticate.guard';
import { hasUnsavedChangesGuard } from './core/guards/form-dirty.guard';
import { MainComponent } from './components/main/main.component';
import { NgModule } from '@angular/core';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { DashboardComponent } from './components/main/dashboard/dashboard.component';
import { UserComponent } from 'src/app/components/main/user/user.component';
import { FlowComponent } from 'src/app/components/flow/flow.component';
import { UserFlowComponent } from 'src/app/components/main/user/user-flow/user-flow.component';
import { EDeliveryComponent } from 'src/app/components/main/e-delivery/e-delivery.component';
import { EDeliveryFlowComponent } from 'src/app/components/main/e-delivery/e-delivery-flow/e-delivery-flow.component';
import { LoginComponent } from 'src/app/components/login/login.component';
import { EventsHistoryComponent } from './components/main/events-history/events-history.component';
import { UserPermissionEnum } from './core/models/app.enums';
import { authorizationGuard } from './core/guards/authorization.guard';
import { SummaryPageComponent } from './components/main/common/summary-page/summary-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'no-permission', component: NotfoundComponent, data: { type: 'pageNotAllowed' } },
  { path: 'not-found', canMatch: [authGuard], component: NotfoundComponent, data: { type: 'pageNotFound' } },
  { path: 'not-registered', component: NotfoundComponent, data: { type: 'userNotRegistered' } },
  {
    path: 'main',
    canMatch: [authGuard],
    component: MainComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'confirmation',
        component: SummaryPageComponent
      },
      {
        path: 'user',
        children: [
          {
            path: '',
            component: UserComponent
          }
        ]
      },
      {
        path: 'e-delivery',
        children: [
          {
            path: '',
            canMatch: [authorizationGuard],
            component: EDeliveryComponent
          }
        ]
      },
      {
        path: 'events',
        children: [
          {
            path: '',
            component: EventsHistoryComponent,
            canActivate: [authGuard],
            data: { permissions: [UserPermissionEnum.CRUD_USER, UserPermissionEnum.CRUD_MS_USER] }
          }
        ]
      }
    ]
  },
  {
    path: 'flow',
    canMatch: [authGuard],
    component: FlowComponent,
    children: [
      { path: '', redirectTo: '/not-found', pathMatch: 'full' },
      {
        path: 'user',
        component: UserFlowComponent,
        canMatch: [authorizationGuard],
        canDeactivate: [hasUnsavedChangesGuard]
      },
      {
        path: 'e-delivery',
        component: EDeliveryFlowComponent,
        canMatch: [authorizationGuard],
        canDeactivate: [hasUnsavedChangesGuard]
      }
    ]
  },
  { path: '**', redirectTo: 'not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {}
