import * as eventActions from '../../core/reducers/event/event.actions';
import * as eventSelectors from '../../core/reducers/event/event.selectors';

import { EventListColumnsEnum, ListsEnum, TableColumnFilterTypeEnum } from 'src/app/core/models/app.enums';
import { select, Store } from '@ngrx/store';

import { CsState } from '../../core/reducers/cs.state';
import { Injectable } from '@angular/core';
import { TableColumnFilterModel } from 'src/app/core/models/app/table.column.model';
import { TableOptionsModel } from 'src/app/core/models/app/table.options.model';

@Injectable()
export class EventService {
  list$ = this.store.pipe(select(eventSelectors.selectList));
  tableOptions$ = this.store.pipe(select(eventSelectors.selectTableOptions));
  actionConfirmation$ = this.store.pipe(select(eventSelectors.selectActionConfirmation));

  constructor(private store: Store<CsState>) {}

  getList() {
    this.store.dispatch(eventActions.getEventListAction());
  }

  setTableOption(options?: TableOptionsModel) {
    this.store.dispatch(options ? eventActions.setListTableOptionsAction(options) : eventActions.resetListTableOptionsAction());
  }

  resetConfirmationAction() {
    this.store.dispatch(eventActions.resetConfirmationAction());
  }

  //used in event components
  getEventsListColumns(): any {
    const columns: TableColumnFilterModel[] = [
      {
        id: EventListColumnsEnum.SUBJECT,
        label: ListsEnum.CS_EVENT.concat('.').concat(EventListColumnsEnum.SUBJECT),
        filterId: EventListColumnsEnum.SUBJECT,
        filterType: TableColumnFilterTypeEnum.INPUT,
        display: '',
        placeholder: 'app.list.event.filter.searchSubjectPlaceholder',
        width: 'cs-col-w-30',
        sortable: true
      },
      {
        id: EventListColumnsEnum.EVENT_TYPE_NAME,
        label: ListsEnum.CS_EVENT.concat('.').concat(EventListColumnsEnum.EVENT_TYPE),
        filterId: EventListColumnsEnum.EVENT_TYPE,
        filterType: TableColumnFilterTypeEnum.EVENT_TYPE,
        display: 'eventType',
        placeholder: 'app.list.event.filter.eventTypeFilterPlaceholder',
        width: 'cs-col-w-20',
        sortable: true
      },
      {
        id: EventListColumnsEnum.OBJECT,
        label: ListsEnum.CS_EVENT.concat('.').concat(EventListColumnsEnum.OBJECT),
        filterId: EventListColumnsEnum.OBJECT,
        filterType: TableColumnFilterTypeEnum.INPUT,
        display: '',
        placeholder: 'app.list.event.filter.searchObjectPlaceholder',
        width: 'cs-col-w-30',
        sortable: true
      },
      {
        id: EventListColumnsEnum.DATE_AND_TIME,
        label: ListsEnum.CS_EVENT.concat('.').concat(EventListColumnsEnum.DATE_AND_TIME),
        filterId: EventListColumnsEnum.DATE_AND_TIME,
        filterType: TableColumnFilterTypeEnum.TIMESTAMP,
        display: 'timestamp',
        width: 'cs-col-w-15',
        sortable: true
      }
    ];

    const filterLabel = {
      id: 'app.list.common.filters',
      filterId: TableColumnFilterTypeEnum.LABEL,
      filterType: TableColumnFilterTypeEnum.LABEL
    };
    return {
      columns: [columns[0], columns[1], columns[2], columns[3]],
      filterByColumns: [columns[0], columns[2], filterLabel, columns[1], columns[3]]
    };
  }
}
