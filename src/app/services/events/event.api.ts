import { AbstractService } from '../abstract.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class EventApi extends AbstractService {
  constructor(protected http: HttpClient) {
    super(http);
  }

  getEventList(params): Observable<object> {
    return this.returnApiPostCall('event/list', params);
  }
}
