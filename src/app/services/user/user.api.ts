import { AbstractService } from '../abstract.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class UserApi extends AbstractService {
  constructor(protected http: HttpClient) {
    super(http);
  }

  getUserProfile(): Observable<object> {
    return this.returnApiGetCall(`user/info`, 'login');
  }

  getUserList(params): Observable<object> {
    return this.returnApiPostCall('user/list', params);
  }

  getUserDetails(id: number): Observable<object> {
    return this.returnApiGetCall(`user/details/${id}`, 'details');
  }

  saveUser(params): Observable<object> {
    const action = params?.id ? `update` : 'save';
    return this.returnApiPostCall(`user/${action}`, params);
  }

  deleteUser(id): Observable<object> {
    return this.returnApiDeleteCall('user', id);
  }

  updateUserStatus(params: { id: number; status: number }): Observable<object> {
    return this.returnApiPostCall(`user/${params.id}/update-status/${params.status}`, {});
  }

  validateUsername(username: string): Observable<object> {
    return this.returnApiGetCall(`user/exists/username/${username}`);
  }

  validateEmailAccount(email: string): Observable<object> {
    return this.returnApiGetCall(`user/exists/email/${email}`);
  }

  changeUserStatus(params): Observable<object> {
    return this.returnApiPostCall(`user/${params.id}/update-status/${params.status}`, {});
  }

  validateUser(params): Observable<object> {
    return this.returnApiPostCall('user/validation', params);
  }
}
