import * as userActions from '../../core/reducers/user/user.actions';
import * as userSelectors from '../../core/reducers/user/user.selectors';

import {
  ListActionsEnum,
  ListsEnum,
  TableColumnFilterTypeEnum,
  UserListColumnsEnum,
  UserPermissionEnum,
  UserRoleEnum,
  UserStatusEnum
} from 'src/app/core/models/app.enums';
import { select, Store } from '@ngrx/store';
import { CsState } from '../../core/reducers/cs.state';
import { Injectable } from '@angular/core';
import { ListActionButtonModel } from 'src/app/core/models/response/list.action.button.model';
import { TableColumnFilterModel } from 'src/app/core/models/app/table.column.model';
import { TableOptionsModel } from 'src/app/core/models/app/table.options.model';
import { UserModel, UserRolesModel, UserStatusModel } from 'src/app/core/models/response/user.model';
import { tap } from 'rxjs';

@Injectable()
export class UserService {
  profile$ = this.store.pipe(
    select(userSelectors.selectUserProfile),
    tap(
      (v) =>
        (this.isUserRegistered = !!v.value?.permissionList?.filter(
          (permission) => permission.permissionName === UserPermissionEnum.USER_APP_VIEW
        ))
    )
  );
  list$ = this.store.pipe(select(userSelectors.selectList));
  tableOptions$ = this.store.pipe(select(userSelectors.selectTableOptions));
  actionConfirmation$ = this.store.pipe(select(userSelectors.selectActionConfirmation));
  private isUserRegistered = false;

  constructor(private store: Store<CsState>) {}

  getUserProfile() {
    this.store.dispatch(userActions.getUserProfileAction());
  }

  resetUserProfile() {
    this.store.dispatch(userActions.resetStateAction());
  }

  getList() {
    this.store.dispatch(userActions.getUserListAction());
  }

  setTableOption(options?: TableOptionsModel) {
    this.store.dispatch(options ? userActions.setListTableOptionsAction(options) : userActions.resetListTableOptionsAction());
  }

  getStatusList() {
    const statusList = [];
    statusList.push(UserStatusEnum.USER_STATUS_ACTIVE);
    statusList.push(UserStatusEnum.USER_STATUS_INACTIVE);
    return statusList;
  }

  getActionsForRow(userPermission: object, row = null, countryCode = null): ListActionButtonModel[] {
    const rowActions = [];

    let disabled = !userPermission[UserPermissionEnum.CRUD_USER];
    if (disabled) {
      disabled = !(userPermission[UserPermissionEnum.CRUD_MS_USER] && row.country === countryCode);
    }
    const allActions: ListActionButtonModel[] = [
      {
        id: ListActionsEnum.EDIT,
        class: 'eui-button--basic eui-button--icon-only eui-button--primary eui-button--rounded',
        tooltip: 'app.list.actions.edit',
        icon: 'pencil',
        set: 'outline',
        color: 'primary',
        disabled
      }
    ];

    if (userPermission[UserPermissionEnum.CRUD_USER] || (userPermission[UserPermissionEnum.CRUD_MS_USER] && row.country === countryCode)) {
      rowActions.push(allActions[0]);
    }

    return rowActions;
  }

  saveUser(user: UserModel) {
    this.store.dispatch(userActions.saveUserAction(user));
  }

  resetConfirmationAction() {
    this.store.dispatch(userActions.resetConfirmationAction());
  }

  getUsersListColumns(
    countryCode = 'EC',
    roles: UserRolesModel[]
  ): { columns: TableColumnFilterModel[]; filterByColumns: TableColumnFilterModel[] } {
    const columns: TableColumnFilterModel[] = [
      {
        id: UserListColumnsEnum.USER_NAME,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.USER_NAME),
        filterId: UserListColumnsEnum.USER_NAME,
        filterType: TableColumnFilterTypeEnum.INPUT,
        display: '',
        placeholder: 'app.list.user.filter.name',
        width: 'cs-col-w-15',
        sortable: true
      },
      {
        id: UserListColumnsEnum.USER_EMAIL,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.USER_EMAIL),
        filterId: UserListColumnsEnum.USER_EMAIL,
        filterType: TableColumnFilterTypeEnum.INPUT,
        display: '',
        placeholder: 'app.list.user.filter.email',
        width: 'cs-col-w-15',
        sortable: true
      },
      {
        id: UserListColumnsEnum.USER_COUNTRY_NAME,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.USER_COUNTRY_NAME),
        filterId: UserListColumnsEnum.USER_COUNTRY_CODE,
        filterType: TableColumnFilterTypeEnum.COUNTRIES,
        display: 'country',
        placeholder: 'app.list.user.filter.country',
        width: 'cs-col-w-10',
        sortable: true
      },
      {
        id: UserListColumnsEnum.USER_UM_APP,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.USER_UM_APP),
        display: 'appUM',
        width: 'cs-col-w-15',
        sortable: false
      },
      {
        id: UserListColumnsEnum.USER_ADMIN_APP,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.USER_ADMIN_APP),
        display: 'appAdmin',
        width: 'cs-col-w-15',
        sortable: false
      },
      {
        id: UserListColumnsEnum.START_DATE,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.START_DATE),
        display: 'startDate',
        width: 'cs-col-w-10',
        sortable: true
      },
      {
        id: UserListColumnsEnum.END_DATE,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.END_DATE),
        display: 'endDate',
        width: 'cs-col-w-10',
        sortable: true
      },
      {
        id: UserListColumnsEnum.STATUS,
        label: ListsEnum.CS_USER.concat('.').concat(UserListColumnsEnum.STATUS),
        filterId: UserListColumnsEnum.STATUS,
        filterType: TableColumnFilterTypeEnum.STATUS,
        display: 'userStatus',
        placeholder: 'app.list.user.filter.status',
        width: 'cs-col-w-5',
        sortable: true,
        translation: 'app.data.user.status_',
        items: this.getStatusList()
      }
    ];

    const filterLabel = {
      id: 'app.list.common.filters',
      filterId: TableColumnFilterTypeEnum.LABEL,
      filterType: TableColumnFilterTypeEnum.LABEL
    };
    if (countryCode !== 'EC') {
      const hideUserRoles = roles && roles.length == 1 && roles[0].roleName === UserRoleEnum.MS_USER_MANAGER_DELEGATE.valueOf();
      if (hideUserRoles) {
        return {
          columns: [columns[0], columns[1], columns[4], columns[5], columns[6], columns[7]],
          filterByColumns: [columns[0], columns[1], filterLabel, columns[7]]
        };
      } else {
        return {
          columns: [columns[0], columns[1], columns[3], columns[4], columns[5], columns[6], columns[7]],
          filterByColumns: [columns[0], columns[1], filterLabel, columns[7]]
        };
      }
    }
    return {
      columns: [columns[0], columns[1], columns[2], columns[3], columns[4], columns[5], columns[6], columns[7]],
      filterByColumns: [columns[0], columns[1], filterLabel, columns[2], columns[7]]
    };
  }

  changeUserStatus(userStatus: UserStatusModel) {
    this.store.dispatch(userActions.changeUserStatusAction(userStatus));
  }

  expiredDate(date) {
    if (date) {
      return new Date(date).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0);
    } else {
      return false;
    }
  }

  getIsUserRegistered() {
    return this.isUserRegistered;
  }
}
