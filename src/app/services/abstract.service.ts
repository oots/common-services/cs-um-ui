import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { HTTPStatusEnum } from 'src/app/core/models/app.enums';
import { environment } from 'src/environments/environment';

export abstract class AbstractService {
  protected readonly httpHeaders: HttpHeaders;
  protected endpoint: any;

  protected constructor(protected http: HttpClient) {
    this.endpoint = environment.endpoint;
    this.httpHeaders = new HttpHeaders({
      'Cache-Control': 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
      Pragma: 'no-cache',
      Expires: '0'
    });
    this.httpHeaders = this.httpHeaders.append('Content-Type', 'application/json');
  }

  protected getUrl(): string {
    return this.endpoint.url;
  }

  protected getHeaders() {
    return this.httpHeaders;
  }

  protected setHttpParams(options: any): HttpParams {
    let httpParams = new HttpParams();

    if (options !== undefined && options.length > 0) {
      options.forEach(function (item) {
        httpParams = httpParams.append('' + item.name.toString(), item.value.toString());
      });
    }
    return httpParams;
  }

  protected request(url: string | Request, options?: any): Observable<any> {
    return this.http.request('', typeof url === 'string' ? url : url.url, options);
  }

  protected get(url: string, options?: any): Observable<any> {
    return this.http.get(url, {
      ...options,
      observe: 'response'
    });
  }

  protected post(url: string, body: any, options?: any): Observable<any> {
    options = {
      ...options,
      observe: 'response'
    };
    return this.http.post<HttpResponse<object>>(url, body, options);
  }

  protected put(url: string, body: any, options?: any): Observable<any> {
    return this.http.put(url, body, options);
  }

  protected delete(url: string, options?: any): Observable<object> {
    options = {
      ...options,
      observe: 'response'
    };
    return this.http.delete(url, options);
  }

  protected returnDefaultErrorStatus(err: any) {
    return of({
      ...err,
      status: err.status ? err.status : -1
    });
  }

  protected returnApiPostCall(path, params): Observable<object> {
    return this.post(`${this.getUrl()}/${path}`, params, {
      headers: this.getHeaders(),
      withCredentials: true
    }).pipe(
      map((response: HttpResponse<object>) => {
        return this.parseResponse(response);
      }),
      catchError((err) => {
        return this.returnDefaultErrorStatus(err);
      })
    );
  }

  protected returnApiGetCall(path, type = 'list'): Observable<object> {
    return this.get(`${this.getUrl()}/${path}`, {
      headers: this.getHeaders(),
      withCredentials: true
    }).pipe(
      map((response: HttpResponse<object>) => {
        return this.parseResponse(response, type);
      }),
      catchError((err) => {
        if (type === 'login') {
          throw this.parseResponse(
            new HttpResponse({
              status: err?.status || HTTPStatusEnum.REDIRECT_302
            }),
            type
          );
        } else {
          return this.returnDefaultErrorStatus(err);
        }
      })
    );
  }

  protected returnApiDeleteCall(path, id): Observable<object> {
    return this.delete(`${this.getUrl()}/${path}/${id}`, {
      headers: this.getHeaders(),
      withCredentials: true
    }).pipe(
      map((response: HttpResponse<object>) => {
        return this.parseResponse(response, 'delete');
      }),
      catchError((err) => {
        return this.returnDefaultErrorStatus(err);
      })
    );
  }

  private parseResponse(response: HttpResponse<object>, callType = 'call'): object {
    let result = { status: -1, data: null };
    if (response) {
      switch (response.status) {
        case HTTPStatusEnum.OK_200:
          result = { status: HTTPStatusEnum.OK_200, data: callType === 'delete' ? true : response.body };
          break;
        case HTTPStatusEnum.CREATED_201:
          result = { status: HTTPStatusEnum.OK_200, data: response.body };
          break;
        case HTTPStatusEnum.NO_CONTENT_204:
          result = { status: HTTPStatusEnum.OK_200, data: [] };
          break;
        case HTTPStatusEnum.FORBIDDEN_403:
          result = { status: response.status, data: null };
          break;
        case HTTPStatusEnum.UNAUTHORIZED_401:
          result = { status: HTTPStatusEnum.UNAUTHORIZED_401, data: response.url || null };
          break;
        case HTTPStatusEnum.REDIRECT_302:
          result = { status: HTTPStatusEnum.REDIRECT_302, data: response.headers['location'] || null };
          break;
        default:
          result = { status: response.status ? response.status : -1, data: null };
      }
    }
    return result;
  }
}
