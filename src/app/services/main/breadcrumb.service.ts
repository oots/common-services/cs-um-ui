import { Injectable } from '@angular/core';
import { BreadcrumbModel } from '../../core/models/app/breadcrumb.model';
import { BreadcrumbLabelsEnum } from '../../core/models/app.enums';

@Injectable()
export class BreadcrumbService {
  getBreadcrumbItems(breadcrumb: BreadcrumbLabelsEnum, options?: BreadcrumbModel): BreadcrumbModel[] {
    const items: BreadcrumbModel[] = [];

    items.push({ label: BreadcrumbLabelsEnum.DASHBOARD, icon: '', url: '/', iconTooltip: 'home' });

    if (breadcrumb === BreadcrumbLabelsEnum.USER_LIST) {
      items.push({
        label: BreadcrumbLabelsEnum.USER_LIST,
        url: './main/user',
        count: options?.count
      });
    } else if (breadcrumb === BreadcrumbLabelsEnum.E_DELIVERY_LIST) {
      items.push({
        label: BreadcrumbLabelsEnum.E_DELIVERY_LIST,
        url: './main/e-delivery',
        count: options?.count
      });
    } else if (breadcrumb === BreadcrumbLabelsEnum.EVENTS_LIST) {
      items.push({
        label: BreadcrumbLabelsEnum.EVENTS_LIST,
        url: './main/events',
        count: options?.count
      });
    }

    return items;
  }
}
