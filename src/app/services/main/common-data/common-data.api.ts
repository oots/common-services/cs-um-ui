import { AbstractService } from '../../abstract.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class CommonDataApi extends AbstractService {
  constructor(protected http: HttpClient) {
    super(http);
  }

  getCountriesList(): Observable<object> {
    return this.returnApiGetCall(`country/list`);
  }

  getRolesList(): Observable<object> {
    return this.returnApiGetCall(`user/role/list`);
  }

  getAssignableRolesList(): Observable<object> {
    return this.returnApiGetCall(`user/roles/list/assignable`);
  }

  getSchemesList(): Observable<object> {
    return this.returnApiGetCall(`common/id-scheme/list`);
  }
}
