import * as commonActions from '../../../core/reducers/common/common.actions';
import * as commonSelectors from '../../../core/reducers/common/common.selectors';

import { Store, select } from '@ngrx/store';

import { ActionConfirmationEnum, AppFlowEnum, UserPermissionEnum } from '../../../core/models/app.enums';
import { CsState } from '../../../core/reducers/cs.state';
import { Injectable } from '@angular/core';
import { ListFilterModel } from '../../../core/models/app/filter.model';
import { UserProfileModel } from 'src/app/core/models/response/user.profile';
import { FlowModel, FlowStepModel } from 'src/app/core/models/app/flow.model';
import { FormControl, FormBuilder } from '@angular/forms';
import { ValidationApi } from '../validation/validation.api';

@Injectable()
export class CommonDataService {
  actionConfirmation$ = this.store.pipe(select(commonSelectors.selectActionConfirmation));
  filter$ = this.store.pipe(select(commonSelectors.selectFilter));
  countries$ = this.store.pipe(select(commonSelectors.selectCountriesList));
  flow$ = this.store.pipe(select(commonSelectors.selectFlow));
  roles$ = this.store.pipe(select(commonSelectors.selectRolesList));
  assignableRoles$ = this.store.pipe(select(commonSelectors.selectAssignableRolesList));
  schemes$ = this.store.pipe(select(commonSelectors.selectSchemesList));

  isGrowlSticky = false;
  isGrowlMultiple = false;
  growlLife = 3000;
  position = 'top-center';
  summaryTitle = 'Summary title';
  messageDetail = 'Message details';

  constructor(
    private store: Store<CsState>,
    private formBuilder: FormBuilder,
    private validationApi: ValidationApi
  ) {}

  handleCallbackError() {
    let errMessage;
    if (errMessage) {
      console.log(errMessage);
    }
  }

  setConfirmationAction(action: ActionConfirmationEnum, url = null) {
    this.store.dispatch(commonActions.setConfirmationAction(action ? { name: action, value: url } : null));
  }

  handleActionConfirmation(
    confirmation: any,
    availableActions: ActionConfirmationEnum[]
  ): { value: any; errorKey: boolean | string; action: string } {
    if (!confirmation || confirmation.loading) {
      return { action: null, errorKey: null, value: null };
    }
    let errorKey;
    if (confirmation.error) {
      errorKey = confirmation.value?.value === 0 ? 'duplicate' : 'unknown';
    }
    return {
      value: confirmation.value?.value,
      errorKey,
      action: availableActions.indexOf(confirmation?.value?.name) > -1 ? confirmation.value.name : null
    };
  }

  saveCurrentFilter(filter: ListFilterModel) {
    this.store.dispatch(commonActions.setCurrentFilterAction(filter));
  }

  getCountriesList() {
    this.store.dispatch(commonActions.getCountryListAction());
  }

  getUserPermissions(user: UserProfileModel, permissions: UserPermissionEnum[], resourceOwner = null): any {
    const result = {};
    if (permissions?.length > 0 && user?.permissionList?.length > 0) {
      permissions.forEach((permission) => {
        result[permission] = user.permissionList.filter((item) => item.permissionName === permission).length > 0;
      });
    }
    if (resourceOwner) {
      result[UserPermissionEnum.FE_EDIT_MS_COUNTRY] = user.country === resourceOwner;
    }
    return result;
  }

  hasPermission(user: UserProfileModel, permissions: UserPermissionEnum[]) {
    const result = this.getUserPermissions(user, permissions);
    let hasPermission = false;
    Object.keys(result).forEach((k) => {
      hasPermission = hasPermission || result[k];
    });
    return hasPermission;
  }

  setFlow(flow: FlowModel) {
    this.store.dispatch(commonActions.setFlowDataAction(flow));
  }

  resetFlow() {
    this.store.dispatch(commonActions.setFlowDataAction(null));
  }

  initializeFlow(flow: AppFlowEnum, activeStep = 0, data = null) {
    if (!flow) {
      return;
    }
    let flowTitle = '';
    const steps: FlowStepModel[] = [];

    if (flow === AppFlowEnum.CREATE_USER) {
      flowTitle = 'app.flow.1.title';
      steps.push({
        id: 0,
        label: 'app.flow.1.step.0.header',
        titleKey: data?.user?.id ? 'app.flow.1.step.0.editTitle' : 'app.flow.1.step.0.title'
      });
    } else if (flow === AppFlowEnum.CREATE_E_DELIVERY) {
      flowTitle = data?.eDelivery?.id ? data?.eDelivery.name : 'app.flow.2.title';
      steps.push({
        id: 0,
        label: 'app.flow.2.step.0.header',
        titleKey: data?.eDelivery?.id ? 'app.flow.2.step.0.editTitle' : 'app.flow.2.step.0.title'
      });
    }

    this.setFlow({
      id: flow,
      activeStep,
      flowTitle,
      steps,
      data,
      status: 'draft'
    });
  }

  getRolesList() {
    this.store.dispatch(commonActions.getRolesListAction());
  }
  getAssignableRolesList() {
    this.store.dispatch(commonActions.getAssignableRolesListAction());
  }

  getSchemesList() {
    this.store.dispatch(commonActions.getSchemesListAction());
  }

  createFormFromJson(json, parentForm?) {
    const form = this.formBuilder.group({}, { updateOn: 'blur' });
    for (const x in json) {
      if (x != 'validators') {
        if (Array.isArray(json[x])) {
          const formArray = [];
          let isArrayOfObjects = true;
          for (const j in json[x]) {
            if (!(json[x][j] instanceof Object)) {
              isArrayOfObjects = false;
              formArray.push(json[x][j]);
            } else {
              formArray.push(this.createFormFromJson(json[x][j], form));
            }
          }
          if (json[x].length == 0) {
            if (json.validators && json.validators[x]) {
              const validatorsArray = [];
              for (const i in json.validators[x]) {
                validatorsArray.push(json.validators[x][i](form, parentForm));
              }
              form.addControl(x, this.formBuilder.array([], validatorsArray));
            } else {
              form.addControl(x, this.formBuilder.array([]));
            }
          } else {
            if (isArrayOfObjects) {
              if (json.validators && json.validators[x]) {
                const validatorsArray = [];
                for (const i in json.validators[x]) {
                  validatorsArray.push(json.validators[x][i](form, parentForm));
                }
                form.addControl(x, this.formBuilder.array(formArray, validatorsArray));
              } else {
                form.addControl(x, this.formBuilder.array(formArray));
              }
            } else {
              if (json.validators && json.validators[x]) {
                const validatorsArray = [];
                for (const i in json.validators[x]) {
                  validatorsArray.push(json.validators[x][i](form, parentForm));
                }
                form.addControl(x, new FormControl(formArray, validatorsArray));
              } else {
                form.addControl(x, new FormControl(formArray));
              }
            }
          }
        } else if (json[x] instanceof Object) {
          const validatorsArray = [];
          if (json.validators && json.validators[x]) {
            for (const i in json.validators[x]) {
              validatorsArray.push(json.validators[x][i](form, parentForm, this.validationApi));
            }
          }
          form.addControl(x, this.createFormFromJson(json[x], form));
          form.get(x).addAsyncValidators(validatorsArray);
        } else {
          if (json.validators && json.validators[x]) {
            const validatorsArray = [];
            for (const i in json.validators[x]) {
              validatorsArray.push(json.validators[x][i](form, parentForm, this.validationApi));
            }
            form.addControl(x, new FormControl(json[x], validatorsArray));
          } else {
            form.addControl(x, new FormControl(json[x]));
          }
        }
      }
    }

    return form;
  }
}
