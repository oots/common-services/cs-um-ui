import { EuiMenuItem } from '@eui/components/eui-menu';
import { Injectable } from '@angular/core';
import { MainMenuItemsEnum } from '../../core/models/app.enums';

@Injectable()
export class MenuService {
  getMenuItems(): EuiMenuItem[] {
    const response = [];
    response.push({ label: MainMenuItemsEnum.MENU_DASHBOARD, url: 'main' });
    response.push({ label: MainMenuItemsEnum.MENU_USER, url: 'main/user' });
    response.push({ label: MainMenuItemsEnum.MENU_E_DELIVERY, url: 'main/e-delivery' });
    response.push({ label: MainMenuItemsEnum.MENU_EVENTS, url: 'main/events' });

    return response.map((item) => ({ ...item, id: item.label, expanded: false, active: false }));
  }

  getActiveIndex(url: string): number {
    if (url.includes('/user')) {
      return 1;
    } else if (url.includes('/e-delivery')) {
      return 2;
    } else if (url.includes('/events')) {
      return 3;
    } else {
      return 0;
    }
  }
}
