import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AbstractService } from '../../abstract.service';

@Injectable()
export class ValidationApi extends AbstractService {
  constructor(protected http: HttpClient) {
    super(http);
  }

  validate(path, params): Observable<object> {
    return this.returnApiPostCall(`${path}`, params);
  }
}
