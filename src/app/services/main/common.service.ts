import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EuiGrowlService } from '@eui/core';

@Injectable()
export class CommonService {
  constructor(
    private translateService: TranslateService,
    private growlService: EuiGrowlService
  ) {}

  displayGrowlMessage(severity: string, heading: string, text: string, position = 'top-right'): void {
    this.growlService.growl(
      {
        severity,
        summary: this.translateService.instant(heading),
        detail: this.translateService.instant(text)
      },
      false,
      false,
      3000,
      position
    );
  }
}
