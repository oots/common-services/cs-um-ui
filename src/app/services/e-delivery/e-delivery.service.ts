import * as eDeliveryActions from '../../core/reducers/e-delivery/e-delivery.actions';
import * as eDeliverySelectors from '../../core/reducers/e-delivery/e-delivery.selectors';

import {
  EDeliveryListColumnsEnum,
  EDeliveryStatusEnum,
  ListActionsEnum,
  ListsEnum,
  TableColumnFilterTypeEnum,
  UserPermissionEnum
} from 'src/app/core/models/app.enums';
import { select, Store } from '@ngrx/store';

import { CsState } from '../../core/reducers/cs.state';
import { Injectable } from '@angular/core';
import { ListActionButtonModel } from 'src/app/core/models/response/list.action.button.model';
import { TableColumnFilterModel } from 'src/app/core/models/app/table.column.model';
import { TableOptionsModel } from 'src/app/core/models/app/table.options.model';
import { EDeliveryModel, EDeliveryStatusModel, EDeliveryValidateModel } from 'src/app/core/models/response/e-delivery.model';

@Injectable()
export class EDeliveryService {
  list$ = this.store.pipe(select(eDeliverySelectors.selectList));
  tableOptions$ = this.store.pipe(select(eDeliverySelectors.selectTableOptions));
  actionConfirmation$ = this.store.pipe(select(eDeliverySelectors.selectActionConfirmation));

  constructor(private store: Store<CsState>) {}

  getList() {
    this.store.dispatch(eDeliveryActions.getEDeliveryListAction());
  }

  setTableOption(options?: TableOptionsModel) {
    this.store.dispatch(options ? eDeliveryActions.setListTableOptionsAction(options) : eDeliveryActions.resetListTableOptionsAction());
  }

  getStatusList() {
    const statusList = [];
    statusList.push(EDeliveryStatusEnum.E_DELIVERY_STATUS_ACTIVE);
    statusList.push(EDeliveryStatusEnum.E_DELIVERY_STATUS_INACTIVE);
    return statusList;
  }

  getActionsForRow(userPermission: object, countryCode = null): ListActionButtonModel[] {
    const rowActions = [];

    let disabled = !userPermission[UserPermissionEnum.CRUD_EDELIVERY];
    if (countryCode) {
      disabled = !(userPermission[UserPermissionEnum.CRUD_EDELIVERY] && countryCode === 'EC');
    }
    const allActions: ListActionButtonModel[] = [
      {
        id: ListActionsEnum.EDIT,
        class: 'eui-button--basic eui-button--icon-only eui-button--primary eui-button--rounded eui-u-f-bold',
        tooltip: 'app.list.actions.edit',
        icon: 'pencil',
        set: 'outline',
        color: 'primary',
        disabled
      },
      {
        id: ListActionsEnum.DELETE,
        class: 'eui-button--basic eui-button--icon-only eui-button--primary eui-button--rounded eui-u-f-bold',
        tooltip: 'app.list.actions.delete',
        icon: 'trash',
        set: 'sharp',
        color: 'primary',
        disabled
      }
    ];

    if (userPermission[UserPermissionEnum.CRUD_EDELIVERY] && countryCode === 'EC') {
      rowActions.push(allActions[0], allActions[1]);
    }

    return rowActions;
  }

  saveEDelivery(node: EDeliveryModel) {
    this.store.dispatch(eDeliveryActions.saveEDeliveryAction(node));
  }

  deleteEDelivery(id: number) {
    this.store.dispatch(eDeliveryActions.deleteEDeliveryAction(id));
  }

  resetConfirmationAction() {
    this.store.dispatch(eDeliveryActions.resetConfirmationAction());
  }

  getListColumns(countryCode = null): any {
    const columns: TableColumnFilterModel[] = [
      {
        id: EDeliveryListColumnsEnum.E_DELIVERY_NAME,
        label: ListsEnum.CS_E_DELIVERY.concat('.').concat(EDeliveryListColumnsEnum.E_DELIVERY_NAME),
        filterId: EDeliveryListColumnsEnum.E_DELIVERY_NAME,
        filterType: TableColumnFilterTypeEnum.INPUT,
        display: '',
        placeholder: 'app.list.eDelivery.filter.name',
        width: 'cs-col-w-30',
        sortable: true
      },
      {
        id: EDeliveryListColumnsEnum.E_DELIVERY_IDENTIFIER,
        label: ListsEnum.CS_E_DELIVERY.concat('.').concat(EDeliveryListColumnsEnum.E_DELIVERY_IDENTIFIER),
        display: '',
        width: 'cs-col-w-20',
        sortable: false
      },
      {
        id: EDeliveryListColumnsEnum.E_DELIVERY_IDENTIFIER_SCHEME,
        label: ListsEnum.CS_E_DELIVERY.concat('.').concat(EDeliveryListColumnsEnum.E_DELIVERY_IDENTIFIER_SCHEME),
        display: '',
        width: 'cs-col-w-20',
        sortable: false
      },
      {
        id: EDeliveryListColumnsEnum.E_DELIVERY_COUNTRY_NAME,
        label: ListsEnum.CS_E_DELIVERY.concat('.').concat(EDeliveryListColumnsEnum.E_DELIVERY_COUNTRY_NAME),
        filterId: EDeliveryListColumnsEnum.E_DELIVERY_COUNTRY_CODE,
        filterType: TableColumnFilterTypeEnum.COUNTRIES,
        display: 'country',
        placeholder: 'app.list.eDelivery.filter.country',
        width: 'cs-col-w-20',
        sortable: true
      },
      {
        id: EDeliveryListColumnsEnum.E_DELIVERY_STATUS,
        label: ListsEnum.CS_E_DELIVERY.concat('.').concat(EDeliveryListColumnsEnum.E_DELIVERY_STATUS),
        filterId: EDeliveryListColumnsEnum.E_DELIVERY_STATUS,
        filterType: TableColumnFilterTypeEnum.STATUS,
        display: 'userStatus',
        placeholder: 'app.list.eDelivery.filter.status',
        width: countryCode && countryCode !== 'EC' ? 'cs-col-w-10' : 'cs-col-w-30',
        sortable: true,
        translation: 'app.data.eDelivery.status_',
        items: this.getStatusList()
      }
    ];

    const filterLabel = {
      id: 'app.list.common.filters',
      filterId: TableColumnFilterTypeEnum.LABEL,
      filterType: TableColumnFilterTypeEnum.LABEL
    };

    if (countryCode === 'EC') {
      return {
        columns: [columns[0], columns[2], columns[1], columns[3], columns[4]],
        filterByColumns: [columns[0], filterLabel, columns[3], columns[4]]
      };
    }

    if (countryCode && countryCode !== 'EC') {
      return {
        columns: [columns[0], columns[2], columns[1], columns[4]],
        filterByColumns: [columns[0], filterLabel, columns[4]]
      };
    }
    return {
      columns: [columns[0], columns[2], columns[1], columns[3], columns[4]],
      filterByColumns: [columns[0], filterLabel, columns[3], columns[4]]
    };
  }

  changeEDeliveryStatus(status: EDeliveryStatusModel) {
    this.store.dispatch(eDeliveryActions.changeEDeliveryStatusAction(status));
  }

  validateEDelivery(eDelivery: EDeliveryValidateModel) {
    this.store.dispatch(eDeliveryActions.validateEDeliveryAction(eDelivery));
  }
}
