import { AbstractService } from '../abstract.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class EDeliveryApi extends AbstractService {
  constructor(protected http: HttpClient) {
    super(http);
  }

  getEDeliveryList(params): Observable<object> {
    return this.returnApiPostCall('edelivery/list', params);
  }

  saveEDelivery(params): Observable<object> {
    const action = params?.id ? `update` : 'save';
    return this.returnApiPostCall(`edelivery/${action}`, params);
  }

  deleteEDelivery(id): Observable<object> {
    return this.returnApiDeleteCall('edelivery', id);
  }

  changeEDeliveryStatus(params): Observable<object> {
    return this.returnApiPostCall(`edelivery/${params.id}/update-status/${params.status}`, {});
  }

  validateEDelivery(params): Observable<object> {
    return this.returnApiPostCall('edelivery/validate', params);
  }
}
