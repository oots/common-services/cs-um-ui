import { I18nService, UserPreferences, UserService } from '@eui/core';

import { EuiServiceStatus } from '@eui/base';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppStarterService {
  defaultUserPreferences: UserPreferences;

  constructor(
    protected userService: UserService,
    protected i18nService: I18nService,
    protected http: HttpClient
  ) {}

  i18nInit(): Observable<EuiServiceStatus> {
    return this.i18nService.init({ activeLang: 'en' });
  }

  i18nUpdate(language: string): void {
    this.i18nService.updateState({ activeLang: language });
  }
}
