import { Pipe, PipeTransform } from '@angular/core';
import { EuiMenuItem } from '@eui/components/eui-menu';
import { TranslateService } from '@ngx-translate/core';

@Pipe({ name: 'translateMenu' })
export class MenuTranslatePipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  transform(value: EuiMenuItem[]): EuiMenuItem[] {
    return value.map((menuItem) => this.translateMenuItem(menuItem));
  }

  private translateMenuItem(item: EuiMenuItem): EuiMenuItem {
    return {
      ...item,
      label: this.translateService.instant(item.label),
      children: item.children?.map((child) => this.translateMenuItem(child))
    };
  }
}
