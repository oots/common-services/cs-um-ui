import { FlagPipe } from './flag.pipe';

describe('FlagPipe', () => {
  it('create an instance', () => {
    const pipe = new FlagPipe();
    expect(pipe).toBeTruthy();
  });

  it('get eu flag', () => {
    const pipe = new FlagPipe();
    expect(pipe.transform('EC')).toBe('eu');
  });

  it('get country flag', () => {
    const pipe = new FlagPipe();
    expect(pipe.transform('FR')).toBe('fr');
  });
});
