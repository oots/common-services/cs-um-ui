import { Pipe, PipeTransform } from '@angular/core';
import { TableColumnSort, TableOptionsModel } from 'src/app/core/models/app/table.options.model';

@Pipe({ name: 'sortColumnIcon' })
export class SortColumnIconPipe implements PipeTransform {
  transform(value: TableOptionsModel, columnName: string): any {
    const sort: TableColumnSort = value.sort?.length > 0 ? value.sort[0] : {};
    return sort.fieldName === columnName ? 'eui-icon-sort-'.concat(sort.order) : 'eui-icon-sort';
  }
}
