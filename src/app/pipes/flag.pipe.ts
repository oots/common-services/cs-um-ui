import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flag',
  standalone: true
})
export class FlagPipe implements PipeTransform {
  transform(value: string): string {
    return value.toLowerCase() === 'ec' ? 'eu' : value.toLowerCase();
  }
}
