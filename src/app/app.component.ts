import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { AppStarterService } from './app-starter.service';
import { BaseComponent } from './components/base/base.component';
import { INACTIVITY_TIMEOUT } from '../config/global';
import { Subject } from 'rxjs';
import { UserService } from './services/user/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent extends BaseComponent {
  userActivity;
  userInactive: Subject<undefined> = new Subject();
  loading$ = this.userService.profile$.pipe(map((userProfile) => !userProfile || userProfile.loading));

  constructor(
    private appStarterService: AppStarterService,
    private userService: UserService,
    private router: Router
  ) {
    super();
  }

  initComponent(): void {
    this.userService.getUserProfile();
    this.userService.resetConfirmationAction();
    this.setTimeout();

    this.addSubscription(
      this.userInactive.subscribe(() => {
        this.logout();
      })
    );

    this.addSubscription(this.appStarterService.i18nInit().subscribe(() => {}));
  }

  setTimeout() {
    if (INACTIVITY_TIMEOUT > 0) {
      this.userActivity = setTimeout(() => {
        this.userInactive.next(undefined);
      }, INACTIVITY_TIMEOUT);
    }
  }

  logout() {
    this.userService.resetConfirmationAction();
    this.router.navigate(['/login']);
  }

  @HostListener('window:mousemove')
  @HostListener('window:keydown')
  refreshUserState() {
    if (INACTIVITY_TIMEOUT > 0) {
      clearTimeout(this.userActivity);
      this.setTimeout();
    }
  }
}
