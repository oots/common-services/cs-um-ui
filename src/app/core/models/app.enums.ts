export enum UserStatusEnum {
  USER_STATUS_INACTIVE = 0,
  USER_STATUS_ACTIVE = 1
}

export enum UserTypeIDEnum {
  USER_TYPE_NONE = 0,
  USER_TYPE_MS = 1,
  USER_TYPE_EC = 2
}

export enum UserListColumnsEnum {
  USER_NAME = 'userName',
  USER_EMAIL = 'email',
  USER_TYPE = 'userType',
  USER_COUNTRY_NAME = 'countryName',
  USER_COUNTRY_CODE = 'country',
  USER_COUNTRY = 'userCountry',
  STATUS = 'status',
  USER_UM_APP = 'userUMApp',
  USER_ADMIN_APP = 'userAdminApp',
  START_DATE = 'startDate',
  END_DATE = 'endDate'
}

export enum EventListColumnsEnum {
  SUBJECT = 'subjectPublicId',
  EVENT_TYPE = 'csEventTypeCode',
  EVENT_TYPE_NAME = 'eventTypeName',
  OBJECT = 'objectEntityPublicId',
  DATE_AND_TIME = 'timestamp'
}

export enum MainMenuItemsEnum {
  MENU_DASHBOARD = 'app.main.menu.dashboard',
  MENU_USER = 'app.main.menu.user',
  MENU_E_DELIVERY = 'app.main.menu.eDelivery',
  MENU_EVENTS = 'app.main.menu.events'
}

export enum HTTPStatusEnum {
  OK_200 = 200,
  CREATED_201 = 201,
  NO_CONTENT_204 = 204,
  REDIRECT_302 = 302,
  NOT_ACCEPTABLE_406 = 406,
  UNAUTHORIZED_401 = 401, //no credentials or invalid credentials
  FORBIDDEN_403 = 403, //not enough permissions
  NOT_FOUND_404 = 404,
  INTERNAL_SERVER_ERROR_500 = 500,
  BAD_REQUEST_400 = 400
}

export enum BreadcrumbLabelsEnum {
  DASHBOARD = 'app.main.breadcrumb.dashboard',
  USER_LIST = 'app.main.breadcrumb.userList',
  E_DELIVERY_LIST = 'app.main.breadcrumb.eDeliveryList',
  EVENTS_LIST = 'app.main.breadcrumb.eventsList'
}

export enum ListActionsEnum {
  VIEW = 'view',
  OPEN = 'open',
  TOGGLE = 'toggle',
  SELECT = 'select',
  EDIT = 'edit',
  CANCEL = 'cancel',
  DELETE = 'delete',
  LINK = 'link',
  SAVE = 'save',
  UPDATE = 'update',
  MORE = 'more',
  ERROR = 'error',
  POPUP_CONFIRM_DISCARD_CHANGES = 'popupConfirmDiscardChanges',
  POPUP_CONFIRM_BACK_TO_FORM = 'popupConfirmBackToForm',
  MAIN_OPEN_DISCARD_CHANGES = 'mainOpenDiscardChanges',
  MAIN_DISCARD_CHANGES = 'mainDiscardChanges',
  MAIN_LEAVE_FORM = 'mainLeaveForm',
  MAIN_CLOSE_ERROR = 'mainCLoseError',
  DELETE_USER = 'deleteUser',
  DELETE_USER_DISCARD_CHANGES = 'deleteUserDiscardChanges',
  DELETE_E_DELIVERY = 'deleteEDelivery',
  DELETE_E_DELIVERY_DISCARD_CHANGES = 'deleteEDeliveryDiscardChanges',

  FLOW1_CONFIRM_EXIT_PROCESS = 'flow1ConfirmExitProcess',
  FLOW1_CANCEL_EXIT_PROCESS = 'flow1CancelExitProcess',
  FLOW1_DISCARD = 'flow1Discard',
  FLOW1_GO_JURISDICTION = 'flow1GoJurisdiction',
  FLOW1_SAVE_GO_JURISDICTION = 'flow1SaveGoJurisdiction',
  FLOW1_UPDATE = 'flow1Update',
  FLOW1_GENERAL_INFORMATION = 'flow1GeneralInformation',
  FLOW1_CONFIRM = 'flow1Confirm',
  FLOW1_SAVE_AS_DRAFT = 'flow1SaveAsDraft',
  FLOW1_SAVE_FOR_REVIEW = 'flow1SaveForReview',
  FLOW1_PUBLISH = 'flow1Publish',
  FLOW1_DONE = 'flow1Done',
  FLOW1_ADD = 'flow1Add',

  FLOW2_CONFIRM_EXIT_PROCESS = 'flow2ConfirmExitProcess',
  FLOW2_CANCEL_EXIT_PROCESS = 'flow2CancelExitProcess',
  FLOW2_DISCARD = 'flow2Discard',
  FLOW2_UPDATE = 'flow2Update',
  FLOW2_CONFIRM = 'flow2Confirm',
  FLOW2_ADD = 'flow2Add'
}

export enum TableColumnFilterTypeEnum {
  INPUT = 'input',
  LABEL = 'label',
  SELECT = 'select',
  DB_SELECT = 'dbSelect',
  COUNTRIES = 'countries',
  COUNTRY = 'country',
  STATUS = 'status',
  EVENT_TYPE = 'eventType',
  TIMESTAMP = 'startEnd'
}

export enum ActionConfirmationEnum {
  USER_SAVE = 'userSave',
  USER_VALIDATE = 'userValidate',
  USER_DELETE = 'userDelete',
  USER_PROFILE_ERROR = 'userProfileError',
  GENERIC_ERROR = 'appGenericError',
  DISCARD_CHANGES_FORM = 'mainDiscardForm',
  USER_STATUS_UPDATE = 'userStatusUpdate',
  E_DELIVERY_SAVE = 'eDeliverySave',
  E_DELIVERY_VALIDATE = 'eDeliveryValidate',
  E_DELIVERY_DELETE = 'eDeliveryDelete',
  E_DELIVERY_UPDATE_STATUS = 'eDeliveryStatusUpdate'
}

export enum ListFilterEnum {
  USER_LIST = 'USER_LIST',
  E_DELIVERY_LIST = 'E_DELIVERY_LIST',
  EVENT_LIST = 'EVENT_LIST'
}

/*
 * Check UserRoleIds.const for enum mapping-ids
 * */
export const enum UserRoleEnum {
  EC_USER_MANAGER = 'EC_USER_MANAGER',
  MS_USER_MANAGER = 'MS_USER_MANAGER',
  EC_EDITOR = 'EC_EDITOR',
  MS_EB_EDITOR = 'MS_EB_EDITOR',
  MS_DSD_EDITOR = 'MS_DSD_EDITOR',
  MS_EB_ADMINISTRATOR = 'MS_EB_ADMINISTRATOR',
  MS_DSD_ADMINISTRATOR = 'MS_DSD_ADMINISTRATOR',
  OBSERVER = 'OBSERVER',
  ADMIN_USER_MANAGER_DELEGATE = 'ADMIN_USER_MANAGER_DELEGATE',
  MS_USER_MANAGER_DELEGATE = 'MS_USER_MANAGER_DELEGATE',
  REQ_ADMINISTRATOR = 'REQ_ADMINISTRATOR',
  REQ_EDITOR = 'REQ_EDITOR'
}

export enum UserTypesEnum {
  ADMIN, //root
  MEMBER_STATE,
  EC_MODERATOR,
  MS_COORDINATOR
}

export enum EventTypeEnum {
  USER_CREATED = 'USER_CREATED', //root
  USER_UPDATED_DETAILS = 'USER_UPDATED_DETAILS',
  USER_UPDATED_ROLES = 'USER_UPDATED_ROLES',
  USER_DISABLED = 'USER_DISABLED',
  USER_ENABLED = 'USER_ENABLED',
  USER_UPDATED_DATES = 'USER_UPDATED_DATES',
  EDELIVERY_CREATED = 'EDELIVERY_CREATED',
  EDELIVERY_UPDATED_DETAILS = 'EDELIVERY_UPDATED_DETAILS',
  EDELIVERY_DISABLED = 'EDELIVERY_DISABLED',
  EDELIVERY_ENABLED = 'EDELIVERY_ENABLED'
}

export enum EntityTypeEnum {
  USER, //root
  AP_NODE,
  PROCEDURE,
  EVIDENCE_TYPE,
  EVIDENCE_PROVIDER,
  REQUIREMENT,
  ACCESS_SERVICE
}

export const enum PermissionEnum {
  CS_APP_VIEW = 'CS_APP_VIEW',
  USER_APP_VIEW = 'USER_APP_VIEW',
  CREATE_USER = 'CREATE_USER',
  VIEW_USER = 'VIEW_USER',
  ASSIGN_ROLE = 'ASSIGN_ROLE',
  CRUD_EDELIVERY_NODE = 'CRUD_EDELIVERY_NODE',
  CRUD_SDG_PROCEDURE = 'CRUD_SDG_PROCEDURE',
  CRUD_MS_PROCEDURE = 'CRUD_MS_PROCEDURE',
  CRUD_REQUIREMENT = 'CRUD_REQUIREMENT',
  CRUD_EVIDENCE_TYPE = 'CRUD_EVIDENCE_TYPE',
  CRUD_EVIDENCE_PROVIDER = 'CRUD_EVIDENCE_PROVIDER',
  CRUD_DATA_SERVICE = 'CRUD_DATA_SERVICE',
  VALIDATE_MS_PROCEDURE = 'VALIDATE_MS_PROCEDURE',
  VALIDATE_REQUIREMENT = 'VALIDATE_REQUIREMENT',
  VALIDATE_EVIDENCE_TYPE = 'VALIDATE_EVIDENCE_TYPE',
  VALIDATE_EVIDENCE_PROVIDER = 'VALIDATE_EVIDENCE_PROVIDER',
  VALIDATE_DATA_SERVICE = 'VALIDATE_DATA_SERVICE'
}

export enum AccessRightsEnum {
  MANAGER = 'MANAGER',
  ADMINISTRATOR = 'ADMINISTRATOR',
  EDITOR = 'EDITOR',
  OBSERVER = 'OBSERVER',
  DSD = 'DSD',
  EB = 'EB'
}

export enum UserTypeEnum {
  EC_USER = 'EC_USER',
  MS_USER = 'MS_USER'
}

export const enum ListTypeEnum {
  STATUS = 'STATUS',
  COUNTRY = 'COUNTRY'
}

export enum UserPermissionEnum {
  CS_APP_VIEW = 'CS_APP_VIEW',
  USER_APP_VIEW = 'USER_APP_VIEW',
  CRUD_USER = 'CRUD_USER',
  CRUD_MS_USER = 'CRUD_MS_USER',
  CRUD_EDELIVERY = 'CRUD_EDELIVERY',
  VIEW_MS_EDELIVERY = 'VIEW_MS_EDELIVERY',
  FE_EDIT_MS_COUNTRY = 'FE_EDIT_MS_COUNTRY'
}

export enum DateFormatEnum {
  DD_MM_YYYY = 'dd/MM/yyyy',
  DD_MM_YYYY_HH_MM_SS = 'dd/MM/yyyy HH:mm:ss'
}

export enum TableOptionsEnum {
  PAGE_NUMBER = 1,
  PAGE_SIZE = 10,
  PAGE_SIZE_25 = 25,
  PAGE_SIZE_50 = 50
}

export enum StatusEnum {
  DRAFT = '0',
  IN_REVIEW = '1',
  PUBLISHED = '2',
  ARCHIVED = '3',
  //TODO remove
  ACTIVE = '4',
  INACTIVE = '5'
}

export enum ListsEnum {
  CS_USER = 'app.list.user.columns',
  CS_E_DELIVERY = 'app.list.eDelivery.columns',
  CS_EVENT = 'app.list.event.table.columns'
}

export enum AppFlowEnum {
  CREATE_USER = 1,
  CREATE_E_DELIVERY = 2
}

export enum ChipsAccessRightsEnum {
  EB = 'EB',
  DSD = 'DSD'
}

export enum EDeliveryStatusEnum {
  E_DELIVERY_STATUS_INACTIVE = 0,
  E_DELIVERY_STATUS_ACTIVE = 1
}

export enum EDeliveryListColumnsEnum {
  E_DELIVERY_NAME = 'name',
  E_DELIVERY_IDENTIFIER_SCHEME = 'schemeId',
  E_DELIVERY_IDENTIFIER = 'identifier',
  E_DELIVERY_COUNTRY_NAME = 'countryName',
  E_DELIVERY_COUNTRY_CODE = 'country',
  E_DELIVERY_STATUS = 'status'
}

export enum AuthFlowNamesEnum {
  EDELIVERY = 'EDELIVERY',
  USER = 'USER'
}
