import { UserPermissionEnum, UserStatusEnum } from 'src/app/core/models/app.enums';
import { UserRolesModel } from './user.model';

export interface UserProfileModel {
  roles?: UserRolesModel[];
  userId?: string;
  username?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  userType?: number;
  country?: string;
  countryName?: string;
  status?: number | UserStatusEnum;
  permissions?: UserPermissionEnum[];
  countryCode?: string;
  permissionList?: any[];
}
