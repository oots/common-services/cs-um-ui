import { TableOptionsModel } from '../app/table.options.model';
import { EDeliveryStatusEnum } from 'src/app/core/models/app.enums';

export interface EDeliveryModel {
  id?: string;
  country?: string;
  owner?: string;
  issuer?: string;
  name?: string;
  identifier?: string;
  schemeId?: string;
  status?: EDeliveryStatusEnum;
}

export interface EDeliveryListModel {
  list: EDeliveryModel[];
  count: number;
  request: TableOptionsModel;
}

export interface EDeliveryStatusModel {
  id?: string;
  status?: EDeliveryStatusEnum | number;
}

export interface EDeliveryValidateModel {
  id?: string;
  name?: string;
  schemeId?: string;
  identifier?: string;
  country?: string;
}
