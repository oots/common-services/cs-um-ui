export interface CountryModel {
  name?: string;
  code?: string;
  checked?: boolean;
  iconClass?: string;
}
