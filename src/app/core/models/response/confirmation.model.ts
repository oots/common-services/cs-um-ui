import { ActionConfirmationEnum } from '../app.enums';

export interface ConfirmationModel {
  loading?: boolean;
  error?: string;
  value?: ConfirmationValueModel;
}

export interface ConfirmationValueModel {
  name?: ActionConfirmationEnum;
  value?: number;
}
