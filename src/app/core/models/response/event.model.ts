import { TableOptionsModel } from '../app/table.options.model';
import { EntityTypeEnum, EventTypeEnum } from 'src/app/core/models/app.enums';

export interface EventModel {
  eventId?: string;
  subjectId?: string;
  subjectName?: string;
  subjectPublicId?: string;
  subjectEntityTypeCode?: EntityTypeEnum;
  csEventTypeCode?: EventTypeEnum;
  eventTypeName?: string;
  objectEntityId?: string;
  objectEntityName?: string;
  objectEntityPublicId?: string;
  objectEntityTypeCode?: EntityTypeEnum;
  associatedEntityId?: string;
  associatedEntityName?: string;
  associatedEntityPublicId?: string;
  associatedEntityTypeCode?: EntityTypeEnum;
  timestamp?: string;
  appId?: string;
  diffPayloadOld?: string;
  diffPayloadNew?: string;
}

export interface EventListModel {
  list: EventModel[];
  count: number;
  request: TableOptionsModel;
}
