import { TableOptionsModel } from '../app/table.options.model';
import { PermissionEnum, UserRoleEnum, UserStatusEnum, UserTypesEnum } from 'src/app/core/models/app.enums';

export interface UserModel {
  startDate?: any;
  endDate?: any;
  id?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  userName?: string;
  userType?: UserTypesEnum;
  country?: string;
  countryCode?: string;
  countryName?: string;
  lastModifiedBy?: string;
  status?: UserStatusEnum;
  permissionList?: PermissionModel[];
  roles?: UserRolesModel[];
  userRoles?: UserRoleEnum[];
  countryInfo?: any;
}

export interface UserListModel {
  list: UserModel[];
  count: number;
  request: TableOptionsModel;
}

export interface PermissionModel {
  id?: string;
  permissionName?: PermissionEnum;
  permissionDescription?: string;
  status?: UserStatusEnum;
  lastModifiedBy?: number;
  lastModified?: string;
}

export interface UserRolesModel {
  id?: string;
  appId?: string;
  roleName?: UserRoleEnum | string;
  status?: UserStatusEnum | number;
  lastModifiedBy?: number;
  lastModified?: string;
  maxAllowedDuration?: number;
}
export interface AssignableUserRolesModel {
  id?: string;
  name?: UserRoleEnum | string;
  roles?: UserRolesModel[];
}

export interface UserStatusModel {
  id?: string;
  status?: UserStatusEnum | number;
}

export interface UserValidateModel {
  id?: string;
  userName?: string;
  email?: string;
}

export interface SchemeModel {
  id?: string;
  url?: string;
}
