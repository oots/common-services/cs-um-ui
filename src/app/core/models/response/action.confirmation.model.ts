import { ActionConfirmationEnum } from 'src/app/core/models/app.enums';

export interface ActionConfirmationModel {
  name?: ActionConfirmationEnum;
  value?: any;
}
