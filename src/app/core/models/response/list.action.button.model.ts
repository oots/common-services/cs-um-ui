export interface ListActionButtonModel {
  id: string;
  tooltip?: string;
  tooltipParam?: any;
  icon?: string;
  iconClass?: string;
  iconSize?: string;
  iconSrc?: string;
  set?: string;
  label?: string;
  class?: string;
  color?: string;
  disabled?: boolean;
}
