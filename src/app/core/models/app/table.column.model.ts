export interface TableColumnFilterModel {
  id?: string;
  filterId?: string;
  filterType?: string;
  label?: string;
  display?: string;
  align?: string;
  placeholder?: string;
  placeholder2?: string;
  width?: string;
  translation?: string;
  items?: any[];
  placeholderKey?: string;
  hasHeaderCountry?: boolean;
  hasNew?: boolean;
  sortable?: boolean;
  hasEUFlag?: boolean;
  detailsLink?: boolean;
}
