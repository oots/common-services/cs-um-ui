import { ListFilterEnum } from 'src/app/core/models/app.enums';

export interface ListFilterModel {
  type: ListFilterEnum;
  filterValue?: any;
  apiParam?: any;
  sort?: any;
}
