export interface FlowModel {
  id: number;
  activeStep: number;
  flowTitle: string;
  editFlow?: boolean;
  data?: any;
  steps: FlowStepModel[];
  status?: string;
}

export interface FlowStepModel {
  id: number;
  label: string;
  infoKey?: string;
  titleKey?: string;
  titleInfoTitle?: string;
  titleInfoDescription?: string;
  descriptionKey?: string;
  info?: string;
}
