export interface TableDataOptionsModel {
  optionName?: string;
  optionValue?: string;
}

export interface TableColumnFilter {
  fieldName: string;
  fieldValues: string[];
}
export interface TableColumnSort {
  fieldName?: string;
  order?: string;
}
export interface TableOptionsModel {
  listType?: string;
  pageNumber?: number;
  pageSize?: number;
  option?: TableDataOptionsModel;
  filter?: TableColumnFilter[];
  sort?: TableColumnSort[];
}

export interface TableExternalFilter {
  filterSelection: any;
  filter: TableColumnFilter[];
  resetAllFilters: boolean;
}

export interface TableSettingsModel {
  addBtnKey?: string;
  addBtnIcon?: string;
  linkBtnKey?: string;
  hasFilter?: boolean;
  hasActions?: boolean;
  hasAddBtn?: boolean;
  hasAddBtnIcon?: boolean;
  hasLinkBtn?: boolean;
  expandableRows?: boolean;
  noRecordsText?: string;
  entityName?: string;
  labelName?: string;
  info?: string;
  detailsRowId?: number;
  hidePaginator?: boolean;
  hasLink?: boolean;
  hasRadioBtn?: boolean;
  countryName?: string;
  countryCode?: string;
  newId?: string;
  sortDataList?: boolean;
  tableClass?: string;
  tableDataType?: string;
  filterPosition?: string;
  hasRowSelection?: boolean;
}
