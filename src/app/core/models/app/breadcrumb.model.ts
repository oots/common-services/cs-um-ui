export interface BreadcrumbModel {
  label: string;
  text?: string;
  url?: string;
  icon?: string;
  tooltip?: string;
  iconTooltip?: string;
  count?: number;
}
