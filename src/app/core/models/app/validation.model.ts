export interface ValidationActionModel {
  path: string;
  param: any;
}

export interface ValidationErrorModel {
  error: string;
  attribute: string;
}
