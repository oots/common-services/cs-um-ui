import { CsStateKeys } from './cs.state';
import { reducer as commonReducer } from './common/common.reducer';
import { reducer as userReducer } from './user/user.reducer';
import { reducer as eDeliveryReducer } from './e-delivery/e-delivery.reducer';

export const ngrxReducers: CsStateKeys = {
  csUser: userReducer,
  csCommon: commonReducer,
  csEDelivery: eDeliveryReducer
};
