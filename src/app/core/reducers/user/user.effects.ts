import * as userActions from './user.actions';
import { ActionConfirmationEnum, HTTPStatusEnum } from '../../models/app.enums';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import { AppGrowlEnum } from '../../models/app/growl.model';
import { CommonService } from '../../../services/main/common.service';
import { Injectable } from '@angular/core';
import { UserApi } from '../../../services/user/user.api';
import { UserService } from '../../../services/user/user.service';
import { of } from 'rxjs';

@Injectable()
export class UserEffects {
  getUserProfileAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.getUserProfileAction),
      mergeMap(() => {
        return this.userApi.getUserProfile().pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return userActions.getUserProfileCompletedAction({
                ...res.data,
                userId: res.data?.id,
                country: res.data?.countryCode || res.data?.country || 'EC',
                status: res.data?.status
              });
            }
            return userActions.getUserProfileFailedAction({
              name: ActionConfirmationEnum.USER_PROFILE_ERROR,
              value: { ...res }
            });
          }),
          catchError((err) => {
            return of(
              userActions.getUserProfileFailedAction({
                name: ActionConfirmationEnum.USER_PROFILE_ERROR,
                value: { ...err, status: err.status || '-1' }
              })
            );
          })
        );
      })
    )
  );

  getUserListAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.getUserListAction),
      withLatestFrom(this.userService.tableOptions$.pipe(map((options) => options?.value))),
      mergeMap((options) => {
        return this.userApi.getUserList(options[1]).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return userActions.getUserListCompletedAction(res.data);
            } else {
              return userActions.getUserListFailedAction(res.status);
            }
          }),
          catchError(() => of(userActions.getUserListFailedAction('-1')))
        );
      })
    )
  );

  setListTableOptionsAction = createEffect(
    () =>
      this.actions$.pipe(
        ofType(userActions.setListTableOptionsAction),
        map(() => this.userService.getList())
      ),
    { dispatch: false }
  );

  saveUserAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.saveUserAction),
      mergeMap((action) => {
        return this.userApi.saveUser(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_SUCCESS,
                AppGrowlEnum.GROW_USER_HEADING,
                AppGrowlEnum.GROWL_USER_SAVE_SUCCESS
              );
              return userActions.saveUserCompletedAction({
                name: ActionConfirmationEnum.USER_SAVE,
                value: res.data
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_USER_HEADING,
                AppGrowlEnum.GROWL_USER_SAVE_ERROR
              );
              return userActions.saveUserFailedAction(res.status);
            }
          }),
          catchError(() => of(userActions.saveUserFailedAction('-1')))
        );
      })
    )
  );

  deleteUserAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.deleteUserAction),
      mergeMap((action) => {
        return this.userApi.deleteUser(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_SUCCESS,
                AppGrowlEnum.GROW_USER_HEADING,
                AppGrowlEnum.GROWL_USER_DELETE_SUCCESS
              );
              return userActions.deleteUserCompletedAction({
                name: ActionConfirmationEnum.USER_DELETE,
                value: res.data
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_USER_HEADING,
                AppGrowlEnum.GROWL_USER_DELETE_ERROR
              );
              return userActions.deleteUserFailedAction(res.status);
            }
          }),
          catchError(() => of(userActions.deleteUserFailedAction('-1')))
        );
      })
    )
  );

  getUserDetailsAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.getUserDetailsAction),
      mergeMap((action) => {
        return this.userApi.getUserDetails(action.payload).pipe(
          map((res: any) => {
            return userActions.getUserDetailsCompletedAction(res.data);
          }),
          catchError(() => of(userActions.getUserDetailsFailedAction('-1')))
        );
      })
    )
  );

  updateUserStatusAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.updateUserStatusAction),
      mergeMap((action) => {
        return this.userApi.updateUserStatus(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_SUCCESS,
                AppGrowlEnum.GROW_USER_STATUS_UPDATE_HEADING,
                AppGrowlEnum.GROWL_USER_STATUS_UPDATE_SUCCESS
              );
              return userActions.updateUserStatusCompletedAction({
                name: ActionConfirmationEnum.USER_STATUS_UPDATE,
                value: true
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_USER_STATUS_UPDATE_HEADING,
                AppGrowlEnum.GROWL_USER_STATUS_UPDATE_ERROR
              );
              return userActions.updateUserStatusFailedAction(res.status);
            }
          }),
          catchError(() => of(userActions.updateUserStatusFailedAction('-1')))
        );
      })
    )
  );

  validateUsernameAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.validateUsernameAction),
      mergeMap((action) => {
        return this.userApi.validateUsername(action.payload).pipe(
          map((res: any) => {
            if (res?.data === 0 && res.status == 200) {
              return userActions.validateUsernameCompletedAction(res.data);
            } else {
              return userActions.validateUsernameFailedAction('-1');
            }
          }),
          catchError(() => of(userActions.validateUsernameFailedAction('-1')))
        );
      })
    )
  );

  validateEmailAccountAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.validateEmailAccountAction),
      mergeMap((action) => {
        return this.userApi.validateEmailAccount(action.payload).pipe(
          map((res: any) => {
            if (res?.data === 0 && res.status == 200) {
              return userActions.validateEmailAccountCompletedAction(res.data);
            } else {
              return userActions.validateEmailAccountFailedAction('-1');
            }
          }),
          catchError(() => of(userActions.validateEmailAccountFailedAction('-1')))
        );
      })
    )
  );

  changeUserStatusAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.changeUserStatusAction),
      mergeMap((action) => {
        return this.userApi.changeUserStatus(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_SUCCESS,
                AppGrowlEnum.GROW_USER_STATUS_UPDATE_HEADING,
                AppGrowlEnum.GROWL_USER_STATUS_UPDATE_SUCCESS
              );
              this.userService.getList();
              return userActions.changeUserStatusCompletedAction({
                name: ActionConfirmationEnum.USER_STATUS_UPDATE,
                value: true
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_USER_HEADING,
                AppGrowlEnum.GROWL_USER_STATUS_UPDATE_ERROR
              );
              this.userService.getList();
              return userActions.changeUserStatusFailedAction(res.status);
            }
          }),
          catchError(() => of(userActions.changeUserStatusFailedAction('-1')))
        );
      })
    )
  );

  validateUserAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.validateUserAction),
      mergeMap((action) => {
        return this.userApi.validateUser(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return userActions.validateUserCompletedAction({
                name: ActionConfirmationEnum.USER_VALIDATE,
                value: res.data
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_USER_HEADING,
                AppGrowlEnum.GROWL_USER_VALIDATE_ERROR
              );
              return userActions.validateUserCompletedAction({
                name: ActionConfirmationEnum.USER_VALIDATE,
                value: -1
              });
            }
          }),
          catchError(() => of(userActions.validateUserFailedAction('-1')))
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private userApi: UserApi,
    private userService: UserService,
    private commonService: CommonService
  ) {}
}
