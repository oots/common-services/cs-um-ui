import { UserState } from './user.state';
import { createSelector } from '@ngrx/store';
import { getUserState } from '../cs.selectors';

export const getUserProfile = (userState: UserState) => userState.profile;
export const getList = (userState: UserState) => userState.list;
export const getTableOptions = (userState: UserState) => userState.tableOptions;
export const getUser = (userState: UserState) => userState.user;
export const getActionConfirmation = (userState: UserState) => userState.confirmAction;
export const getUserActionStatus = (userState: UserState) => userState.userStatusUpdate;

export const selectUserProfile = createSelector(getUserState, getUserProfile);
export const selectList = createSelector(getUserState, getList);
export const selectTableOptions = createSelector(getUserState, getTableOptions);
export const selectUser = createSelector(getUserState, getUser);
export const selectActionConfirmation = createSelector(getUserState, getActionConfirmation);
export const selectUserActionStatus = createSelector(getUserState, getUserActionStatus);
