import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { UserApi } from '../../../services/user/user.api';
import { UserEffects } from './user.effects';
import { UserService } from '../../../services/user/user.service';
import { reducer } from './user.reducer';

@NgModule({
  imports: [StoreModule.forFeature('csUser', reducer), EffectsModule.forFeature([UserEffects])],
  providers: [UserService, UserApi]
})
export class UserStateModule {}
