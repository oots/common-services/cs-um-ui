import * as userActions from './user.actions';

import { Action, createReducer, on } from '@ngrx/store';
import { initialUserState, UserState } from './user.state';

export const reducerFn = createReducer(
  initialUserState,

  on(userActions.getUserProfileAction, (state) => ({
    ...state,
    profile: { value: state.profile.value, loading: true, error: null }
  })),

  on(userActions.getUserProfileCompletedAction, (state, { payload }) => ({
    ...state,
    profile: { value: payload, loading: false, error: null }
  })),

  on(userActions.getUserProfileFailedAction, (state, { payload }) => ({
    ...state,
    profile: { value: state.profile.value, loading: false, error: payload.value },
    confirmAction: { value: payload, loading: false, error: false }
  })),

  on(userActions.getUserListAction, (state) => ({
    ...state,
    list: { value: state.list.value, loading: true, error: null }
  })),

  on(userActions.getUserListCompletedAction, (state, { payload }) => ({
    ...state,
    list: { value: payload, loading: false, error: null }
  })),

  on(userActions.getUserListFailedAction, (state, { payload }) => ({
    ...state,
    list: { value: null, loading: false, error: payload }
  })),

  on(userActions.setListTableOptionsAction, (state, { payload }) => ({
    ...state,
    tableOptions: { value: payload, loading: false, error: null }
  })),

  on(userActions.saveUserAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(userActions.saveUserCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(userActions.saveUserFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  })),

  on(userActions.deleteUserAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(userActions.deleteUserCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(userActions.deleteUserFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  })),

  on(userActions.resetConfirmationAction, (state) => ({
    ...state,
    confirmAction: initialUserState.confirmAction
  })),

  on(userActions.getUserDetailsAction, (state) => ({
    ...state,
    user: { value: null, loading: true, error: null }
  })),

  on(userActions.getUserDetailsCompletedAction, (state, { payload }) => ({
    ...state,
    user: { value: payload, loading: false, error: null }
  })),

  on(userActions.getUserDetailsFailedAction, (state, { payload }) => ({
    ...state,
    user: { value: null, loading: false, error: payload }
  })),

  on(userActions.updateUserStatusAction, (state) => ({
    ...state,
    userStatusUpdate: { value: null, loading: true, error: null }
  })),

  on(userActions.updateUserStatusCompletedAction, (state, { payload }) => ({
    ...state,
    userStatusUpdate: { value: payload, loading: false, error: null }
  })),

  on(userActions.updateUserStatusFailedAction, (state, { payload }) => ({
    ...state,
    userStatusUpdate: { value: null, loading: false, error: payload }
  })),

  on(userActions.validateUsernameAction, (state) => ({
    ...state,
    validateUsername: { value: null, loading: true, error: null }
  })),

  on(userActions.validateUsernameCompletedAction, (state, { payload }) => ({
    ...state,
    validateUsername: { value: payload, loading: false, error: null }
  })),

  on(userActions.validateUsernameFailedAction, (state, { payload }) => ({
    ...state,
    validateUsername: { value: null, loading: false, error: payload }
  })),

  on(userActions.validateEmailAccountAction, (state) => ({
    ...state,
    validateEmail: { value: null, loading: true, error: null }
  })),

  on(userActions.validateEmailAccountCompletedAction, (state, { payload }) => ({
    ...state,
    validateEmail: { value: payload, loading: false, error: null }
  })),

  on(userActions.validateEmailAccountFailedAction, (state, { payload }) => ({
    ...state,
    validateEmail: { value: null, loading: false, error: payload }
  })),

  on(userActions.resetStateAction, () => initialUserState),

  on(userActions.changeUserStatusAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(userActions.changeUserStatusCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(userActions.changeUserStatusFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  })),

  on(userActions.validateUserAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(userActions.validateUserCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(userActions.validateUserFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  }))
);

export function reducer(state = initialUserState, action: Action): UserState {
  return reducerFn(state, action);
}
