import { UserListModel, UserModel } from '../../models/response/user.model';

import { ActionConfirmationModel } from '../../models/response/action.confirmation.model';
import { AsyncStoreObject } from '../cs.utils';
import { TableOptionsModel } from '../../models/app/table.options.model';
import { UserProfileModel } from '../../models/response/user.profile';

export interface UserState {
  profile: AsyncStoreObject<UserProfileModel>;
  list: AsyncStoreObject<UserListModel>;
  tableOptions: AsyncStoreObject<TableOptionsModel>;
  user: AsyncStoreObject<UserModel>;
  confirmAction: AsyncStoreObject<ActionConfirmationModel>;
  userStatusUpdate: AsyncStoreObject<ActionConfirmationModel>;
}

export const initialUserState: UserState = {
  profile: { value: null, loading: false, error: null },
  list: { value: null, loading: false, error: null },
  tableOptions: {
    value: {
      pageNumber: 0,
      pageSize: 25
    },
    loading: false,
    error: null
  },
  user: { value: null, loading: false, error: null },
  confirmAction: { value: null, loading: false, error: null },
  userStatusUpdate: { value: null, loading: false, error: false }
};
