import { UserListModel, UserModel, UserStatusModel, UserValidateModel } from '../../models/response/user.model';

import { ActionConfirmationModel } from '../../models/response/action.confirmation.model';
import { TableOptionsModel } from '../../models/app/table.options.model';
import { UserProfileModel } from '../../models/response/user.profile';
import { createAction } from '@ngrx/store';

export const getUserProfileAction = createAction('[csUser] Get user profile');

export const getUserProfileCompletedAction = createAction('[csUser] Get user profile completed', (payload: UserProfileModel) => ({
  payload
}));

export const getUserProfileFailedAction = createAction('[csUser] Get user profile failed', (payload: ActionConfirmationModel) => ({
  payload
}));

export const resetStateAction = createAction('[csUser] Reset state');

export const getUserListAction = createAction('[csUser] Get user list');

export const getUserListCompletedAction = createAction('[csUser] Get user list completed', (payload: UserListModel) => ({ payload }));

export const getUserListFailedAction = createAction('[csUser] Get user list failed', (payload: string) => ({ payload }));

export const setListTableOptionsAction = createAction('[csUser] Set user table options', (payload: TableOptionsModel) => ({ payload }));

export const resetListTableOptionsAction = createAction('[csUser] Reset user table options');

export const getUserDetailsAction = createAction('[csUser] Get user details', (payload: number) => ({ payload }));

export const getUserDetailsCompletedAction = createAction('[csUser] Get user details completed', (payload: UserModel) => ({ payload }));

export const getUserDetailsFailedAction = createAction('[csUser] Get user details failed', (payload: string) => ({ payload }));

export const saveUserAction = createAction('[csUser] Save user', (payload: UserModel) => ({ payload }));

export const saveUserCompletedAction = createAction('[csUser] Save user completed', (payload: ActionConfirmationModel) => ({ payload }));

export const saveUserFailedAction = createAction('[csUser] Save user failed', (payload: string) => ({ payload }));

export const deleteUserAction = createAction('[csUser] Delete user action', (payload: number) => ({ payload }));

export const deleteUserCompletedAction = createAction('[csUser] Delete user completed action', (payload: ActionConfirmationModel) => ({
  payload
}));

export const deleteUserFailedAction = createAction('[csUser] Delete user failed action', (payload: string) => ({ payload }));

export const resetConfirmationAction = createAction('[csUser] Reset Action Confirmation');

export const updateUserStatusAction = createAction('[csUser] Update user status action', (payload: { id: number; status: number }) => ({
  payload
}));

export const updateUserStatusCompletedAction = createAction(
  '[csUser] Update user status completed',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const updateUserStatusFailedAction = createAction('[csUser] Update user status failed', (payload: string) => ({ payload }));

export const validateUsernameAction = createAction('[csUser] Validate username action', (payload: string) => ({ payload }));

export const validateUsernameCompletedAction = createAction('[csUser] Validate username completed', (payload: ActionConfirmationModel) => ({
  payload
}));

export const validateUsernameFailedAction = createAction('[csUser] Validate username failed', (payload: string) => ({ payload }));

export const validateEmailAccountAction = createAction('[csUser] Validate email account action', (payload: string) => ({ payload }));

export const validateEmailAccountCompletedAction = createAction(
  '[csUser] Validate email account completed',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const validateEmailAccountFailedAction = createAction('[csUser] Validate email account failed', (payload: string) => ({ payload }));

export const changeUserStatusAction = createAction('[csUser] Change user status action', (payload: UserStatusModel) => ({ payload }));

export const changeUserStatusCompletedAction = createAction(
  '[csUser] Change user status completed',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const changeUserStatusFailedAction = createAction('[csUser] Change user status failed', (payload: string) => ({ payload }));

export const validateUserAction = createAction('[csUser] Validate user data action', (payload: UserValidateModel) => ({ payload }));

export const validateUserCompletedAction = createAction(
  '[csUser] Validate user data completed action',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const validateUserFailedAction = createAction('[csUser] Validate user data failed action', (payload: string) => ({ payload }));
