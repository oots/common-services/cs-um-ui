import { CsState } from './cs.state';

export const getUserState = (state: CsState) => state.csUser;
export const getEDeliveryState = (state: CsState) => state.csEDelivery;
export const getCommonState = (state: CsState) => state.csCommon;
export const getEventState = (state: CsState) => state.csEvent;
