import { createSelector } from '@ngrx/store';
import { getEDeliveryState } from '../cs.selectors';
import { EDeliveryState } from 'src/app/core/reducers/e-delivery/e-delivery.state';

export const getList = (eDeliveryState: EDeliveryState) => eDeliveryState.list;
export const getTableOptions = (eDeliveryState: EDeliveryState) => eDeliveryState.tableOptions;
export const getActionConfirmation = (eDeliveryState: EDeliveryState) => eDeliveryState.confirmAction;

export const selectList = createSelector(getEDeliveryState, getList);
export const selectTableOptions = createSelector(getEDeliveryState, getTableOptions);
export const selectActionConfirmation = createSelector(getEDeliveryState, getActionConfirmation);
