import * as eDeliveryActions from './e-delivery.actions';
import { Action, createReducer, on } from '@ngrx/store';
import { initialEDeliveryState, EDeliveryState } from './e-delivery.state';

export const reducerFn = createReducer(
  initialEDeliveryState,

  on(eDeliveryActions.getEDeliveryListAction, (state) => ({
    ...state,
    list: { value: state.list.value, loading: true, error: null }
  })),

  on(eDeliveryActions.getEDeliveryListCompletedAction, (state, { payload }) => ({
    ...state,
    list: { value: payload, loading: false, error: null }
  })),

  on(eDeliveryActions.getEDeliveryListFailedAction, (state, { payload }) => ({
    ...state,
    list: { value: null, loading: false, error: payload }
  })),

  on(eDeliveryActions.setListTableOptionsAction, (state, { payload }) => ({
    ...state,
    tableOptions: { value: payload, loading: false, error: null }
  })),

  on(eDeliveryActions.saveEDeliveryAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(eDeliveryActions.saveEDeliveryCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(eDeliveryActions.saveEDeliveryFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  })),

  on(eDeliveryActions.deleteEDeliveryAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(eDeliveryActions.deleteEDeliveryCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(eDeliveryActions.deleteEDeliveryFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  })),

  on(eDeliveryActions.resetConfirmationAction, (state) => ({
    ...state,
    confirmAction: initialEDeliveryState.confirmAction
  })),

  on(eDeliveryActions.resetStateAction, () => initialEDeliveryState),

  on(eDeliveryActions.changeEDeliveryStatusAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(eDeliveryActions.changeEDeliveryStatusCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(eDeliveryActions.changeEDeliveryStatusFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  })),

  on(eDeliveryActions.validateEDeliveryAction, (state) => ({
    ...state,
    confirmAction: { value: null, loading: true, error: null }
  })),

  on(eDeliveryActions.validateEDeliveryCompletedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(eDeliveryActions.validateEDeliveryFailedAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: null, loading: false, error: payload }
  }))
);

export function reducer(state = initialEDeliveryState, action: Action): EDeliveryState {
  return reducerFn(state, action);
}
