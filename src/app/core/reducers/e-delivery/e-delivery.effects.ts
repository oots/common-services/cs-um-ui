import * as eDeliveryActions from './e-delivery.actions';

import { ActionConfirmationEnum, HTTPStatusEnum } from '../../models/app.enums';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, take, withLatestFrom } from 'rxjs/operators';

import { AppGrowlEnum } from '../../models/app/growl.model';
import { CommonService } from '../../../services/main/common.service';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { EDeliveryApi } from 'src/app/services/e-delivery/e-delivery.api';
import { EDeliveryService } from 'src/app/services/e-delivery/e-delivery.service';

@Injectable()
export class EDeliveryEffects {
  getEDeliveryListAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(eDeliveryActions.getEDeliveryListAction),
      withLatestFrom(this.eDeliveryService.tableOptions$.pipe(map((options) => options?.value))),
      mergeMap((options) => {
        return this.eDeliveryApi.getEDeliveryList(options[1]).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return eDeliveryActions.getEDeliveryListCompletedAction(res.data);
            } else {
              return eDeliveryActions.getEDeliveryListFailedAction(res.status);
            }
          }),
          catchError(() => of(eDeliveryActions.getEDeliveryListFailedAction('-1')))
        );
      })
    )
  );

  setListTableOptionsAction = createEffect(
    () =>
      this.actions$.pipe(
        ofType(eDeliveryActions.setListTableOptionsAction),
        map(() => this.eDeliveryService.getList())
      ),
    { dispatch: false }
  );

  saveEDeliveryAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(eDeliveryActions.saveEDeliveryAction),
      mergeMap((action) => {
        return this.eDeliveryApi.saveEDelivery(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_SUCCESS,
                AppGrowlEnum.GROW_E_DELIVERY_HEADING,
                AppGrowlEnum.GROWL_E_DELIVERY_SAVE_SUCCESS
              );
              return eDeliveryActions.saveEDeliveryCompletedAction({
                name: ActionConfirmationEnum.E_DELIVERY_SAVE,
                value: res.data
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_E_DELIVERY_HEADING,
                AppGrowlEnum.GROWL_E_DELIVERY_SAVE_ERROR
              );
              return eDeliveryActions.saveEDeliveryFailedAction(res.status);
            }
          }),
          catchError(() => of(eDeliveryActions.saveEDeliveryFailedAction('-1')))
        );
      })
    )
  );

  deleteEDeliveryAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(eDeliveryActions.deleteEDeliveryAction),
      mergeMap((action) => {
        return this.eDeliveryApi.deleteEDelivery(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_SUCCESS,
                AppGrowlEnum.GROW_E_DELIVERY_HEADING,
                AppGrowlEnum.GROWL_E_DELIVERY_DELETE_SUCCESS
              );
              this.eDeliveryService.tableOptions$.pipe(take(1)).subscribe((options) => {
                this.eDeliveryService.setTableOption({ ...options.value, pageNumber: 1 });
              });
              return eDeliveryActions.deleteEDeliveryCompletedAction({
                name: ActionConfirmationEnum.E_DELIVERY_DELETE,
                value: res.data
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_E_DELIVERY_HEADING,
                AppGrowlEnum.GROWL_E_DELIVERY_DELETE_ERROR
              );
              return eDeliveryActions.deleteEDeliveryFailedAction(res.status);
            }
          }),
          catchError(() => of(eDeliveryActions.deleteEDeliveryFailedAction('-1')))
        );
      })
    )
  );

  changeEDeliveryStatusAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(eDeliveryActions.changeEDeliveryStatusAction),
      mergeMap((action) => {
        return this.eDeliveryApi.changeEDeliveryStatus(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_SUCCESS,
                AppGrowlEnum.GROW_E_DELIVERY_HEADING,
                AppGrowlEnum.GROWL_E_DELIVERY_STATUS_UPDATE_SUCCESS
              );
              this.eDeliveryService.getList();
              return eDeliveryActions.changeEDeliveryStatusCompletedAction({
                name: ActionConfirmationEnum.E_DELIVERY_UPDATE_STATUS,
                value: true
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_E_DELIVERY_HEADING,
                AppGrowlEnum.GROWL_E_DELIVERY_STATUS_UPDATE_ERROR
              );
              this.eDeliveryService.getList();
              return eDeliveryActions.changeEDeliveryStatusFailedAction(res.status);
            }
          }),
          catchError(() => of(eDeliveryActions.changeEDeliveryStatusFailedAction('-1')))
        );
      })
    )
  );

  validateEDeliveryAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(eDeliveryActions.validateEDeliveryAction),
      mergeMap((action) => {
        return this.eDeliveryApi.validateEDelivery(action.payload).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return eDeliveryActions.validateEDeliveryCompletedAction({
                name: ActionConfirmationEnum.E_DELIVERY_VALIDATE,
                value: res.data
              });
            } else {
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROW_E_DELIVERY_HEADING,
                AppGrowlEnum.GROWL_E_DELIVERY_VALIDATE_SUCCESS
              );
              return eDeliveryActions.validateEDeliveryCompletedAction({
                name: ActionConfirmationEnum.E_DELIVERY_VALIDATE,
                value: -1
              });
            }
          }),
          catchError(() => of(eDeliveryActions.validateEDeliveryFailedAction('-1')))
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private eDeliveryApi: EDeliveryApi,
    private eDeliveryService: EDeliveryService,
    private commonService: CommonService
  ) {}
}
