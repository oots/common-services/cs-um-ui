import { UserStatusModel } from '../../models/response/user.model';
import { ActionConfirmationModel } from '../../models/response/action.confirmation.model';
import { TableOptionsModel } from '../../models/app/table.options.model';
import { createAction } from '@ngrx/store';
import { EDeliveryListModel, EDeliveryModel, EDeliveryValidateModel } from 'src/app/core/models/response/e-delivery.model';

export const resetStateAction = createAction('[csEDelivery] Reset state');

export const getEDeliveryListAction = createAction('[csEDelivery] Get eDelivery nodes list action');

export const getEDeliveryListCompletedAction = createAction(
  '[csEDelivery] Get eDelivery nodes list completed action',
  (payload: EDeliveryListModel) => ({ payload })
);

export const getEDeliveryListFailedAction = createAction('[csEDelivery] Get eDelivery nodes list failed action', (payload: string) => ({
  payload
}));

export const setListTableOptionsAction = createAction('[csEDelivery] Set user table options', (payload: TableOptionsModel) => ({
  payload
}));

export const resetListTableOptionsAction = createAction('[csEDelivery] Reset user table options');

export const saveEDeliveryAction = createAction('[csEDelivery] Save eDelivery node action', (payload: EDeliveryModel) => ({ payload }));

export const saveEDeliveryCompletedAction = createAction(
  '[csEDelivery] Save eDelivery node completed action',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const saveEDeliveryFailedAction = createAction('[csEDelivery] Save eDelivery node failed action', (payload: string) => ({
  payload
}));

export const deleteEDeliveryAction = createAction('[csEDelivery] Delete eDelivery node action', (payload: number) => ({ payload }));

export const deleteEDeliveryCompletedAction = createAction(
  '[csEDelivery] Delete eDelivery node completed action',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const deleteEDeliveryFailedAction = createAction('[csEDelivery] Delete eDelivery node failed action', (payload: string) => ({
  payload
}));

export const resetConfirmationAction = createAction('[csEDelivery] Reset Action Confirmation');

export const changeEDeliveryStatusAction = createAction(
  '[csEDelivery] Change eDelivery node status action',
  (payload: UserStatusModel) => ({ payload })
);

export const changeEDeliveryStatusCompletedAction = createAction(
  '[csEDelivery] Change eDelivery node status completed action',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const changeEDeliveryStatusFailedAction = createAction(
  '[csEDelivery] Change eDelivery node status failed action',
  (payload: string) => ({ payload })
);

export const validateEDeliveryAction = createAction(
  '[csEDelivery] Validate eDelivery node data action',
  (payload: EDeliveryValidateModel) => ({ payload })
);

export const validateEDeliveryCompletedAction = createAction(
  '[csEDelivery] Validate eDelivery node data completed action',
  (payload: ActionConfirmationModel) => ({ payload })
);

export const validateEDeliveryFailedAction = createAction(
  '[csEDelivery] Validate eDelivery node data failed action',
  (payload: string) => ({ payload })
);
