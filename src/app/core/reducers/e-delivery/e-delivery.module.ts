import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { reducer } from './e-delivery.reducer';
import { EDeliveryService } from 'src/app/services/e-delivery/e-delivery.service';
import { EDeliveryApi } from 'src/app/services/e-delivery/e-delivery.api';
import { EDeliveryEffects } from 'src/app/core/reducers/e-delivery/e-delivery.effects';

@NgModule({
  imports: [StoreModule.forFeature('csEDelivery', reducer), EffectsModule.forFeature([EDeliveryEffects])],
  providers: [EDeliveryService, EDeliveryApi]
})
export class EDeliveryStateModule {}
