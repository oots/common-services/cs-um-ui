import { ActionConfirmationModel } from '../../models/response/action.confirmation.model';
import { AsyncStoreObject } from '../cs.utils';
import { TableOptionsModel } from '../../models/app/table.options.model';
import { EDeliveryListModel } from 'src/app/core/models/response/e-delivery.model';

export interface EDeliveryState {
  list: AsyncStoreObject<EDeliveryListModel>;
  tableOptions: AsyncStoreObject<TableOptionsModel>;
  confirmAction: AsyncStoreObject<ActionConfirmationModel>;
}

export const initialEDeliveryState: EDeliveryState = {
  list: { value: null, loading: false, error: null },
  tableOptions: {
    value: {
      pageNumber: 0,
      pageSize: 25
    },
    loading: false,
    error: null
  },
  confirmAction: { value: null, loading: false, error: null }
};
