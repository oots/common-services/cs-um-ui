import { InjectionToken } from '@angular/core';
import { ActionReducer, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { CoreState, localStorageSync, reducers as coreReducers } from '@eui/core';

import { environment } from 'src/environments/environment';
import { CsState } from './cs.state';
import { ngrxReducers } from './cs.reducer';
import { storeLogger } from 'ngrx-store-logger';

export const REDUCER_TOKEN = new InjectionToken<any>('Registered Reducers');

/**
 * Define here your app state
 *
 * [IMPORTANT]
 * There are some **reserved** slice of the state
 * that you **can not** use in your application ==> app |user | notification
 */

/* tslint:disable-next-line */
export interface AppState extends CoreState {
  cs: CsState;
}

/**
 * Define here the reducers of your app
 */
const rootReducer = Object.assign({}, coreReducers, ngrxReducers);

export function getReducers() {
  return rootReducer;
}

export const storeLoggerConst = storeLogger();

export function storeLoggerWrapper(reducer): ActionReducer<any, any> {
  return function (state, action) {
    const newReducer = storeLoggerConst(reducer);
    return newReducer(state, action);
  };
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? [localStorageSync, storeFreeze, storeLoggerWrapper]
  : [localStorageSync];
