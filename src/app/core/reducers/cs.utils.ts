export interface AsyncStoreObject<T> {
  value: T;
  loading: boolean;
  error: any;
}
