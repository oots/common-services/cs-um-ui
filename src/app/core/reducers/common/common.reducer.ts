import * as commonActions from 'src/app/core/reducers/common/common.actions';

import { Action, createReducer, on } from '@ngrx/store';
import { CommonState, initialCommonState } from 'src/app/core/reducers/common/common.state';

export const reducerFn = createReducer(
  initialCommonState,

  on(commonActions.setConfirmationAction, (state, { payload }) => ({
    ...state,
    confirmAction: { value: payload, loading: false, error: null }
  })),

  on(commonActions.setCurrentFilterAction, (state, { payload }) => ({
    ...state,
    filter: { value: payload, loading: false, error: null }
  })),

  on(commonActions.getCountryListAction, (state) => ({
    ...state,
    countries: { value: state.countries.value, loading: true, error: null }
  })),

  on(commonActions.getCountryListCompletedAction, (state, { payload }) => ({
    ...state,
    countries: { value: payload, loading: false, error: null }
  })),

  on(commonActions.getCountryListFailedAction, (state, { payload }) => ({
    ...state,
    countries: { value: null, loading: false, error: payload }
  })),

  on(commonActions.setFlowDataAction, (state, { payload }) => ({
    ...state,
    flow: { value: payload, loading: false, error: null }
  })),

  on(commonActions.getRolesListAction, (state) => ({
    ...state,
    roles: { value: state.roles.value, loading: true, error: null }
  })),

  on(commonActions.getAssignableRolesListAction, (state) => ({
    ...state,
    roles: { value: state.assignableRoles.value, loading: true, error: null }
  })),

  on(commonActions.getRolesListCompletedAction, (state, { payload }) => ({
    ...state,
    roles: { value: payload, loading: false, error: null }
  })),
  on(commonActions.getAssignableRolesListCompletedAction, (state, { payload }) => ({
    ...state,
    assignableRoles: { value: payload, loading: false, error: null }
  })),

  on(commonActions.getRolesListFailedAction, (state, { payload }) => ({
    ...state,
    roles: { value: null, loading: false, error: payload }
  })),

  on(commonActions.getAssignableRolesListFailedAction, (state, { payload }) => ({
    ...state,
    roles: { value: null, loading: false, error: payload }
  })),

  on(commonActions.getSchemesListAction, (state) => ({
    ...state,
    schemes: { value: state.schemes.value, loading: true, error: null }
  })),

  on(commonActions.getSchemesListCompletedAction, (state, { payload }) => ({
    ...state,
    schemes: { value: payload, loading: false, error: null }
  })),

  on(commonActions.getSchemesListFailedAction, (state, { payload }) => ({
    ...state,
    schemes: { value: null, loading: false, error: payload }
  }))
);

export function reducer(state = initialCommonState, action: Action): CommonState {
  return reducerFn(state, action);
}
