import { ActionConfirmationModel } from 'src/app/core/models/response/action.confirmation.model';
import { AsyncStoreObject } from '../cs.utils';
import { CountryModel } from '../../models/response/country.model';
import { ListFilterModel } from 'src/app/core/models/app/filter.model';
import { FlowModel } from 'src/app/core/models/app/flow.model';
import { AssignableUserRolesModel, SchemeModel, UserRolesModel } from 'src/app/core/models/response/user.model';

export interface CommonState {
  confirmAction: AsyncStoreObject<ActionConfirmationModel>;
  filter: AsyncStoreObject<ListFilterModel>;
  countries: AsyncStoreObject<CountryModel[]>;
  flow: AsyncStoreObject<FlowModel>;
  roles: AsyncStoreObject<UserRolesModel[]>;
  assignableRoles: AsyncStoreObject<AssignableUserRolesModel[]>;
  schemes: AsyncStoreObject<SchemeModel[]>;
}

export const initialCommonState: CommonState = {
  confirmAction: { value: null, loading: false, error: null },
  filter: { value: null, loading: false, error: null },
  countries: { value: null, loading: false, error: null },
  flow: { value: null, loading: false, error: null },
  roles: { value: null, loading: false, error: null },
  assignableRoles: { value: null, loading: false, error: null },
  schemes: { value: null, loading: false, error: null }
};
