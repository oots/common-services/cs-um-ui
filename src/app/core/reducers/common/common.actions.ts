import { CountryModel } from '../../models/response/country.model';
import { ListFilterModel } from 'src/app/core/models/app/filter.model';
import { createAction } from '@ngrx/store';
import { AssignableUserRolesModel, SchemeModel, UserRolesModel } from 'src/app/core/models/response/user.model';

export const setConfirmationAction = createAction('[csCommon] Set Action Confirmation', (payload: any) => ({ payload }));

export const setCurrentFilterAction = createAction('[csCommon] Set/reset app list filter', (payload: ListFilterModel) => ({ payload }));

export const getCountryListAction = createAction('[csCommon] Get country list');

export const getCountryListCompletedAction = createAction('[csCommon] Get country list completed', (payload: CountryModel[]) => ({
  payload
}));

export const getCountryListFailedAction = createAction('[csCommon] Get country list failed', (payload: string) => ({ payload }));

export const setFlowDataAction = createAction('[csCommon] Set current flow data', (payload: any) => ({ payload }));

export const getRolesListAction = createAction('[csCommon] Get roles list');
export const getAssignableRolesListAction = createAction('[csCommon] Get assignable roles list');

export const getRolesListCompletedAction = createAction('[csCommon] Get roles list completed', (payload: UserRolesModel[]) => ({
  payload
}));

export const getAssignableRolesListCompletedAction = createAction(
  '[csCommon] Get assignable roles list completed',
  (payload: AssignableUserRolesModel[]) => ({
    payload
  })
);

export const getRolesListFailedAction = createAction('[csCommon] Get roles list failed', (payload: string) => ({ payload }));
export const getAssignableRolesListFailedAction = createAction('[csCommon] Get assignable roles list failed', (payload: string) => ({
  payload
}));

export const getSchemesListAction = createAction('[csCommon] Get schemes list');

export const getSchemesListCompletedAction = createAction('[csCommon] Get schemes list completed', (payload: SchemeModel[]) => ({
  payload
}));

export const getSchemesListFailedAction = createAction('[csCommon] Get schemes list failed', (payload: string) => ({ payload }));
