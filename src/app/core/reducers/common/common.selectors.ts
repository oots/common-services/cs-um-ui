import { CommonState } from 'src/app/core/reducers/common/common.state';
import { createSelector } from '@ngrx/store';
import { getCommonState } from '../cs.selectors';

export const getActionConfirmation = (commonState: CommonState) => commonState.confirmAction;
export const getFilter = (commonState: CommonState) => commonState.filter;
export const getCountriesList = (commonState: CommonState) => commonState.countries;
export const getFlow = (commonState: CommonState) => commonState.flow;
export const getRolesList = (commonState: CommonState) => commonState.roles;
export const getAssignableRolesList = (commonState: CommonState) => commonState.assignableRoles;
export const getSchemesList = (commonState: CommonState) => commonState.schemes;

export const selectCountriesList = createSelector(getCommonState, getCountriesList);
export const selectActionConfirmation = createSelector(getCommonState, getActionConfirmation);
export const selectFilter = createSelector(getCommonState, getFilter);
export const selectFlow = createSelector(getCommonState, getFlow);
export const selectRolesList = createSelector(getCommonState, getRolesList);
export const selectAssignableRolesList = createSelector(getCommonState, getAssignableRolesList);
export const selectSchemesList = createSelector(getCommonState, getSchemesList);
