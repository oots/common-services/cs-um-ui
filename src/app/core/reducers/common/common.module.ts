import { CommonDataService } from 'src/app/services/main/common-data/common-data.service';
import { CommonEffects } from './common.effects';
import { CommonService } from 'src/app/services/main/common.service';
import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { reducer } from 'src/app/core/reducers/common/common.reducer';
import { ValidationApi } from '../../../services/main/validation/validation.api';

@NgModule({
  imports: [StoreModule.forFeature('csCommon', reducer), EffectsModule.forFeature([CommonEffects])],
  providers: [CommonService, CommonDataService, ValidationApi]
})
export class CommonStateModule {}
