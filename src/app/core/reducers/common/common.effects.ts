import * as commonActions from './common.actions';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { CommonDataApi } from '../../../services/main/common-data/common-data.api';
import { HTTPStatusEnum } from '../../models/app.enums';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable()
export class CommonEffects {
  getCountryListAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(commonActions.getCountryListAction),
      mergeMap(() =>
        this.commonApi.getCountriesList().pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return commonActions.getCountryListCompletedAction(res.data);
            } else {
              return commonActions.getCountryListFailedAction(res.status);
            }
          }),
          catchError(() => {
            return of(commonActions.getCountryListFailedAction('-1'));
          })
        )
      )
    )
  );

  getRolesListAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(commonActions.getRolesListAction),
      mergeMap(() =>
        this.commonApi.getRolesList().pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return commonActions.getRolesListCompletedAction(res.data);
            } else {
              return commonActions.getRolesListFailedAction(res.status);
            }
          }),
          catchError(() => {
            return of(commonActions.getRolesListFailedAction('-1'));
          })
        )
      )
    )
  );

  getAssignableRolesListAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(commonActions.getAssignableRolesListAction),
      mergeMap(() =>
        this.commonApi.getAssignableRolesList().pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return commonActions.getAssignableRolesListCompletedAction(res.data);
            } else {
              return commonActions.getAssignableRolesListFailedAction(res.status);
            }
          }),
          catchError(() => {
            return of(commonActions.getAssignableRolesListFailedAction('-1'));
          })
        )
      )
    )
  );

  getSchemesListAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(commonActions.getSchemesListAction),
      mergeMap(() =>
        this.commonApi.getSchemesList().pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return commonActions.getSchemesListCompletedAction(res.data);
            } else {
              return commonActions.getSchemesListFailedAction(res.status);
            }
          }),
          catchError(() => {
            return of(commonActions.getSchemesListFailedAction('-1'));
          })
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private commonApi: CommonDataApi
  ) {}
}
