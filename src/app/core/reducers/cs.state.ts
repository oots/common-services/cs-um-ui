import { CommonState } from 'src/app/core/reducers/common/common.state';
import { UserState } from './user/user.state';
import { EDeliveryState } from 'src/app/core/reducers/e-delivery/e-delivery.state';
import { EventState } from './event/event.state';

export interface CsStateKeys {
  csUser: any;
  csCommon: any;
  csEDelivery: any;
}

export interface CsState {
  csUser: UserState;
  csCommon: CommonState;
  csEDelivery: EDeliveryState;
  csEvent: EventState;
}
