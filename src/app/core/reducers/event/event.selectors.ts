import { EventState } from './event.state';
import { createSelector } from '@ngrx/store';
import { getEventState } from '../cs.selectors';

export const getList = (eventState: EventState) => eventState.list;
export const getTableOptions = (eventState: EventState) => eventState.tableOptions;
export const getActionConfirmation = (eventState: EventState) => eventState.confirmAction;

export const selectList = createSelector(getEventState, getList);
export const selectTableOptions = createSelector(getEventState, getTableOptions);
export const selectActionConfirmation = createSelector(getEventState, getActionConfirmation);
