import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EventApi } from '../../../services/events/event.api';
import { EventEffects } from './event.effects';
import { EventService } from '../../../services/events/event.service';
import { reducer } from './event.reducer';

@NgModule({
  imports: [StoreModule.forFeature('csEvent', reducer), EffectsModule.forFeature([EventEffects])],
  providers: [EventService, EventApi]
})
export class EventStateModule {}
