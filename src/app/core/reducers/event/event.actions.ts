import { TableOptionsModel } from '../../models/app/table.options.model';
import { createAction } from '@ngrx/store';
import { EventListModel } from '../../models/response/event.model';

export const resetStateAction = createAction('[csEvent] Reset state');

export const getEventListAction = createAction('[csEvent] Get event list');

export const getEventListCompletedAction = createAction('[csEvent] Get event list completed', (payload: EventListModel) => ({ payload }));

export const getEventListFailedAction = createAction('[csEvent] Get event list failed', (payload: string) => ({ payload }));

export const setListTableOptionsAction = createAction('[csEvent] Set event table options', (payload: TableOptionsModel) => ({ payload }));

export const resetListTableOptionsAction = createAction('[csEvent] Reset event table options');

export const resetConfirmationAction = createAction('[csEvent] Reset Action Confirmation');
