import * as eventActions from './event.actions';

import { Action, createReducer, on } from '@ngrx/store';
import { initialEventState, EventState } from './event.state';

export const reducerFn = createReducer(
  initialEventState,

  on(eventActions.getEventListAction, (state) => ({
    ...state,
    list: { value: state.list.value, loading: true, error: null }
  })),

  on(eventActions.getEventListCompletedAction, (state, { payload }) => ({
    ...state,
    list: { value: payload, loading: false, error: null }
  })),

  on(eventActions.getEventListFailedAction, (state, { payload }) => ({
    ...state,
    list: { value: null, loading: false, error: payload }
  })),

  on(eventActions.setListTableOptionsAction, (state, { payload }) => ({
    ...state,
    tableOptions: { value: payload, loading: false, error: null }
  })),

  on(eventActions.resetConfirmationAction, (state) => ({
    ...state,
    confirmAction: initialEventState.confirmAction
  })),

  on(eventActions.resetStateAction, () => initialEventState)
);

export function reducer(state = initialEventState, action: Action): EventState {
  return reducerFn(state, action);
}
