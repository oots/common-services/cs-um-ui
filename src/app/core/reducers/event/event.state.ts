import { ActionConfirmationModel } from '../../models/response/action.confirmation.model';
import { AsyncStoreObject } from '../cs.utils';
import { TableOptionsModel } from '../../models/app/table.options.model';
import { EventListModel } from '../../models/response/event.model';

export interface EventState {
  list: AsyncStoreObject<EventListModel>;
  tableOptions: AsyncStoreObject<TableOptionsModel>;
  confirmAction: AsyncStoreObject<ActionConfirmationModel>;
}

export const initialEventState: EventState = {
  list: { value: null, loading: false, error: null },
  tableOptions: {
    value: {
      pageNumber: 0,
      pageSize: 25
    },
    loading: false,
    error: null
  },
  confirmAction: { value: null, loading: false, error: null }
};
