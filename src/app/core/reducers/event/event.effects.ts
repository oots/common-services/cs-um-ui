import * as eventActions from './event.actions';
import { HTTPStatusEnum } from '../../models/app.enums';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { EventApi } from '../../../services/events/event.api';
import { EventService } from '../../../services/events/event.service';
import { of } from 'rxjs';

@Injectable()
export class EventEffects {
  getEventListAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(eventActions.getEventListAction),
      withLatestFrom(this.eventService.tableOptions$.pipe(map((options) => options?.value))),
      mergeMap((options) => {
        return this.eventApi.getEventList(options[1]).pipe(
          map((res: any) => {
            if (res.status === HTTPStatusEnum.OK_200) {
              return eventActions.getEventListCompletedAction(res.data);
            } else {
              return eventActions.getEventListFailedAction(res.status);
            }
          }),
          catchError(() => of(eventActions.getEventListFailedAction('-1')))
        );
      })
    )
  );

  setListTableOptionsAction = createEffect(
    () =>
      this.actions$.pipe(
        ofType(eventActions.setListTableOptionsAction),
        map(() => this.eventService.getList())
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private eventApi: EventApi,
    private eventService: EventService
  ) {}
}
