import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { catchError, filter, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HTTPStatusEnum, UserPermissionEnum } from '../models/app.enums';
import { CommonDataService } from '../../services/main/common-data/common-data.service';

export const authGuard: CanActivateFn = () => {
  const userService = inject(UserService);
  const commonDataService = inject(CommonDataService);
  const router = inject(Router);

  return userService.profile$.pipe(
    filter((filterVal) => !filterVal.loading),
    map((userProfile) => {
      if (userProfile?.value?.userId) {
        const userPermissions = commonDataService.getUserPermissions(userProfile.value, [UserPermissionEnum.USER_APP_VIEW]);
        if (userPermissions[UserPermissionEnum.USER_APP_VIEW]) {
          return true;
        }
        router.navigate(['/not-registered']);
        return false;
      }

      if (userProfile?.error?.status === HTTPStatusEnum.NOT_FOUND_404) {
        router.navigate(['/not-registered']);
        return false;
      }

      if (userProfile?.error?.status === HTTPStatusEnum.UNAUTHORIZED_401) {
        router.navigate(['/no-permission']);
        return false;
      }

      router.navigate(['/login']);
      return false;
    }),
    catchError(() => {
      router.navigate(['/login']);
      return of(false);
    })
  );
};
