import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { CommonDataService } from '../../services/main/common-data/common-data.service';
import { UserService } from '../../services/user/user.service';
import { UserPermissionEnum } from '../models/app.enums';
import { catchError, filter, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { UserEditComponent } from '../../components/main/user/user-edit/user-edit.component';
import { UserFlowComponent } from '../../components/main/user/user-flow/user-flow.component';
import { EDeliveryEditComponent } from '../../components/main/e-delivery/e-delivery-edit/e-delivery-edit.component';
import { EDeliveryFlowComponent } from '../../components/main/e-delivery/e-delivery-flow/e-delivery-flow.component';
import { UserComponent } from '../../components/main/user/user.component';
import { EDeliveryComponent } from '../../components/main/e-delivery/e-delivery.component';

export const authorizationGuard: CanActivateFn = (route) => {
  const commonDataService = inject(CommonDataService);
  const router = inject(Router);

  return inject(UserService).profile$.pipe(
    filter((filterVal) => !filterVal.loading),
    map((userProfile) => {
      let permissionsArray!: UserPermissionEnum[];
      switch (route?.component?.name) {
        case EDeliveryEditComponent.name:
        case EDeliveryComponent.name:
          permissionsArray = [UserPermissionEnum.CRUD_EDELIVERY, UserPermissionEnum.VIEW_MS_EDELIVERY];
          break;
        case EDeliveryFlowComponent.name:
          permissionsArray = [UserPermissionEnum.CRUD_EDELIVERY];
          break;
        case UserEditComponent.name:
        case UserComponent.name:
        case UserFlowComponent.name:
          permissionsArray = [UserPermissionEnum.CRUD_USER, UserPermissionEnum.CRUD_MS_USER];
          break;
      }

      const userPermissions = commonDataService.getUserPermissions(userProfile.value, permissionsArray);

      if (userPermissions[permissionsArray[0]] || userPermissions[permissionsArray[1] || null]) {
        return true;
      }
      router.navigate(['/no-permission']);
      return false;
    }),
    catchError(() => {
      router.navigate(['/home']);
      return of(false);
    })
  );
};
