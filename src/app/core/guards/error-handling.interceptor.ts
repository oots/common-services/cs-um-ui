import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CommonService } from '../../services/main/common.service';
import { HTTPStatusEnum } from '../models/app.enums';
import { AppGrowlEnum } from '../models/app/growl.model';
import { UserService } from '../../services/user/user.service';

@Injectable()
export class ErrorHandlingInterceptor implements HttpInterceptor {
  constructor(
    private commonService: CommonService,
    private userService: UserService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error) => {
        if (this.userService.getIsUserRegistered()) {
          switch (error.status) {
            case HTTPStatusEnum.UNAUTHORIZED_401:
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROWL_UNAUTHORISED_401_HEADING,
                AppGrowlEnum.GROWL_UNAUTHORISED_401_MESSAGE
              );
              break;
            case HTTPStatusEnum.NOT_FOUND_404:
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROWL_NOT_FOUND_404_HEADING,
                AppGrowlEnum.GROWL_NOT_FOUND_404_MESSAGE
              );
              break;
            case HTTPStatusEnum.NOT_ACCEPTABLE_406:
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROWL_NOT_FOUND_406_HEADING,
                AppGrowlEnum.GROWL_NOT_FOUND_406_MESSAGE
              );
              break;
            case HTTPStatusEnum.INTERNAL_SERVER_ERROR_500:
              this.commonService.displayGrowlMessage(
                AppGrowlEnum.GROWL_DANGER,
                AppGrowlEnum.GROWL_GENERIC_SERVER_ERROR_500_HEADING,
                AppGrowlEnum.GROWL_GENERIC_SERVER_ERROR_500_MESSAGE
              );
              break;
          }
        }
        return throwError(() => error);
      })
    );
  }
}
