import { EuiAllModule } from '@eui/components';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const MODULES = [CommonModule, FormsModule, ReactiveFormsModule, EuiAllModule, TranslateModule];

@NgModule({
  imports: [...MODULES],
  declarations: [],
  exports: [...MODULES]
})
export class SharedModule {}
