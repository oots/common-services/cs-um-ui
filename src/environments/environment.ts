import { EuiEnvConfig } from '@eui/core';

export const environment: EuiEnvConfig = {
  production: false,
  version: '1.7.0+b4',
  enableDevToolRedux: true,
  loginUrl: 'http://localhost:8085/user-mgmt/api/oauth2/authorization/eulogin',
  envDynamicConfig: {
    uri: 'assets/env-json-config.json',
    deepMerge: true,
    merge: ['modules']
  },
  endpoint: { url: '//localhost:8085/user-mgmt/api' },
  //adminAppHomeUrl: '',
  adminAppHomeUrl: 'http://localhost:4300/home'
};
