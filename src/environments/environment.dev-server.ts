import { EuiEnvConfig } from '@eui/core';

export const environment: EuiEnvConfig = {
  production: false,
  version: '1.7.0+b4',
  enableDevToolRedux: true,
  loginUrl: '/oauth2/authorization/eulogin',
  evidenceTypeClassificationPrefix: 'https://sr.oots.tech.europa.eu/evidencetypeclassifications',
  envDynamicConfig: {
    uri: 'assets/env-json-config.json',
    deepMerge: true,
    merge: ['modules']
  },
  endpoint: { url: '' },
  adminAppHomeUrl: ''
};
