import { EuiEnvConfig } from '@eui/core';

export const environment: EuiEnvConfig = {
  production: true,
  version: '1.4.0',
  enableDevToolRedux: true,
  loginUrl: '/user-mgmt/api/oauth2/authorization/eulogin',
  evidenceTypeClassificationPrefix: 'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications',
  envDynamicConfig: {
    uri: 'assets/env-json-config.json',
    deepMerge: true,
    merge: ['modules']
  },
  endpoint: {
    url: '/user-mgmt/api'
  },
  adminAppHomeUrl: ''
};
